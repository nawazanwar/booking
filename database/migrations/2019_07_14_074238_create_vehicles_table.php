<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->integer('terminal_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('time')->nullable();
            $table->string('seats')->nullable();
            $table->boolean('transport')->default(true);
            $table->boolean('booking')->default(true);
            $table->enum('property', array('no_ac', 'ac'))->default('no_ac');
            $table->boolean('featured')->default(false);
            $table->boolean('active')->default(true);
            $table->boolean('public')->default(true);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
            $table->foreign('terminal_id')
                ->references('id')
                ->on('terminals')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
