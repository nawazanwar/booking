<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('nav_links')) {
            return true;
        }

        Schema::create('nav_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nav_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('url');
            $table->integer('order');
            $table->boolean('is_blank')->default(false);
            $table->timestamps();

            $table->foreign('nav_id')
                ->references('id')
                ->on('nav')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nav_links');
    }
}
