<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->integer('cinema_id')->unsigned();
            $table->string('name')->nullable();
            $table->date('date')->nullable();
            $table->string('start')->nullable();
            $table->string('end')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('break')->nullable();
            $table->string('ticket')->nullable();
            $table->enum('type', array('lollywood', 'bollywood', 'hollywood', 'animated'))->default('lollywood');
            $table->boolean('featured')->default(false);
            $table->boolean('active')->default(true);
            $table->boolean('public')->default(true);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
            $table->foreign('cinema_id')
                ->references('id')
                ->on('cinemas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
