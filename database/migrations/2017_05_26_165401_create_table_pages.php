<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if(Schema::hasTable('pages')) {
            return true;
        }

        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug')->unique();
            
            $table->text('content')->nullable();
            $table->string('excerpt', 512)->nullable();
            
            $table->string('hashtag', 255)->nullable();
            $table->string('related', 255)->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            
            $table->boolean('active')->default(true);
            $table->boolean('public')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}