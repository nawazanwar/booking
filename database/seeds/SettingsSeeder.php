<?php

use Illuminate\Database\Seeder;

use App\Models\Setting;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();
        Setting::create([
            'name' => 'site-name',
            'value' => 'Booking',
            'type'=>'text'
        ]);
        Setting::create([
            'name' => 'site-logo',
            'value' => 'uploads/settings/site-logo.jpg',
            'type' => 'file',
        ]);
        Setting::create([
            'name' => 'favicon-logo',
            'value' => 'uploads/settings/favicon-logo.jpg',
            'type' => 'file',
        ]);
        Setting::create([
            'name' => 'cinema-logo',
            'value' => 'uploads/settings/cinema-logo.jpg',
            'type' => 'file',
        ]);
        Setting::create([
            'name' => 'food-point-logo',
            'value' => 'uploads/settings/food-point-logo.jpg',
            'type' => 'file',
        ]);
        Setting::create([
            'name' => 'terminal-logo',
            'value' => 'uploads/settings/terminal-logo.jpg',
            'type' => 'file',
        ]);
        Setting::create([
            'name' => 'first-heading',
            'value' => 'Booking Online',
            'type'=>'text'
        ]);
        Setting::create([
            'name' => 'second-heading',
            'value' => 'Booking Movies,Vehicles and Foods',
            'type'=>'textarea'
        ]);
        Setting::create([
            'name' => 'third-heading',
            'value' => 'Here you can book all types of the movies in any cinema of the pakistan also you can also book the vehicles and foods from different food points',
            'type'=>'textarea'
        ]);
        Setting::create([
            'name' => 'maintenance',
            'value' => 0,
            'type' => 'checkbox',
        ]);
        Setting::create([
            'name' => 'facebook',
            'value' => 'https://www.facebook.com/',
        ]);
        Setting::create([
            'name' => 'instagram',
            'value' => 'https://www.instagram.com/',
        ]);
        Setting::create([
            'name' => 'googleplus',
            'value' => 'https://www.plus.google.com/',
        ]);
        Setting::create([
            'name' => 'vimeo',
            'value' => 'https://www.vimeo.com/',
        ]);
        Setting::create([
            'name' => 'youtube',
            'value' => 'https://www.youtube.com/',
        ]);

        Setting::create([
            'name' => 'first-slide',
            'value' => 'uploads/settings/first-slide.jpg',
            'type' => 'file',
        ]);

        Setting::create([
            'name' => 'second-slide',
            'value' => 'uploads/settings/second-slide.jpg',
            'type' => 'file',
        ]);

        Setting::create([
            'name' => 'third-slide',
            'value' => 'uploads/settings/third-slide.jpg',
            'type' => 'file',
        ]);

        Setting::create([
            'name' => 'fourth-slide',
            'value' => 'uploads/settings/fourth-slide.jpg',
            'type' => 'file',
        ]);

        Setting::create([
            'name' => 'fifth-slide',
            'value' => 'uploads/settings/fifth-slide.jpg',
            'type' => 'file',
        ]);

        Setting::create([
            'name' => 'meta_description',
            'value' => '',
            'type' => 'textarea',
        ]);
        Setting::create([
            'name' => 'meta_keywrods',
            'value' => '',
            'type' => 'textarea',
        ]);
    }
}
