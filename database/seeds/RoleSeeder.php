<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();

        Role::create([
            'name' => 'admin',
            'label' => 'Admin',
            'active' => true,
        ]);
        Role::create([
            'name' => 'user',
            'label' => 'User',
            'active' => true,
        ]);
    }
}
