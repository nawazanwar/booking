<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{

    public function run()
    {

        DB::table('users')->delete();

        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@booking.com',
            'password' => Hash::make('admin123'),
            'active' => true,
        ]);
        User::create([
            'name' => 'user',
            'username' => 'user',
            'email' => 'user@booking.com',
            'password' => Hash::make('user123'),
            'active' => true,
        ]);
    }
}
