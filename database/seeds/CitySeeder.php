<?php

class CitySeeder extends CsvSeeder
{
    /**
     * DB table name
     *
     * @var string
     */
    protected $table;

    /**
     * CSV filename
     *
     * @var string
     */
    protected $filename;

    public function __construct()
    {
        $this->table = 'cities';
        $this->filename = base_path('database/seeds/csv/cities.csv');

    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->table)->delete();
        $seedData = $this->seedFromCSV($this->filename, ',');
        foreach($seedData as $data) {
            DB::table($this->table)->insert($data);
        }
    }
}
