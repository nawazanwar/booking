<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        Category::create([
            'name' => 'Cinemas',
            'slug' => 'cinemas',
            'parent_id' => 0,
            'user_id' => 1,
            'image' => 'uploads/categories/cinema.jpg',
        ]);

        Category::create([
            'name' => 'Terminals',
            'slug' => 'terminals',
            'parent_id' => 0,
            'user_id' => 1,
            'image' => 'uploads/categories/terminal.jpg',
        ]);

        Category::create([
            'name' => 'Food Points',
            'slug' => 'food-points',
            'parent_id' => 0,
            'user_id' => 1,
            'image' => 'uploads/categories/food.jpg',
        ]);
    }
}
