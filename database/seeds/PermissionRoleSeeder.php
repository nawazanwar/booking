<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Permission;

class PermissionRoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('permission_role')->delete();
        $adminRoleId = DB::table('roles')->where('name', 'admin')->value('id');
        $permissions = Permission::all();

        foreach ($permissions as $permission) {

            DB::table('permission_role')->insert([
                'permission_id' => $permission->id,
                'role_id' => $adminRoleId,
            ]);
        }
    }
}
