<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    public function run()
    {
        DB::table('role_user')->delete();

        /*Admin*/

        $admin_id = DB::table('users')->where('email', 'admin@booking.com')->value('id');
        $admin_role_id = DB::table('roles')->where('name', 'admin')->value('id');

        DB::table('role_user')->insert([
            'user_id' => $admin_id,
            'role_id' => $admin_role_id,
        ]);

        /*user*/

        $user_id = DB::table('users')->where('email', 'user@booking.com')->value('id');
        $user_role_id = DB::table('roles')->where('name', 'user')->value('id');

        DB::table('role_user')->insert([
            'user_id' => $user_id,
            'role_id' => $user_role_id,
        ]);

    }
}
