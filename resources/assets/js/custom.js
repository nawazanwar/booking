function loadFile(event) {
    var reader = new FileReader();
    reader.onload = function () {
        var decode_file = reader.result;
        $(event.target).closest('.form-group').find('img').attr('src', decode_file);
    };
    reader.readAsDataURL(event.target.files[0]);
}

(function ($) {


    $( ".auto_city" ).autocomplete({

        source: function(request, response) {
            $.ajax({
                url: "/cities/autocomplete",
                data: {
                    term : request.term
                },
                dataType: "json",
                success: function(data){
                    var resp = $.map(data,function(obj){
                        //console.log(obj.city_name);
                        return obj.name;
                    });

                    response(resp);
                }
            });
        },
        minLength: 1
    });


    $(document).on('click', ".btn-nav-add-row", function (event) {


        event.preventDefault();

        var $tr = $(this).closest('tr');
        var $clone = $tr.clone();
        var itemId = $clone.find('input[type=checkbox]').data('id');

        $clone.find('input[type=checkbox]').attr('id', 'id_' + (itemId + 1));
        $clone.find('[id^=id_]').val('');

        $tr.after($clone);

    });

    $(document).on('click', ".btn-nav-remove-row", function (event) {
        event.preventDefault();

        var rowCount = $('#navTbl tr').length;

        if (rowCount > 1) {
            var $tr = $(this).closest('tr');
            $tr.remove();
        }

    });


    $('input.timepicker').timepicker({});

    $(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });

    $(document).on('change', '#category-type', function () {
        var value = $(this).find('option:selected').val();
        if (value == 'cinema') {
            $(".seats_holder").show();
        } else {
            $(".seats_holder").hide();
        }
    });

    $(document).on('change', '#post-type', function () {
        $(".post_detail_holder").hide();
        var value = $(this).find('option:selected').val();
        if (value == 'movie') {
            $(".movie_booking_holder").show();
        } else if (value == 'vehicle') {
            $(".vehicle_booking_holder").show();
        }
    });

    $(document).on('keyup', '#name', function () {
        $("#slug").val('');
        var value = $(this).val().replace(/\s+/g, '-').toLowerCase();
        $("#slug").val(value);
    });

    // UpVote Answer
    $(document).on('click', '.upvote', function () {
        var object = $(this);
        var objectId = object.data('id');
        ajaxRequest('/votes/up', {id: objectId}, 'POST', function (response) {

            if (response.status == "error") {
                console.log('Error Occurred: ', response);
            } else {
                if (response.data.votes) {
                    object.find('.upvotes').text(response.data.votes);
                }
            }

        });

    });

    // DownVote Answer
    $(document).on('click', '.downvote', function () {
        var object = $(this);
        var objectId = object.data('id');
        ajaxRequest('/votes/down', {id: objectId}, 'POST', function (response) {

            if (response.status == "error") {
                console.log('Error Occurred: ', response);
            } else {
                if (response.data.votes) {
                    object.find('.downvotes').text(response.data.votes);
                }
            }

        });

    });

    // Follow Question/Topic
    $(document).on('click', '.follow-it', function () {
        var object = $(this);
        var objectId = object.data('id');
        var type = object.data('type');
        ajaxRequest('/follow/' + type, {id: objectId}, 'POST', function (response) {

            if (response.status == "error") {
                console.log('Error Occurred: ', response);
            } else {
                if (parseInt(response.data.follow) == 1) {
                    object.find('i').addClass('text-primary fa-bell').removeClass('fa-bell-o');
                } else {
                    object.find('i').removeClass('text-primary fa-bell').addClass('fa-bell-o');
                }
            }

        });

    });

    // Star Object
    $(document).on('click', '.star-it', function () {
        var object = $(this);
        var objectId = object.data('id');
        var type = object.data('type');
        ajaxRequest('/favorite/' + type, {id: objectId}, 'POST', function (response) {

            if (response.status == "error") {
                console.log('Error Occurred: ', response);
            } else {
                if (parseInt(response.data.favorite) == 1) {
                    object.find('i').addClass('text-primary fa-star').removeClass('fa-star-o');
                } else {
                    object.find('i').removeClass('text-primary fa-star').addClass('fa-star-o');
                }
            }

        });

    });

    // Star Object
    $(document).on('change', '.select-country', function () {
        var object = $(this);
        var countryId = object.find('option:selected').val();

        ajaxRequest('/cities/' + countryId, {}, 'GET', function (response) {

            if (response.status == "error") {
                console.log('Error Occurred: ', response);
            } else {

                if (response.data.cities) {

                    $(object.data('target')).empty().selectpicker('refresh');

                    $.each(response.data.cities, function (val, text) {

                        $(object.data('target')).append(
                            $('<option></option>').attr("value", val).html(text)
                        ).selectpicker('refresh');

                    });
                }

            }

        });

    });

    // Generic function to send all ajax requests
    function ajaxRequest(url, data, method = 'GET', callback) {
        $.ajax({
            type: method,
            global: false,
            async: true,
            url: url,
            data: data,
            success: function (response) {
                callback(response);
            },
            error: function () {
                console.log("Error Occurred");
            }
        });
    }

})(jQuery);
