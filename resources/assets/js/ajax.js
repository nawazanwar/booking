var Ajax = new function () {

    this.call = function (url, data, method = 'GET', callback) {
        this.setAjaxHeader();
        $.ajax({
            type: method,
            global: false,
            async: true,
            url: url,
            data: data,
            beforeSend: function () {
                $('.overlay').show();
            },
            success: function (response) {
                $('.overlay').hide();
                callback(response);
            },
            error: function () {
                console.log("Error Occurred");
            }
        });
    };
    this.setAjaxHeader = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
            }
        });
    };
};