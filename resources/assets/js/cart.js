var Cart = new function () {

    this.cart = {'movie': {}, 'vehicle': {}, 'food': {}};

    this.add = function (modal_id, modal_type, seat_id, quantities, price) {

        if ($.cookie('cart')) {
            this.cart = JSON.parse($.cookie('cart'));
        } else {
            this.cart = this.cart;
        }

        if (this.cart.hasOwnProperty(modal_type)) {
            this.cart[modal_type][seat_id] = {
                'modal_id': modal_id,
                'seat_id': seat_id,
                'quantities': quantities,
                'price': price
            };
        }
        $.cookie('cart', JSON.stringify(this.cart));
        this.bookedMe(this.cart);
        Ajax.call('/booking/store/', this.cart, 'GET', function (response) {
            //alert(response);
        })
    };

    this.bookedMe = function (cart) {
        $.each(cart, function (key, value) {
            $.each(value, function (inner_key, inner_value) {
                $("#" + inner_key).removeClass('badge-primary').addClass('badge-success').empty().html("Booked");
                $("#booked-icon-" + inner_key).removeClass('badge-info').addClass('badge-success').find('i').removeClass('fa fa-ticket').addClass('fa fa-check');
            })
        });
    };

    this.remove = function () {

    };
    this.update = function () {

    }


};