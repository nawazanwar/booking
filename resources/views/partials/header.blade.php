<nav class="navbar navbar-expand-lg navbar-light navbar-stick-dark" data-navbar="sticky">
    <div class="container">
        <div class="row">
            <section class="col-lg-5 navbar-mobile">
                <nav class="nav nav-navbar mr-auto">
                    @if(Auth()->check() and (Auth::user()->roles()->first()->name=="super_admin" || Auth::user()->roles()->first()->name=="admin"))
                        <a class="nav-link lead-3" href="#" data-toggle="offcanvas" data-target="#offcanvas-menu">☰</a>
                    @endif
                    @isset($mainNav)
                        @isset($mainNav->links)
                            @foreach($mainNav->links as $item)
                                <a href="{{$item->url}}" class="nav-link"
                                   @if($item->is_blank) target="_blank" @endif>{{$item->title}}</a>
                            @endforeach
                        @endisset
                    @endisset
                </nav>
            </section>

            <div class="col-auto col-lg-auto1 mr-auto mx-lg-auto d-flex align-items-center">
                <a class="navbar-brand" href="{{route('home')}}">
                    <img class="logo-dark avatar avatar-sm img-thumbnail "
                         src="{{asset(isset($generalSetting['site-logo'])?$generalSetting['site-logo']:'')}}"
                         alt="logo">
                    <img class="logo-light avatar avatar-sm img-thumbnail"
                         src="{{asset(isset($generalSetting['site-logo'])?$generalSetting['site-logo']:'')}}"
                         alt="logo">
                </a>
            </div>

            <div class="col-auto col-lg-5 text-right">
                <style>
                    .total-header-section {
                        border-bottom: 1px solid #d2d2d2;
                    }

                    .total-section p {
                        margin-bottom: 20px;
                    }

                    .cart-detail {
                        padding: 15px 0px;
                    }

                    .cart-detail-img img {
                        width: 100%;
                        height: 100%;
                        padding-left: 15px;
                    }

                    .cart-detail-product p {
                        margin: 0px;
                        color: #000;
                        font-weight: 500;
                    }

                    .cart-detail .price {
                        font-size: 12px;
                        margin-right: 10px;
                        font-weight: 500;
                    }

                    .cart-detail .count {
                        color: #C2C2DC;
                    }

                    .checkout {
                        border-top: 1px solid #d2d2d2;
                        padding-top: 15px;
                    }
                </style>
                @if(Auth::check())
                    @php $user = Auth()->user(); @endphp
                    <div class="dropdown pull-right mr-2">
                      <span class="dropdown-toggle no-caret" data-toggle="dropdown" aria-expanded="false">
                        <img class="avatar avatar-xs" src="{{asset('img/avatar/1.jpg')}}" alt="user">
                      </span>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Profile</a>
                            <a class="dropdown-item" href="#">Inbox</a>
                            <a class="dropdown-item" href="#">Settings</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{route('logout')}}">Logout</a>
                        </div>
                    </div>
                    @if(session('cart'))
                        <div class="dropdown pull-right mr-3">
                            <span class="dropdown-toggle no-caret text-white btn btn-xs btn-success fs-13"
                                  data-toggle="dropdown"
                                  aria-expanded="false">
                                <i class="fa fa-cart-arrow-down"></i> {{ count(session('cart')) }}
                            </span>
                            <div class="dropdown-menu dropdown-menu-right w-500">
                                <div class="row total-header-section">
                                    <div class="col-lg-6 col-sm-6 col-6">
                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span
                                                class="badge badge-pill badge-danger">{{ count(session('cart')) }}</span>
                                    </div>

                                    <?php $total = 0 ?>
                                    @foreach(session('cart') as $id => $details)
                                        <?php $total += $details['price'] * $details['quantity'] ?>
                                    @endforeach

                                    <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                                        <p>Total: <span class="text-info">{{ $total }}</span></p>
                                    </div>
                                </div>

                                @if(session('cart'))
                                    @foreach(session('cart') as $id => $details)
                                        @php
                                            if (isset($details['front_image'])){
                                             $front_image = $details['front_image'];
                                            }else{
                                             $front_image = 'img/no_image.png';
                                            }
                                            if (isset($details['back_image'])){
                                                $back_image = $details['back_image'];
                                            }else{
                                                $back_image = 'img/no_image.png';
                                            }
                                        @endphp
                                        <div class="row cart-detail">
                                            <div class="col-lg-3 col-sm-3 col-3 cart-detail-img">
                                                <div class="card overflow-hidden h-80px w-80px">
                                                    <div class="card-body"
                                                         style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                                                    </div>
                                                    <div class="card-hover text-white bg-img"
                                                         data-animation="slide-down"
                                                         style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                                                         data-overlay="6">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-9 col-sm-9 col-9 cart-detail-product">
                                                <p>{{ $details['name'] }}</p>
                                                <span class="price text-info"> {{ $details['price'] }}</span> <span
                                                        class="count"> Quantity:{{ $details['quantity'] }}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-12  checkout text-right">
                                        <a href="{{route('cart.index') }}" class="badge badge-info btn-sm">View all
                                            <span
                                                    class="fa fa-eye"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <a class="btn btn-sm btn-round btn-outline-success d-none d-lg-inline-block mr-2"
                       href="{{route('login')}}">Sign in</a>
                    <a class="btn btn-sm btn-round btn-success" href="{{route('register')}}">Sign up</a>
                @endif
            </div>
        </div>
    </div>
</nav><!-- /.navbar -->


<!-- Header -->
<style>
    .header {
        background: url({{asset('uploads/settings/main-slide.jpg')}});
        background-position: center center;
        animation: chbg 15s infinite alternate;
        background-size: cover;
        background-repeat: no-repeat;
    }

    @keyframes chbg {
        0% {
            background: url({{asset(isset($generalSetting['first-slide'])?$generalSetting['first-slide']:'')}});
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;

        }
        20% {
            background: url({{asset(isset($generalSetting['third-slide'])?$generalSetting['third-slide']:'')}});
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;

        }
        60% {
            background: url({{asset(isset($generalSetting['fourth-slide'])?$generalSetting['fourth-slide']:'')}});
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;

        }
        80% {
            background: url({{asset(isset($generalSetting['fifth-slide'])?$generalSetting['fifth-slide']:'')}});
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;

        }
        100% {
            background: url({{asset(isset($generalSetting['first-slide'])?$generalSetting['first-slide']:'')}});
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;
        }
    }
</style>
@php
    if(Request::is('/')){
       $height ="h-fullscreen";
    }else{
        $height ="h-500";
    }
@endphp
<header class="header text-white pb-0 overflow-hidden {{$height}}">
    <div class="overlay opacity-95"
         style="background-image: linear-gradient(to bottom, #09203f8a 0%, #537895 100%);"></div>
    <div class="container text-center">
        <div class="row align-items-center h-100">

            <div class="col-md-8 mx-auto">
                <h1 class="font-weight-bold">{{isset($generalSetting['first-heading'])?$generalSetting['first-heading']:''}}</h1>
                <h3 class="font-weight-bold">{{isset($generalSetting['second-heading'])?$generalSetting['second-heading']:''}}</h3>
                <p class="lead mt-4 mb-1">{{isset($generalSetting['third-heading'])?$generalSetting['third-heading']:''}}</p>
            </div>
            <div class="col-md-8 mx-auto align-self-end mt-6">
                <div class="row gap-y justify-content-center">
                    @if(strpos($_SERVER['REQUEST_URI'], 'muntazim') == false)
                        @isset($latestCategories)
                            @foreach($latestCategories as $lc)
                                <div class="col-md-4">
                                    @php
                                        if($lc->slug=='cinemas'){
                                          $lc_path = asset(isset($generalSetting['cinema-logo'])?$generalSetting['cinema-logo']:'');
                                        }elseif($lc->slug=='terminals'){
                                          $lc_path = asset(isset($generalSetting['terminal-logo'])?$generalSetting['terminal-logo']:'');
                                        }elseif($lc->slug=='food-points'){
                                          $lc_path = asset(isset($generalSetting['food-point-logo'])?$generalSetting['food-point-logo']:'');
                                        }
                                    @endphp
                                    <div class="card text-white bg-img justify-content-center h-150 header-sections"
                                         style="background-image: url({{$lc_path}})"
                                         data-scrim-bottom="3">
                                        <div class="card-body flex-grow-0">
                                            <a href="{{route('home',[$lc->slug])}}"
                                               class="card-title text-center mb-0 badge badge-primary">{{$lc->name}}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endisset
                    @else
                        <header class="col-md-12">
                            <div class="w-100 bg-primary text-center breadcrumb_holder">
                                @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
                            </div>
                        </header>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header><!-- /.header -->
<div id="offcanvas-menu" class="offcanvas text-white w-250 px-7" data-animation="slide-left"
     style="background-color: rgba(0,0,0,0.9)">
    <button type="button" class="close position-static px-0" data-dismiss="offcanvas" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>

    <nav class="nav nav-lead flex-column my-7">
        <a class="nav-link"
           href="{{route('muntazim.countries')}}">{{__('faqs.all_countries')}}</a>
        <a class="nav-link"
           href="{{route('muntazim.roles')}}">{{__('faqs.all_roles')}}</a>
        <a class="nav-link"
           href="{{route('muntazim.permissions')}}">{{__('faqs.all_permissions')}}</a>
        <a class="nav-link"
           href="{{route('muntazim.categories')}}">{{__('faqs.all_categories')}}</a>
        <a class="nav-link"
           href="{{route('muntazim.posts')}}">{{__('faqs.all_posts')}}</a>
        <a class="nav-link" href="{{route('muntazim.nav')}}">{{__('faqs.nav')}}</a>
        <a class="nav-link" href="{{route('muntazim.settings')}}">{{__('faqs.settings')}}</a>
        <a class="nav-link" href="{{route('muntazim.users')}}">{{__('faqs.all_users')}}</a>
    </nav>
</div>