<div class="row">
    @if($errors->any())
        <div class="alert alert-danger col-md-8 mx-auto alert-dismissible mt-3 mb-3" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if ($success = \Illuminate\Support\Facades\Session::get('success'))
        <div class="alert alert-success col-md-8 mx-auto alert-dismissible mt-3 mb-3" role="alert">
            <p class="alert-heading font-weight-bold">Well done!</p>
            <p>{{ $success }}</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

    @if ($danger = \Illuminate\Support\Facades\Session::get('danger'))
        <div class="alert alert-danger col-md-8 mx-auto alert-dismissible mt-3 mb-3" role="alert">
            <p class="alert-heading font-weight-bold">Oh ho!</p>
            <p>{{ $danger }}</p>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    @endif

</div>