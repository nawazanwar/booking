@section('scripts')
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=o23blg9ah1zca8fzz4hpnqndoxanbov1pv288bvw0ptznp19"></script>
    <script type="text/javascript">
        $(function () {
            // Replace the textarea with tinyMCE
            tinymce.init({
                selector: '.rich-editor',
                height: 300,
                menubar: false,
                plugins: [
                    /*"print preview fullpage searchreplace autolink directionality visualblocks",
                    "visualchars fullscreen image link media template codesample table charmap hr pagebreak",
                    "nonbreaking anchor toc insertdatetime advlist lists wordcount",
                    "imagetools textpattern help"*/
                    "autolink directionality visualblocks image imagetools link media codesample table charmap hr",
                    "insertdatetime advlist lists wordcount textpattern",
                ],
                toolbar1: "insertfile undo redo | formatselect | bold italic underline strikethrough superscript subscript code emoticons forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table insertdatetime",
                toolbar2: "link image media pageembed codesample charmap hr | removeformat",
                browser_spellcheck: true,
                image_advtab: true,
                relative_urls: false,
                remove_script_host: false,
                images_upload_url: '/general/uploader',
                images_upload_base_path: '/uploads/answers/images',
                image_caption: true,
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == 'image') {
                        $('#upload').trigger('click');
                        $('#upload').on('change', function () {
                            var file = this.files[0];
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                callback(e.target.result, {
                                    alt: ''
                                });
                            };
                            reader.readAsDataURL(file);
                        });
                    }
                },
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
            });
        });
    </script>
@endsection