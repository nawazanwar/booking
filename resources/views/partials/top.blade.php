@section('pageTitle', $pageTitle)

@if(isset($settings['meta_description']) && $settings['meta_description'])
    @section('metaDescription', $settings['meta_description'])
@endif

@if(isset($settings['meta_keywords']) && $settings['meta_keywords'])
    @section('metaKeywords', $settings['meta_keywords'])
@endif

@section('header')
    @include('includes.header')
@stop