<form class="d-flex justify-content-between align-content-center shadow-9 border pt-2 pl-2">
    <nav class="nav">

        <div class="custom-control custom-checkbox nav-link">
            <input type="checkbox" class="custom-control-input" name="filter[public]" value="1"
                   @if(isset($filters['public']) && $filters['public'] == 1) checked @endif>
            <label class="custom-control-label">{{__('faqs.public')}}</label>
        </div>

        <div class="custom-control custom-checkbox nav-link">
            <input type="checkbox" class="custom-control-input" name="filter[public]" value="0"
                   @if(isset($filters['public']) && $filters['public'] == 0) checked @endif>
            <label class="custom-control-label">{{__('faqs.private')}}</label>
        </div>

        <div class="custom-control custom-checkbox nav-link">
            <input type="checkbox" class="custom-control-input" name="filter[featured]" value="1"
                   @if(isset($filters['featured']) && $filters['featured'] == 1) checked @endif>
            <label class="custom-control-label">{{__('faqs.featured')}}</label>
        </div>

    </nav>

    <nav class="nav">

        <div class="nav-link">
            <input type="text" name="keyword" class="form-control form-control-sm search-xs nav-link"
                   placeholder="Search" autocomplete="off" value="{{$keyword || old('keyword')}}">
        </div>

        <div class="nav-link">
            <select class="form-control form-control-sm" name="field">
                <option value="name" @if(isset($field) and $field == 'name') selected @endif>
                    {{__('faqs.title')}}
                </option>
                <option value="slug"
                        @if(isset($field) and $field == 'slug') selected @endif>
                    {{__('faqs.slug')}}
                </option>
            </select>
        </div>

        <div class="nav-link">
            <button type="submit" class="btn btn-primary search-xs">{{__('faqs.filter')}}</button>
        </div>
        @isset($modelClass)
            @php
                if(isset($permission) && !empty($permission)) {
                $permissionLabel = $permission;
                } else {
                $permissionLabel = 'create';
                }
            @endphp

            @can($permissionLabel, $modelClass)
                @if($type!='front')
                    <div class="nav-link">
                        <a href="{{route($route,[isset($extra_parameters)?$extra_parameters:''])}}"
                           class="btn btn-primary"
                           style="background-image: linear-gradient(120deg, #667eea 0%, #764ba2 100%);">
                            @empty($button)
                                {{__('faqs.create_new')}}
                            @endempty

                            @isset($button)
                                {{$button}}
                            @endisset
                        </a>
                    </div>
                @endif
            @endcan
        @endisset

    </nav>
</form>