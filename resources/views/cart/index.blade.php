@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            @include('partials.messages')
            @if(session('cart'))
                <div class="row gap-y">
                    <div class="col-lg-8">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th class="w-100px">Quantity</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">STotal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody valign="middle">
                            <?php $total = 0 ?>
                            @foreach(session('cart') as $id => $details)
                                @php
                                    if (isset($details['front_image'])){
                                     $front_image = $details['front_image'];
                                    }else{
                                     $front_image = 'img/no_image.png';
                                    }
                                    if (isset($details['back_image'])){
                                        $back_image = $details['back_image'];
                                    }else{
                                        $back_image = 'img/no_image.png';
                                    }
                                @endphp
                                <?php $total += $details['price'] * $details['quantity'] ?>
                                <tr>
                                    <td>
                                        <div class="card overflow-hidden h-80px w-80px">
                                            <div class="card-body"
                                                 style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                                            </div>
                                            <div class="card-hover text-white bg-img" data-animation="slide-down"
                                                 style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                                                 data-overlay="6">
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h5>{{ $details['name'] }}</h5>
                                    </td>
                                    <td>
                                        <input class="form-control form-control-sm quantity" type="number"
                                               placeholder="Quantity"
                                               value="{{ $details['quantity'] }}">
                                    </td>
                                    <td>
                                        {{ $details['price'] }}
                                    </td>
                                    <td>
                                        {{ $details['price'] * $details['quantity'] }}
                                    </td>
                                    <td>
                                        <a class="update-cart cursor-pointer badge" data-id="{{ $id }}"><i
                                                    class="fa fa-refresh text-info"></i></a>
                                        <a class="remove-from-cart cursor-pointer" data-id="{{ $id }}"><i
                                                    class="fa fa-trash-o text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="cart-price">
                            <div class="flexbox">
                                <div>
                                    <p><strong>Subtotal:</strong></p>
                                    <p><strong>Shipping:</strong></p>
                                    <p><strong>Tax (%10):</strong></p>
                                </div>
                                <div>
                                    <p>{{ $total }}</p>
                                    <p>0</p>
                                    <p>0</p>
                                </div>
                            </div>
                            <hr>
                            <div class="flexbox">
                                <div>
                                    <p><strong>Total:</strong></p>
                                </div>
                                <div>
                                    <p class="fw-600">{{ $total }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row gap-y">
                    <h3>Add Extra Information</h3>
                    <div class="col-md-12 mx-auto">
                        <form method="post" action="{{route('cart.proceeded')}}" class="input-border"
                              autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Enter Phone" name="phone"
                                       value="{{Auth::user()->phone}}">
                            </div>
                            <div class="form-group">
                                <label>{{__('faqs.country')}}</label>
                                <select class="form-control" name="country">
                                    @foreach($countries as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{__('faqs.city')}}</label>
                                <select class="form-control" name="city">
                                    @foreach($cities as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{__('faqs.bio')}} (Address)</label>
                                <textarea class="form-control" rows="4" name="bio"
                                          placeholder="Address">{{Auth::user()->bio}}</textarea>
                            </div>
                            <div class="form-group input-group">
                                <input type="text" class="form-control" placeholder="Username" name="username"
                                       value="{{Auth::user()->username}}">
                            </div>
                            <div class="form-group input-group">
                                <input type="text" class="form-control" placeholder="Email address" name="email"
                                       value="{{Auth::user()->email}}">
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-primary" type="submit">Proceeded
                                    Order
                                    <i class="ti-angle-right fs-9"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12 text-center">
                        <strong class="text-center">Cart is Empty</strong>
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(".update-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
            var id = ele.attr("data-id");
            var quantity = ele.parents("tr").find(".quantity").val();
            Ajax.call('{{ route('cart.update') }}', {'id': id, quantity: quantity}, 'patch', function () {
                window.location.reload();
            });
        });
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);
            var id = ele.attr("data-id");
            if (confirm("Are you sure")) {
                Ajax.call('{{route('cart.remove') }}', {'id': id}, 'DELETE', function () {
                    window.location.reload();
                });
            }
        });
    </script>
@endsection