@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <form class="row gap-y">
                @if(session('cart'))
                    <div class="col-lg-8">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th class="w-100px">Quantity</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">STotal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody valign="middle">
                            <?php $total = 0 ?>
                            @if(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                    <?php $total += $details['price'] * $details['quantity'] ?>
                                    <tr>
                                        <td>
                                            <div class="card overflow-hidden h-80px w-80px">
                                                <div class="card-body"
                                                     style="background-image: url({{asset($details['front_image'])}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                                                </div>
                                                <div class="card-hover text-white bg-img" data-animation="slide-down"
                                                     style="background-image: url({{asset($details['back_image'])}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                                                     data-overlay="6">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <h5>{{ $details['name'] }}</h5>
                                        </td>
                                        <td>
                                            <input class="form-control form-control-sm quantity" type="number"
                                                   placeholder="Quantity"
                                                   value="{{ $details['quantity'] }}">
                                        </td>
                                        <td>
                                            {{ $details['price'] }}
                                        </td>
                                        <td>
                                            {{ $details['price'] * $details['quantity'] }}
                                        </td>
                                        <td>
                                            <a class="update-cart cursor-pointer badge" data-id="{{ $id }}"><i
                                                        class="fa fa-refresh text-info"></i></a>
                                            <a class="remove-from-cart cursor-pointer" data-id="{{ $id }}"><i
                                                        class="fa fa-trash-o text-danger"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-4">
                        <div class="cart-price">
                            <div class="flexbox">
                                <div>
                                    <p><strong>Subtotal:</strong></p>
                                    <p><strong>Shipping:</strong></p>
                                    <p><strong>Tax (%10):</strong></p>
                                </div>
                                <div>
                                    <p>{{ $total }}</p>
                                    <p>0</p>
                                    <p>0</p>
                                </div>
                            </div>
                            <hr>
                            <div class="flexbox">
                                <div>
                                    <p><strong>Total:</strong></p>
                                </div>
                                <div>
                                    <p class="fw-600">{{ $total }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <a class="btn btn-block btn-primary" href="{{route('cart.proceeded')}}" type="submit">Proceeded Order
                                    <i class="ti-angle-right fs-9"></i></a>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12 text-center">
                        <strong>Cart is Empty ............</strong>
                    </div>
                @endif
            </form>
        </div>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
    </script>
@endsection
