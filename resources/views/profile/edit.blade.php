@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section">
        <div class="container">

            <form method="post" action="{{route('profile.update', $user->username)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <div class="row gap-y">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('faqs.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('faqs.name')}}"
                                   value="{{old('name', $user->name)}}" tabindex="1" autofocus required>
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.phone')}}</label>
                            <input type="number" name="phone" class="form-control" placeholder="{{__('faqs.phone')}}"
                                   value="{{old('phone', $user->phone)}}" tabindex="3">
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.gender')}}</label>
                            <select class="form-control" name="gender" data-placeholder="{{__('faqs.gender')}}"
                                    tabindex="5">
                                <option value="male" @if($user->gender == 'male') selected @endif>
                                    {{__('faqs.male')}}
                                </option>
                                <option value="female" @if($user->gender == 'female') selected @endif>
                                    {{__('faqs.female')}}
                                </option>
                                <option value="unspecified" @if($user->gender == 'unspecified') selected @endif>
                                    {{__('faqs.unspecified')}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.country')}}</label>
                            <select class="form-control selectpicker show-menu-arrow select-country" name="country"
                                    data-live-search="true" data-liveSearchPlaceholder="{{__('faqs.search_country')}}"
                                    data-noneSelectedText="{{__('faqs.select_country')}}"
                                    data-size="10" data-placeholder="{{__('faqs.country')}}" data-target="#cities"
                                    tabindex="7">
                                <option disabled selected value>{{__('faqs.select_country')}}</option>
                                @foreach($countries as $key => $value)
                                    <option value="{{$key}}"
                                            @if($user->country && $user->country->id == $key) selected @endif >{{$value}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{__('faqs.bio')}}</label>
                            <textarea name="bio" class="form-control" placeholder="{{__('faqs.bio')}}"
                                      rows="7" tabindex="9">{{old('bio', $user->bio)}}</textarea>
                        </div>
                        @can('delete', $user)
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="active" class="custom-control-input" value="1"
                                       @if($user->active) checked @endif tabindex="11">
                                <label class="custom-control-label">{{__('faqs.active')}}</label>
                            </div>
                        @endcan

                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('faqs.username')}}</label>
                            <input type="text" name="username" class="form-control" tabindex="2"
                                   placeholder="{{__('faqs.username')}}" value="{{old('username', $user->username)}}"
                                   required>
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.email')}}</label>
                            <input type="email" name="email" class="form-control" placeholder="{{__('faqs.email')}}"
                                   value="{{old('email', $user->email)}}" tabindex="4" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.password')}}</label>
                            <input type="password" name="password" class="form-control"
                                   placeholder="{{__('faqs.password')}}" tabindex="6">
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.city')}}</label>
                            <select class="form-control selectpicker show-menu-arrow" id="cities" name="city"
                                    data-live-search="true" data-liveSearchPlaceholder="{{__('faqs.search_city')}}"
                                    data-noneSelectedText="{{__('faqs.select_city')}}"
                                    data-size="10" data-placeholder="{{__('faqs.city')}}"
                                    tabindex="8">
                                <option disabled selected value>{{__('faqs.select_city')}}</option>
                                @foreach($cities as $key => $value)
                                    <option value="{{$key}}"
                                            @if($user->city && $user->city->id == $key) selected @endif >{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('faqs.avatar')}}</label>
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="customFile"
                                       accept=".png, .jpg, .jpeg" tabindex="10">
                                <label class="custom-file-label"
                                       for="customFile">{{__('faqs.choose_new_avatar')}}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="delete_avatar" class="custom-control-input" value="1">
                                <label class="custom-control-label">{{__('faqs.delete_avatar')}}</label>
                            </div>
                        </div>

                        <div class="text-center">
                            <img class="w-150 h-150 rounded-circle border" src="{{$user->avatar}}" alt="avatar">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        @can('delete', $user)
                            <a href="{{route('muntazim.user.delete', $user->id)}}" class="btn btn-label btn-danger"
                               onclick="return confirm('{{__('faqs.are_you_sure_to_delete')}}');">
                                <label><i class="fa fa-trash"></i></label> {{__('faqs.delete_user')}}
                            </a>
                        @endcan
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="pull-right">
                            <a href="{{route('profile', $user->username)}}"
                               class="btn btn-label btn-secondary">
                                <label><i class="fa fa-user"></i></label> {{__('faqs.profile')}}
                            </a>
                            <button type="submit" class="btn btn-label btn-primary" tabindex="12">
                                <label><i class="fa fa-save"></i></label> {{__('faqs.update')}}
                            </button>
                        </div>
                    </div>

                </div>
            </form>

        </div>
    </section>
@endsection