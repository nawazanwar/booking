@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    {{--<header class="header bg-gradient-dark text-white">
    </header>--}}
    <section class="section">
        <div class="container">

            <div class="row gap-y">
                <div class="col-md-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="container text-center">
                                <img src="{{$user->avatar}}" class="profile-image img-responsive rounded-circle border">
                            </div>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.name')}}
                            <span class="badge pull-right">{{$user->name}}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.username')}}
                            <span class="badge pull-right">{{$user->username}}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.email')}}
                            <span class="badge pull-right">{{$user->email}}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.gender')}}
                            <span class="badge pull-right">{!! $user->genderIcon() !!}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.country')}}
                            <span class="badge pull-right">{{ $user->country->name }}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.city')}}
                            <span class="badge pull-right">{{ $user->city->name }}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.active')}}
                            @if($user->active)
                                <span class="badge badge-success pull-right">{{__('faqs.active')}}</span>
                            @else
                                <span class="badge badge-danger pull-right">{{__('faqs.inactive')}}</span>
                            @endif
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.created_date')}}
                            <span class="badge pull-right">{{$user->created_at->diffForHumans()}}</span>
                        </li>
                        <li class="list-group-item">
                            {{__('faqs.updated_date')}}
                            <span class="badge pull-right">{{$user->updated_at->diffForHumans()}}</span>
                        </li>
                        @can('update', $user)
                            <li class="list-group-item text-center">
                                <a class="btn btn-xs btn-primary btn-block"
                                   href="{{route('profile.edit',[$user->username])}}">{{__('faqs.edit')}}</a>

                            </li>
                        @endcan
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="tab-content p-4">
                        @if(Route::is('profile'))
                            @include('profile.my_answers', ['answers' => $user->answers])
                        @elseif(Route::is('profile.questions'))
                            @include('profile.my_questions', ['questions' => $user->questions])
                        @elseif(Route::is('profile.upvotes'))
                            @include('profile.upvotes', ['questions' => $user->questions])
                        @elseif(Route::is('profile.downvotes'))
                            @include('profile.downvotes', ['questions' => $user->questions])
                        @elseif(Route::is('profile.favorite'))
                            @include('profile.favorite', ['questions' => $user->questions])
                        @elseif(Route::is('profile.following'))
                            @include('profile.following', ['questions' => $user->questions])
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <ul class="nav nav-pills flex-column" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile')) active @endif"
                               href="{{route('profile', $user->username)}}">{{__('faqs.my_answers')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile.questions')) active @endif"
                               href="{{route('profile.questions', $user->username)}}">{{__('faqs.my_questions')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile.upvotes')) active @endif"
                               href="#">{{__('faqs.upvotes')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile.downvotes')) active @endif"
                               href="#">{{__('faqs.downvotes')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile.favorite')) active @endif"
                               href="#">{{__('faqs.favorite')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(Route::is('profile.following')) active @endif"
                               href="#">{{__('faqs.following')}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
@endsection