{{--Start of the Questions List section--}}
@if($answers->count())
    @foreach($answers as $answer)
        @include('components.answer', ['showQuestion' => true])
    @endforeach
@endif
{{--End of the Questions List Section--}}