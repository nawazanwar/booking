@extends('layouts.muntazim')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <form class="form-inline">
                <div class="box-tools">

                    <div class="pull-left">
                        <label>
                            <input name="filter[active]" value="1" type="checkbox" @if(isset($filters['active']) && $filters['active'] == 1) checked @endif>
                            <span class="label label-success">Active</span>
                        </label>
                        <label>
                            <input name="filter[active]" value="0" type="checkbox" @if(isset($filters['active']) && $filters['active'] == 0) checked @endif>
                            <span class="label label-danger">Not Active</span>
                        </label>
                        <label>
                            <input name="filter[public]" value="1" type="checkbox" @if(isset($filters['public'])) checked @endif>
                            <span class="label bg-teal">Public</span>
                        </label>
                        <label>
                            <input name="filter[featured]" value="1" type="checkbox" @if(isset($filters['featured'])) checked @endif>
                            <span class="label bg-navy">Featured</span>
                        </label>
                        &nbsp;&nbsp;
                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                    </div>
                    <div class="pull-right">
                        <div class="form-group">
                            <label>Search in: </label>
                            <select class="form-control" name="field">
                                <option value="title" @if($field == 'title') selected @endif>Title</option>
                                <option value="slug" @if($field == 'slug') selected @endif>Slug</option>
                                <option value="content" @if($field == 'content') selected @endif>Content</option>
                                <option value="excerpt" @if($field == 'excerpt') selected @endif>Excerpt</option>
                                <option value="meta_keywords" @if($field == 'meta_keywords') selected @endif>Meta
                                    Keywords
                                </option>
                                <option value="meta_description" @if($field == 'meta_description') selected @endif>Meta
                                    Description
                                </option>
                            </select>
                        </div>

                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="keyword" class="form-control" placeholder="Search"
                                   value="{{$keyword}}">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                        <a class="btn btn-sm btn-success" href="{{route('muntazim.pages')}}">Clear All Filters</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
        <div class="box-body">

            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))
                <div class="callout callout-success">
                    <p>{{ $successMessage }}</p>
                </div>
            @endif

            <table id="pageListTbl" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th width="100">Title</th>
                   
                    <th width="200">Content</th>
                    <th>Misc</th>
                    <th>Created At</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @if($pageCount)
                    @foreach($pages as $page)
                        <tr>
                            <td>{{$page->id}}</td>
                            <td>{{$page->title}}</td>
                            
                            <td>{{$page->excerpt}}</td>
                            <td>
                                @if($page->active)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-danger">Not Active</span>
                                @endif

                                @if($page->public)
                                    <span class="label bg-teal">Public</span>
                                @endif

                               

                                <span class="label bg-maroon">
                                {{$page->categoryName()}}
                            </span>

                            </td>
                            <td>{{$page->created_at}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{route('home.page', ['slug' => $page->slug])}}"
                                               target="_blank">View</a></li>
                                        <li><a href="{{route('muntazim.pages.edit', [$page->id])}}">Edit</a></li>
                                        <li><a onclick="return confirm('Are you sure you want to delete this item?');" href="{{route('muntazim.pages.delete', [$page->id])}}">Delete</a></li>
                                        <li>
                                            <a href="#" class="share-on-fb"
                                               data-href="{{route('home.page', ['slug' => $page->slug])}}">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="share-on-twitter" href="https://twitter.com/share"
                                               data-size="large"
                                               data-text="{{$page->title}}"
                     
                                               data-hashtags="{{$page->hashtag}}"
                                               data-via="AlifBeyTech"
                                               data-related="{{$page->related}}">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.pinterest.com/pin/create/button/"
                                               data-pin-do="buttonPin"
                                               data-pin-custom="true" data-pin-description="{{$page->excerpt}}"
                                               
                                               
                                                <i class="fa fa-pinterest"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" class="text-center">No data found.</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div><!-- /.box-body -->

        <div class="box-footer with-border">
            <div class="pull-left">
                <a href="{{route('muntazim.pages.create')}}" class="btn btn-sm btn-primary btn-flat">
                    Create New Page
                </a>
            </div>

            <div class="pull-right">
                @if($field)
                    {{$pages->appends(['field' => $field, 'keyword' => $keyword, 'filter' => $filters])->links('muntazim.paginator')}}
                @else
                    {{$pages->links('muntazim.paginator')}}
                @endif
            </div>

            <div class="clear"></div>
        </div><!-- /.box-header -->
    </div><!-- /.box -->
@stop
