@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section pt-5 pb-5">
        <div class="container">

            <form method="post" action="{{route('muntazim.settings.update')}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                @if(!count($settings))
                    <div class="alert alert-warning" role="alert">
                        {{__('meme.no_data_available')}}
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-8 mx-auto">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab-general">General</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-heading">Headings</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-logo">Logo</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-slide">Slides</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab-social">Social Urls</a>
                                </li>

                            </ul>
                            <div class="tab-content p-4">
                                <div class="tab-pane fade show active" id="tab-general">
                                    @foreach($settings as $setting)
                                        @if ($setting->type == 'checkbox')
                                            <div class="form-group">
                                                <div class="switch">
                                                    <input type="checkbox" name="{{$setting->name}}"
                                                           class="switch-input" value="1"
                                                           @if($setting->value) checked @endif>
                                                    <label class="switch-label">{{$setting->name}}</label>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="tab-heading">
                                    @foreach($settings as $setting)
                                        @if ($setting->name == 'first-heading')
                                            <div class="form-group">
                                                <label for="settingValue{{$setting->id}}">{{$setting->name}}</label>
                                                <input type="text" name="{{$setting->name}}" class="form-control"
                                                       id="settingValue{{$setting->id}}" value="{{$setting->value}}">
                                            </div>
                                        @elseif($setting->name == 'second-heading' OR $setting->name == 'third-heading')
                                            <div class="form-group">
                                                <label for="settingValue{{$setting->id}}">{{$setting->name}}</label>
                                                <textarea name="{{$setting->name}}" class="form-control" rows="5"
                                                          id="settingValue{{$setting->id}}">{{$setting->value}}</textarea>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="tab-logo">
                                    @foreach($settings as $setting)
                                        @if ($setting->name == 'site-logo' OR
                                         $setting->name == 'favicon-logo' OR
                                         $setting->name == 'cinema-logo' OR
                                         $setting->name == 'terminal-logo' OR
                                         $setting->name == 'food-point-logo')
                                            <div class="form-group">
                                                <label for="settingValue{{$setting->id}}">{{$setting->name}}</label>
                                                <div class="custom-file">
                                                    <input type="file" name="{{$setting->name}}"
                                                           class="custom-file-input"
                                                           id="{{$setting->id}}"
                                                           accept=".png, .jpg, .jpeg">
                                                    <label class="custom-file-label"
                                                           for="customFile">{{__('faqs.choose_new_avatar')}}</label>
                                                </div>
                                                @if($setting->value)
                                                    <div style="margin-top:20px;">
                                                        <img class="img-thumbnail"
                                                             src="{{asset($setting->value)}}">
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="tab-pane fade" id="tab-slide">
                                    @foreach($settings as $setting)
                                        @if ($setting->name == 'first-slide' OR
                                         $setting->name == 'second-slide' OR
                                         $setting->name == 'third-slide' OR
                                         $setting->name == 'fourth-slide' OR
                                         $setting->name == 'fifth-slide')
                                            <div class="form-group">
                                                <label for="settingValue{{$setting->id}}">{{$setting->name}}</label>
                                                <div class="custom-file">
                                                    <input type="file" name="{{$setting->name}}"
                                                           class="custom-file-input"
                                                           id="{{$setting->id}}"
                                                           accept=".png, .jpg, .jpeg">
                                                    <label class="custom-file-label"
                                                           for="customFile">{{__('faqs.choose_new_avatar')}}</label>
                                                </div>
                                                @if($setting->value)
                                                    <div style="margin-top:20px;">
                                                        <img class="img-thumbnail"
                                                             src="{{asset($setting->value)}}">
                                                    </div>
                                                @endif
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                                <div class="tab-pane fade" id="tab-social">
                                    @foreach($settings as $setting)
                                        @if ($setting->name == 'facebook' OR
                                         $setting->name == 'instagram' OR
                                         $setting->name == 'googleplus' OR
                                         $setting->name == 'vimeo' OR
                                         $setting->name == 'youtube')
                                            <div class="form-group">
                                                <label for="settingValue{{$setting->id}}">{{$setting->name}}</label>
                                                <input type="text" name="{{$setting->name}}" class="form-control"
                                                       id="settingValue{{$setting->id}}" value="{{$setting->value}}">
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="pull-right">
                                <button type="submit" class="btn btn-label btn-primary">
                                    <label><i class="fa fa-save"></i></label> {{__('faqs.update')}}
                                </button>
                            </div>
                        </div>
                    </div>
                @endif
            </form>
        </div>
    </section>
@stop
