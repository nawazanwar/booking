@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section">
        <div class="container">
            <h3>No Permission to Access this Route</h3>
        </div>
    </section>
@stop