@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            <!-- form start -->
            {!! Form::model($category, ['route' => ['muntazim.categories.update', $category->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            {!! Form::hidden('parent_id',$parent_id, ['class' => 'form-control']) !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab-general">General</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-detail">Detail</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-properties">Properties</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-media">Media</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-slide">Slides</a>
                        </li>
                    </ul>

                    <div class="tab-content p-4">
                        <div class="tab-pane fade show active" id="tab-general">

                            <div class="form-group">
                                {!! Form::label('type', 'Type') !!}
                                <select class="form-control" name="type">
                                    <option value="cinema">Cinema</option>
                                    <option value="terminal">Terminal</option>
                                    <option value="food">Food</option>
                                </select>
                            </div>

                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', null, ['class' => 'form-control',  'id' => 'name' ]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('slug', 'Slug') !!}
                                {!! Form::text('slug', null, ['class' => 'form-control',  'id' => 'slug' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('seats', 'Seats') !!}
                                {!! Form::text('seats', null, ['class' => 'form-control',  'id' => 'seats' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('order', 'Order') !!}
                                {!! Form::text('order', null, ['class' => 'form-control', 'value' => 0,  'id' => 'order' ]) !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-properties">

                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('status', 1, true) !!} Active
                                </label>
                            </div>

                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('featured', 1) !!} Featured
                                </label>
                            </div>

                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('breaking', 1) !!} Breaking
                                </label>
                            </div>

                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('tag', 1) !!} Tag
                                </label>
                            </div>

                            <div class="form-group">
                                <label>
                                    {!! Form::checkbox('public', 1, true) !!} Public
                                </label>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="tab-detail">

                            <div class="form-group">
                                {!! Form::label('content', 'Content') !!}
                                {!! Form::textarea('content', null, ['class' => 'form-control','rows' => 4]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('excerpt', 'Excerpt') !!}
                                {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' => 4]) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::label('meta_keywords', 'Meta Keywords') !!}
                                {!! Form::textarea('meta_keywords', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'meta_keywords' ]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_description', 'Meta Description') !!}
                                {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'meta_description' ]) !!}
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-media">

                            <div class="form-group">
                                <label>Front Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="front"
                                           accept=".png, .jpg, .jpeg" name="image[front]" onchange="loadFile(event)">
                                    <label class="custom-file-label" for="front">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[0]->src)?$category->media[0]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group">
                                <label>Back Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="back"
                                           accept=".png, .jpg, .jpeg" name="image[back]" onchange="loadFile(event)">
                                    <label class="custom-file-label" for="front">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[1]->src)?$category->media[1]->src:'img/no_image.png')}}">
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-slide">
                            <div class="form-group">
                                <label>First Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="first_slide"
                                           accept=".png, .jpg, .jpeg" name="image[first_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="first_slide">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[2]->src)?$category->media[2]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group">
                                <label>Second Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="second_slide"
                                           accept=".png, .jpg, .jpeg" name="image[second_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="second_slide">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[3]->src)?$category->media[3]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group">
                                <label>Third Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="third_slide"
                                           accept=".png, .jpg, .jpeg" name="image[third_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="third_slide">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[4]->src)?$category->media[4]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group">
                                <label>Fourth Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fourth_slide"
                                           accept=".png, .jpg, .jpeg" name="image[fourth_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="fourth_slide">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[5]->src)?$category->media[5]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group">
                                <label>Fifth Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fifth_slide"
                                           accept=".png, .jpg, .jpeg" name="image[fifth_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="fifth_slide">Choose file</label>
                                </div>
                                <img class="w-250 h-200 my-2 img-thumbnail"
                                     src="{{asset(isset($category->media[6]->src)?$category->media[6]->src:'img/no_image.png')}}">
                            </div>
                            <div class="form-group text-right">
                                {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                            </div>
                        </div>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('pageScript')
@stop