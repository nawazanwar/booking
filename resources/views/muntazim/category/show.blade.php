@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="row gap-y">

                @if(!isset($category->id))
                    <div class="alert alert-danger">Invalid Category!</div>
                @else
                    @include('components.category.detail',['data'=>$category,'type'=>'muntazim'])
                @endif
            </div>
        </div>
    </section>
@stop