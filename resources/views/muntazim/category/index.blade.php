@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="row mt-5">
                @if(count($categories))
                    @foreach($categories as $category)
                        @include('components.category.list',['data'=>$category,'type'=>'muntazim'])
                    @endforeach
                @endif
            </div>
        </div>
    </section><!-- /.box -->
@stop