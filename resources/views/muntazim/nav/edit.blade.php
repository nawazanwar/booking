@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('faqs.create_nav')}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>
    
    <section class="section">
        <div class="container">

            <form method="post" action="{{route('muntazim.nav.update', $nav->id)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{method_field('PUT')}}
                {{ csrf_field() }}

                <div class="row gap-y">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>{{__('faqs.identifier')}}</label>
                            <input type="text" name="identifier" class="form-control" placeholder="{{__('faqs.identifier')}}"
                                   value="{{old('identifier', $nav->identifier)}}" autofocus>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1"
                                   @if($nav->active) checked @endif>
                            <label class="custom-control-label">{{__('faqs.active')}}</label>
                        </div>
                    </div>
                </div>

                <div class="pull-right">
                    <a href="{{route('muntazim.nav')}}"
                       class="btn btn-label btn-secondary">
                        <label><i class="fa fa-table"></i></label> {{__('faqs.all_navs')}}
                    </a>
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('faqs.update')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop