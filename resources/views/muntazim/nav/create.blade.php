@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')

    <section class="section py-6">
        <div class="container">
            @include('partials.messages')
            <form method="post" action="{{route('muntazim.nav.store')}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="row gap-y">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>{{__('faqs.identifier')}}</label>
                            <input type="text" name="identifier" class="form-control"
                                   placeholder="{{__('faqs.identifier')}}"
                                   value="{{old('identifier')}}" autofocus>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1" checked
                                   @if(old('active')) checked @endif>
                            <label class="custom-control-label">{{__('faqs.active')}}</label>
                        </div>
                    </div>
                </div>

                <div class="pull-right">
                    <a href="{{route('muntazim.nav')}}"
                       class="btn btn-label btn-secondary">
                        <label><i class="fa fa-table"></i></label> {{__('faqs.all_navs')}}
                    </a>
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('faqs.create')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop