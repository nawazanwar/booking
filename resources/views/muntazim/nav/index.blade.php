@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="row gap-y">
                {{-- filters --}}
                @include('partials.filters', ['modelClass' => \App\Models\Nav::class, 'route' => 'muntazim.nav.create','type'=>'muntazim','extra_parameters'=>''])

                <table class="table table-striped table-bordered my-5">
                    <thead>
                    <tr>
                        <th>{{__('faqs.identifier')}}</th>
                        <th class="text-center">{{__('faqs.misc')}}</th>
                        <th class="text-center">{{__('faqs.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($navs))
                        @foreach($navs as $nav)
                            <tr>
                                <td>{{$nav->identifier}}</td>
                                <td class="text-center">
                                    {{--Active--}}
                                    @if($nav->active)
                                        <span class="badge btn-info">{{__('faqs.active')}}</span>
                                    @else
                                        <span class="badge badge-danger">{{__('faqs.inactive')}}</span>
                                    @endif

                                </td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <span class="btn btn-primary bg-gradient-primary dropdown-toggle"
                                          data-toggle="dropdown">{{__('faqs.options')}}</span>
                                        <div class="dropdown-menu">
                                            @can('view', $nav)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.nav.links', $nav->id)}}">{{__('faqs.links')}}</a>
                                            @endcan
                                            @can('update', $nav)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.nav.edit', $nav->id)}}">{{__('faqs.edit')}}</a>
                                            @endcan
                                            @can('delete', $nav)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.nav.delete',[$nav->id])}}"
                                                   onclick="return confirm('{{__('faqs.are_you_sure_to_delete')}}');">{{__('faqs.delete')}}</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $navs->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>

        </div>
    </section><!-- /.box -->
@endsection