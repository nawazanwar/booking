@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')

    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('faqs.navigation_links')}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>

    <section class="section">
        <div class="container">

            <form method="post" action="{{route('muntazim.nav.links.store', $navId)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{method_field('PUT')}}
                {{ csrf_field() }}
                <input name="nav_id" type="hidden" value="{{$navId}}">

                <table class="table table-striped table-bordered" id="navTbl">
                    <tbody>
                    @if(count($navLinks) > 0)
                        <?php $index = 0; ?>
                        @foreach($navLinks as $item)
                            <tr>
                                <td>
                                    <input type="hidden" name="id[]" id="id_{{$item->id}}"
                                           value="{{$item->id}}">
                                    <div class="form-group">
                                        <input type="text" name="title[]" class="form-control"
                                               placeholder="{{__('faqs.title')}}" id="title_{{$item->id}}"
                                               value="{{$item->title}}" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="url" name="url[]" class="form-control"
                                               placeholder="{{__('faqs.url')}}" id="url_{{$item->id}}"
                                               value="{{$item->url}}" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <input type="number" name="order[]" class="form-control"
                                               placeholder="{{__('faqs.order')}}" id="order_{{$item->id}}" min="1"
                                               value="{{$item->order}}" required>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <div class="switch">
                                            <input type="checkbox" name="is_blank[{{$index}}]" class="switch-input"
                                                   id="is_blank_{{$item->id}}" value="1" data-id="{{$item->id}}"
                                                   @if($item->is_blank) checked @endif>
                                            <label class="switch-label">{{__('faqs.open_in_new_window')}}</label>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-nav-add-row"><i class="fa fa-plus"></i>
                                    </button>
                                    <button class="btn btn-danger btn-nav-remove-row"><i class="fa fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                            <?php $index++; ?>
                        @endforeach
                    @else
                        <tr>
                            <td>
                                <input type="hidden" name="id[]" id="id_0">
                                <div class="form-group">
                                    <input type="text" name="title[]" class="form-control"
                                           placeholder="{{__('faqs.title')}}"
                                           id="title_0" required>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="url" name="url[]" class="form-control" placeholder="{{__('faqs.url')}}"
                                           id="url_0" required>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="order[]" class="form-control"
                                           placeholder="{{__('faqs.order')}}"
                                           id="order_0" min="0" required>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <div class="switch">
                                        <input type="checkbox" name="is_blank[]" class="switch-input"
                                               id="is_blank_0" value="1">
                                        <label class="switch-label">{{__('faqs.open_in_new_window')}}</label>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary btn-nav-add-row"><i
                                                class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-danger btn-nav-remove-row"><i
                                                class="fa fa-minus"></i></button>
                                </div>
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="pull-right">
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('faqs.update')}}
                    </button>
                </div>
            </form>

        </div>
    </section>
@stop

@section('scripts')
    <script type="text/javascript">
        (function ($) {

            $(document).on('click', ".btn-nav-add-row", function (event) {
                event.preventDefault();

                var $tr = $(this).closest('tr');
                var $clone = $tr.clone();
                var itemId = $clone.find('input[type=checkbox]').data('id');

                $clone.find('input[type=checkbox]').attr('id', 'id_' + (itemId + 1));
                $clone.find('[id^=id_]').val('');

                $tr.after($clone);

            });

            $(document).on('click', ".btn-nav-remove-row", function (event) {
                event.preventDefault();

                var rowCount = $('#navTbl tr').length;

                if (rowCount > 1) {
                    var $tr = $(this).closest('tr');
                    $tr.remove();
                }

            });

        })(jQuery);
    </script>
@endsection