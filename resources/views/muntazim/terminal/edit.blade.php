@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <!-- form start -->
            yar mujya edir kerddo please
            {!! Form::model($modal, ['route' => ['muntazim.terminals.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            @include('partials.messages')
            <div class="row">
                <input type="hidden" name="up_type" value="detail">
                <div class="col-md-8 mx-auto">
                    <h5 class="text-center">Add detail of <strong class="text-primary">{{$modal->name}}</strong></h5>
                    <p><strong>Located in :</strong> {{$modal->city->name}}</p>
                    <div class="form-group">
                        {!! Form::label('address', 'Address') !!}
                        {!! Form::textarea('address',null,['class'=>'form-control', 'rows' => 2, 'cols' => 40,'id'=>'tinyMCE']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!} (<span class="text-info">Branch Name</span>)
                        {!! Form::text('name', null, ['class' => 'form-control',  'id' => 'name' ]) !!}
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('active', 1, ($modal->active ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Active</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('public', 1, ($modal->public ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Public</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('featured', 1, ($modal->featured ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Featured</label>
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('scripts')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=o23blg9ah1zca8fzz4hpnqndoxanbov1pv288bvw0ptznp19"></script>
    <script type="text/javascript">
        $(function () {
            // Replace the textarea with tinyMCE
            tinymce.init({
                selector: '#tinyMCE',
                height: 100,
                theme: 'modern',
                plugins: [
                    "advlist autolink lists link  charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern "
                ],
                toolbar1: "",
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        });
    </script>
@stop