@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            <!-- form start -->
            {!! Form::open(array('route' => array('muntazim.posts.store',$type), 'files' => true, 'class' => 'section-top')) !!}
            {!! csrf_field() !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="form-group">
                        {!! Form::label('categories', 'Categories') !!}
                        <select class="form-control selectpicker" name="categories[]" id="categories" multiple>
                            @foreach($data as $d)
                                <option value="{{$d->id}}">{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!}
                        {!! Form::text('name', null, ['class' => 'form-control',  'id' => 'name' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('slug', 'Slug') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control',  'id' => 'slug' ]) !!}
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="default_image"
                                   accept=".png, .jpg, .jpeg" name="image" onchange="loadFile(event)">
                            <label class="custom-file-label" for="default_image">Choose file</label>
                        </div>
                        <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop
@section('pageScript')
@stop