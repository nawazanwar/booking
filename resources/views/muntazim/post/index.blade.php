@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="row mt-5">
                <div class="col-12 col-md-4">
                    <div class="card d-block border mb-6 shadow-9">
                        <div class="card-img-top">
                            <img src="{{asset('img/movie_banner.jpg')}}" alt="Card image cap">
                            <div class="badges badges-right">
                                <a class="badge badge-info" href="{{route('muntazim.movies')}}">Movies</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card d-block border mb-6 shadow-9">
                        <div class="card-img-top">
                            <img src="{{asset('img/vehicle_banner.jpg')}}" alt="Card image cap">
                            <div class="badges badges-right">
                                <a class="badge badge-info"
                                   href="{{route('muntazim.vehicles')}}">Vehicles</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="card d-block border mb-6 shadow-9">
                        <div class="card-img-top">
                            <img src="{{asset('img/food_banner.jpg')}}" alt="Card image cap">
                            <div class="badges badges-right">
                                <a class="badge badge-info"
                                   href="{{route('muntazim.foods')}}">Foods</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.box -->
@stop