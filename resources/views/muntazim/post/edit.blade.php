@extends('layouts.muntazim')
@section('pageTitle', $pageTitle)

@section('bodyClass', 'skin-green sidebar-mini')

@section('breadcrumbs')
    @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
@stop

@section('content')

    <div class="row">
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))
            <div class="callout callout-success">
                <p>{{ $successMessage }}</p>
            </div>
        @endif
    </div>

    <!-- form start -->
    {!! Form::model($post, ['route' => ['muntazim.posts.update', $post->id], 'method' => 'PUT', 'files' => true] ) !!}
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        {!! Form::label('content', 'Content') !!}
                        {!! Form::textarea('content', null, ['class' => 'form-control', 'id' => 'tinyMCE' ]) !!}
                        <input name="image" type="file" id="upload" class="hidden" onchange="">
                    </div>

                    <div class="form-group">
                        {!! Form::label('excerpt', 'Excerpt') !!}
                        {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'dir' => 'rtl', 'rows' => 15, 'id' => 'excerpt' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('image', 'Image') !!}
                        {!! Form::file('image', null, ['class' => 'form-control', 'id' => 'image' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('image', 'Social Media Image') !!}
                        {!! Form::file('social_media_image', null, ['class' => 'form-control', 'id' => 'social-media-image' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::hidden('current_image', $post->getImage('full'), ['class' => 'form-control', 'id' => 'current_image_text' ]) !!}
                    </div>

                    <div class="form-group" id="current_image_div">
                        {!! Form::label('current_image', 'Selected Image') !!}
                        <img src="{{$post->getImage('full')}}" class="img-responsive" id="current_image">
                        <a href="javascript:void(0)" id="delete-img">Remove</a>
                    </div>

                    <div class="form-group">
                        {!! Form::hidden('current_social_media_image', $post->getSocialMediaImage(), ['class' => 'form-control', 'id' => 'current_social_media_image_text' ]) !!}
                    </div>

                    <div class="form-group" id="current_social_media_image_div">
                        {!! Form::label('current_social_media_image', 'Selected Social Media Image') !!}
                        <img src="{{$post->getSocialMediaImage()}}" class="img-responsive" id="current_image">
                        <a href="javascript:void(0)" id="delete-social-media-img">Remove</a>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>

        <div class="col-md-4">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('title', 'Title') !!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'dir' => 'rtl', 'required', 'id' => 'title' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('slug', 'Slug') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control', 'dir' => 'rtl', 'id' => 'slug' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('category', 'Category') !!}
                        {!! Form::select('category', $categories, isset($post->categories[0]) ? $post->categories[0]->id : null, ['class' => 'form-control', 'required', 'id' => 'category' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('type', 'Type') !!}
                        {!! Form::select('type', ['article' => 'Article', 'slides' => 'Slides', 'video' => 'Video'], 'article',
                        ['class' => 'form-control', 'required', 'id' => 'type' ]) !!}
                    </div>

                    <div class="form-group">
                        <label>
                            {!! Form::checkbox('status', 1, $post->active ? true : false) !!}
                            Active
                        </label>
                    </div>
                    <div class="form-group">
                        {!! Form::hidden('status_active', $post->active) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::hidden('status_public', $post->public) !!}
                    </div>

                    <div class="form-group">
                        <label>
                            {!! Form::checkbox('featured', 1, $post->featured ? true : false) !!}
                            Featured
                        </label>
                    </div>

                    <div class="form-group">
                        <label>
                            {!! Form::checkbox('public', 1, $post->public ? true : false) !!}
                            Public
                        </label>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Social Media</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('hashtag', '#HashTags') !!}
                        {!! Form::text('hashtag', null, ['class' => 'form-control', 'id' => 'hashtag' ]) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('related', 'Related') !!}
                        {!! Form::text('related', null, ['class' => 'form-control', 'id' => 'related' ]) !!}
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Meta</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('meta_keywords', 'Meta Keywords') !!}
                        {!! Form::textarea('meta_keywords', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'meta_keywords' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('meta_description', 'Meta Description') !!}
                        {!! Form::textarea('meta_description', null, ['class' => 'form-control', 'rows' => 5, 'id' => 'meta_description' ]) !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>

    <div class="row">
        <div class="box no-border">
            <div class="box-footer">
                {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
            </div>
        </div><!-- /.box -->
    </div>
    {!! Form::close() !!}

@stop

@section('pageScript')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=o23blg9ah1zca8fzz4hpnqndoxanbov1pv288bvw0ptznp19"></script>
    <script type="text/javascript">
        $(function () {
            // Replace the textarea with tinyMCE
            tinymce.init({
                selector: '#tinyMCE',
                height: 500,
                theme: 'modern',
                directionality: 'rtl',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern image imagetools"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
                relative_urls: false,
                remove_script_host: false,
                images_upload_url: '/muntazim/posts/uploader',
                images_upload_base_path: '/uploads/posts',
                image_caption: true,
                file_picker_callback: function (callback, value, meta) {
                    if (meta.filetype == 'image') {
                        $('#upload').trigger('click');
                        $('#upload').on('change', function () {
                            var file = this.files[0];
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                callback(e.target.result, {
                                    alt: ''
                                });
                            };
                            reader.readAsDataURL(file);
                        });
                    }
                },
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css'
                ]
            });
        });

        $("#delete-img").on("click", function () {
            $("#current_image_div").remove();
            $("#current_image_text").val('');
        });

        $("#delete-social-media-img").on("click", function () {
            $("#current_social_media_image_div").remove();
            $("#current_social_media_image_text").val('');
        });

    </script>
@stop