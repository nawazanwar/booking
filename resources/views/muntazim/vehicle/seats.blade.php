@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-auto my-4">
                    <h5 class="text-center">Available seats of <strong class="text-primary">{{$modal->name}}</strong>
                    </h5>
                    <p class="m-0"><strong>Start City:</strong> {{$modal->terminal->city->name}}</p>
                    <p class="m-0"><strong>Start Time:</strong> {{$modal->time}} daily</p>
                    <p class="m-0"><strong>Start Terminal:</strong> {{$modal->terminal->name}}</p>
                    <p class="m-0"><strong>Total Seats:</strong> {{$modal->seats}}</p>
                    <div class="gap-x-3 lh-1 text-center py-5" style="background-color: #8b969e;">
                        @for($i =1; $i<=$modal->seats;$i++)
                            <div class="w-90px m-3 p-3 product-3 product-media text-center">
                                <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                <span class="badge badge-pill badge-info badge-pos-right"
                                      id="booked-icon-front-{{$i}}"><i
                                            class="fa fa-ticket"></i></span>
                                <a class="badge badge-primary cursor-pointer text-white mt-2 mb-1"
                                   style="margin-left: -7px !important;"
                                   id="seat-{{$i}}">Booked It</a>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
@stop