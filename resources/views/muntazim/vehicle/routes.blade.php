@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <!-- form start -->
            {!! Form::model($modal, ['route' => ['muntazim.vehicles.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            {!! Form::hidden('up_type','routes', ['class' => 'form-control']) !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h5 class="text-center">Add Routes of <strong class="text-primary">{{$modal->name}}</strong></h5>
                    <p class="m-0"><strong>Start City:</strong> {{$modal->terminal->city->name}}</p>
                    <p class="m-0"><strong>Start Time:</strong> {{$modal->time}} daily</p>
                    <p class="m-0"><strong>Start Terminal:</strong> {{$modal->terminal->name}}</p>
                    <table class="table table-responsive table-bordered mt-3" id="navTbl">
                        <thead>
                        <tr>
                            <th class="text-center"><label class="col-form-label">City</label></th>
                            <th class="text-center"><label class="col-form-label">R Time</label></th>
                            <th class="text-center"><label class="col-form-label">Duration</label></th>
                            <th class="text-center"><label class="col-form-label">Ticket</label></th>
                            <th class="text-center"><label class="col-form-label">Action</label></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($routes) > 0)
                            <?php $index = 0; ?>
                            @foreach($routes as $item)
                                <tr>
                                    <td>
                                        {{ Form::select('city[]',$cities,$item->city_id, array('class' => 'form-control','id'=>'city_'.$item->id)) }}
                                    </td>
                                    <td>
                                        <input type="hidden" name="id[]" id="id_{{$item->id}}" dir="rtl"
                                               value="{{$item->id}}">
                                        <input type="text" name="time[]" class="form-control timepicker"
                                               placeholder="time AM/PM" value="{{$item->time}}"
                                               id="time_{{$item->id}}" required autocomplete="off">
                                    </td>
                                    <td>
                                        <input type="text" name="duration[]" value="{{$item->duration}}"
                                               class="form-control" placeholder="In Minutes"
                                               id="duration_{{$item->id}}" required autocomplete="off">
                                    </td>
                                    <td>
                                        <input type="text" name="ticket[]" value="{{$item->ticket}}"
                                               class="form-control" placeholder="In Rupees"
                                               id="ticket_{{$item->id}}" required autocomplete="off">
                                    </td>
                                    <td>
                                        <a class="btn-nav-add-row cursor-pointer"><i
                                                    class="fa fa-plus text-primary"></i></a>
                                        <a class="btn-nav-remove-row cursor-pointer"><i
                                                    class="fa fa-minus text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td>
                                    {{ Form::select('city[]',$cities,null, array('class' => 'form-control','id'=>'city_0')) }}
                                </td>
                                <td>
                                    <input type="text" name="time[]" class="form-control timepicker"
                                           placeholder="time AM/PM"
                                           id="time_0" required autocomplete="off">
                                </td>
                                <td>
                                    <input type="text" name="duration[]" class="form-control" placeholder="In Minutes"
                                           id="duration_0" required autocomplete="off">
                                </td>
                                <td>
                                    <input type="text" name="ticket[]" class="form-control" placeholder="In Rupees"
                                           id="ticket_0" required autocomplete="off">
                                </td>
                                <td>
                                    <a class="btn-nav-add-row cursor-pointer"><i
                                                class="fa fa-plus text-primary"></i></a>
                                    <a class="btn-nav-remove-row cursor-pointer"><i class="fa fa-minus text-danger"></i></a>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div class="form-group text-right">
                        {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('pageScript')
@stop