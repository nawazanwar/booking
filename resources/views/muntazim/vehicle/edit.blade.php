@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <!-- form start -->
            {!! Form::model($modal, ['route' => ['muntazim.vehicles.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            @include('partials.messages')
            {!! Form::hidden('up_type','detail') !!}
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h3 class="text-center">Edit the detail of <strong class="text-primary">{{$modal->name}}</strong></h3>
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!} (<span class="text-info"> Alternate Name</span>)
                        {!! Form::text('name', null, ['class' => 'form-control',  'id' => 'name' ]) !!}
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('transport', 1, true,['class'=>'custom-control-input']) !!}
                        <label class="custom-control-label">Is for Transport</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('booking', 1, true,['class'=>'custom-control-input']) !!}
                        <label class="custom-control-label">Is for Booking</label>
                    </div>
                    <div class="form-group">
                        {!! Form::label('time', 'Time') !!} (<span class="text-info">Time with AM or PM</span>)
                        {!! Form::text('time', null, ['class' => 'form-control timepicker',  'id' => 'time','autocomplete'=>'off','required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('seats', 'Total Seats') !!} (<span
                                class="text-info">Total Number of Seats</span>)
                        {!! Form::text('seats', null, ['class' => 'form-control',  'id' => 'seats','autocomplete'=>'off','required']) !!}
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('active', 1, ($modal->active ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Active</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('public', 1, ($modal->public ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Public</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('featured', 1, ($modal->featured ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Featured</label>
                    </div>
                    <div class="form-group">
                        {!! Form::label('property', 'Property') !!} (<span class="text-info">Select AC if you wants to enjoy journey with cool</span>)
                        {{ Form::select('property', array('ac'=>'AC','no_ac'=>'Non AC'), null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('pageScript')
@stop