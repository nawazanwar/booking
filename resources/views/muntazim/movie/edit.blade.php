@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-3">
        <div class="container">
            <!-- form start -->
            {!! Form::model($modal, ['route' => ['muntazim.movies.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h3 class="text-center">Edit the detail of <strong class="text-primary">{{$modal->name}}</strong></h3>
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!} (<span class="text-info"> Alternate Name</span>)
                        {!! Form::text('name', null, ['class' => 'form-control datepicker',  'id' => 'name','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('date', 'Date') !!}<span class="text-danger">*</span>
                        {!! Form::text('date', null, ['class' => 'form-control datepicker',  'id' => 'date','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('start', 'Start') !!}<span class="text-danger">*</span>
                        {!! Form::text('start', null, ['class' => 'form-control timepicker',  'id' => 'start','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('end', 'End') !!}<span class="text-danger">*</span>
                        {!! Form::text('end', null, ['class' => 'form-control timepicker',  'id' => 'end','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('duration', 'Duration') !!}(<span
                                class="text-info">total number of minutes</span>)
                        {!! Form::text('duration', null, ['class' => 'form-control',  'id' => 'duration','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('break', 'Break') !!}(<span class="text-info">total number of minutes</span>)
                        {!! Form::text('break', null, ['class' => 'form-control',  'id' => 'break','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ticket', 'Ticket') !!}(<span class="text-info">total number of Rupees</span>)
                        {!! Form::text('ticket', null, ['class' => 'form-control',  'id' => 'ticket','autocomplete'=>'off' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('type', 'Type') !!}<span class="text-danger">*</span>
                        <select class="form-control selectpicker show-menu-arrow" name="type"
                                data-live-search="true" data-size="10"
                                data-actions-box="true">
                            <option value="hollywood">HollyWood</option>
                            <option value="bollywood">BollyWood</option>
                            <option value="lollywood">LollyWood</option>
                            <option value="animated">Animated</option>
                        </select>
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('pageScript')
@stop