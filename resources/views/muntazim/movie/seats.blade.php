@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-auto my-4">
                    <h5 class="text-center">Available seats of <strong class="text-primary">{{$modal->name}}</strong>
                    </h5>
                    <p class="m-0"><strong>City:</strong> {{$modal->cinema->city->name}}</p>
                    <p class="m-0"><strong>Cinema:</strong> {{$modal->cinema->name}}</p>
                    <p class="m-0"><strong>Date:</strong> {{$modal->date}}</p>
                    <p class="m-0"><strong>Start:</strong> {{$modal->start}}</p>
                    <p class="m-0"><strong>End:</strong> {{$modal->end}}</p>
                    <p class="m-0"><strong>Duration:</strong> {{$modal->duration}} minutes</p>
                    <p class="m-0"><strong>Break:</strong> {{$modal->break}} minute</p>
                    <p class="m-0"><strong>Ticket:</strong> {{$modal->ticket}} RS</p>
                    <p class="m-0"><strong>Type:</strong> {{$modal->type}}</p>
                </div>
                <div class="col-md-12 mx-auto my-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab"
                               href="#tab-front_seats-{{$modal->id}}">Front</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-back_seats-{{$modal->id}}">Back</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab"
                               href="#tab-gallery_seats-{{$modal->id}}">Gallery</a>
                        </li>
                    </ul>
                    <div class="tab-content p-4 text-left" style="height: 258px;overflow: auto;">
                        <div class="tab-pane fade show active" id="tab-front_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5" style="background-color: #8b969e;">
                                @for($i =1; $i<=$modal->cinema->front;$i++)
                                    <div class="w-90px m-3 p-3 product-3 product-media text-center">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="booked-icon-front-{{$i}}"><i
                                                    class="fa fa-ticket"></i></span>
                                        <a class="badge badge-primary cursor-pointer text-white mt-2 mb-1"
                                           style="margin-left: -7px !important;"
                                           id="seat-{{$i}}">Booked It</a>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-back_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5" style="background-color: #8b969e;">
                                @for($i =1; $i<=$modal->cinema->back;$i++)
                                    <div class="w-90px m-3 p-3 product-3 product-media text-center">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="booked-icon-front-{{$i}}"><i
                                                    class="fa fa-ticket"></i></span>
                                        <a class="badge badge-primary cursor-pointer text-white mt-2 mb-1"
                                           style="margin-left: -7px !important;"
                                           id="seat-{{$i}}">Booked It</a>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-gallery_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5" style="background-color: #8b969e;">
                                @for($i =1; $i<=$modal->cinema->gallery;$i++)
                                    <div class="w-90px m-3 p-3 product-3 product-media text-center">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="booked-icon-front-{{$i}}"><i
                                                    class="fa fa-ticket"></i></span>
                                        <a class="badge badge-primary cursor-pointer text-white mt-2 mb-1"
                                           style="margin-left: -7px !important;"
                                           id="seat-{{$i}}">Booked It</a>
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
@stop