@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('faqs.edit_city')}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>

    <section class="section">
        <div class="container">

            <form method="post" action="{{route('muntazim.cities.update', $city->id)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{method_field('PUT')}}
                {{ csrf_field() }}

                <div class="row gap-y">

                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label>{{__('faqs.country')}}</label>
                            <select class="form-control selectpicker show-menu-arrow" name="country"
                                    data-live-search="true" data-size="10"
                                    data-actions-box="true" data-placeholder="{{__('faqs.country')}}">
                                @foreach($countries as $id => $name)
                                    <option value="{{$id}}"
                                            @if($city->country->id == $id) selected @endif >
                                        {{$name}}
                                    </option>
                                @endforeach
                            </select>

                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label>{{__('faqs.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('faqs.name')}}"
                                   value="{{old('name', $city->name)}}" autofocus>
                        </div>
                    </div>
                </div>

                <div class="pull-right">
                    <a href="{{route('muntazim.cities')}}"
                       class="btn btn-label btn-secondary">
                        <label><i class="fa fa-table"></i></label> {{__('faqs.all_cities')}}
                    </a>
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('faqs.update')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop