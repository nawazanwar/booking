@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            @include('partials.filters', ['modelClass' => \App\Models\City::class, 'route' => 'muntazim.cities.create','extra_parameters'=>$parent->id,'type'=>'muntazim'])
            @include('partials.messages')
            <div class="row mt-5">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>{{__('faqs.name')}}</th>
                        <th>Country</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($cities))
                        @foreach($cities as $city)
                            <tr>
                                <td>{{$city->name}}</td>
                                <td>{{$parent->name}}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $cities->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>

        </div>
    </section><!-- /.box -->
@stop