@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <!-- form start -->
            {!! Form::model($modal, ['route' => ['muntazim.foods.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h3 class="text-center">Edit the detail of <strong class="text-primary">{{$modal->name}}</strong></h3>
                    <div class="form-group">
                        {!! Form::label('name', 'Name') !!} (<span class="text-info"> Alternate Name</span>)
                        {!! Form::text('name', null, ['class' => 'form-control',  'id' => 'name' ]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Price') !!}<span class="text-danger">*</span>
                        {!! Form::text('price', null, ['class' => 'form-control',  'id' => 'price' ]) !!}
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('active', 1, ($modal->active ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Active</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('public', 1, ($modal->public ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Public</label>
                    </div>
                    <div class="form-group custom-control custom-checkbox">
                        {!! Form::checkbox('featured', 1, ($modal->featured ==1?true:null), ['class' => 'custom-control-input']) !!}
                        <label class="custom-control-label">Is Featured</label>
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('scripts')
@stop