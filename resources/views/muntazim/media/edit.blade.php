@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <!-- form start -->
            {!! Form::model($modal, ['route' => ['muntazim.media.update',$modal->id], 'method' => 'PUT', 'files' => true] ) !!}
            {!! csrf_field() !!}
            {!! Form::hidden('type',$type) !!}
            @include('partials.messages')
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h5 class="text-center my-2">Add media to <strong class="text-primary">{{$modal->name}}</strong>
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link show active" data-toggle="tab" href="#tab-media">Media</a>
                        </li>
                        {{--<li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-slide">Slides</a>
                        </li>--}}
                    </ul>
                    <div class="tab-content p-4">
                        <div class="tab-pane fade active show" id="tab-media">

                            <div class="form-group">
                                <label>Front Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="front"
                                           accept=".png, .jpg, .jpeg" name="image[front]" onchange="loadFile(event)">
                                    <label class="custom-file-label" for="front">Choose file</label>
                                </div>
                                @if(isset($modal->media[0]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[0]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Back Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="back"
                                           accept=".png, .jpg, .jpeg" name="image[back]" onchange="loadFile(event)">
                                    <label class="custom-file-label" for="front">Choose file</label>
                                </div>
                                @if(isset($modal->media[1]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[1]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-slide">
                            <div class="form-group">
                                <label>First Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="first_slide"
                                           accept=".png, .jpg, .jpeg" name="image[first_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="first_slide">Choose file</label>
                                </div>
                                @if(isset($modal->media[2]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[2]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Second Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="second_slide"
                                           accept=".png, .jpg, .jpeg" name="image[second_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="second_slide">Choose file</label>
                                </div>
                                @if(isset($modal->media[3]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[3]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Third Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="third_slide"
                                           accept=".png, .jpg, .jpeg" name="image[third_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="third_slide">Choose file</label>
                                </div>
                                @if(isset($modal->media[4]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[4]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Fourth Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fourth_slide"
                                           accept=".png, .jpg, .jpeg" name="image[fourth_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="fourth_slide">Choose file</label>
                                </div>
                                @if(isset($modal->media[5]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[5]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Fifth Slide</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="fifth_slide"
                                           accept=".png, .jpg, .jpeg" name="image[fifth_slide]"
                                           onchange="loadFile(event)">
                                    <label class="custom-file-label" for="fifth_slide">Choose file</label>
                                </div>
                                @if(isset($modal->media[6]->src))
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset($modal->media[6]->src)}}">
                                @else
                                    <img class="w-250 h-200 my-2 img-thumbnail" src="{{asset('img/no_image.png')}}">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop

@section('pageScript')
@stop