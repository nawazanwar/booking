@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{$pageTitle}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>

    <section class="section">
        <div class="container">

            <div class="row gap-y">
                <div class="col-md-4">
                    <h3 class="text-center">{{$model->name}}</h3>
                    <ul class="list-group">
                        <li class="list-group-item">{{__('general.name')}}
                            <span class="badge pull-right">{{$model->name}}</span>
                        </li>
                        <li class="list-group-item">{{__('general.username')}}
                            <span class="badge pull-right">{{$model->username}}</span>
                        </li>
                        <li class="list-group-item">{{__('general.email')}}
                            <span class="badge pull-right">{{$model->email}}</span>
                        </li>
                        <li class="list-group-item">{{__('general.gender')}}
                            <span class="badge pull-right">{!! $model->genderIcon() !!}</span>
                        </li>
                        <li class="list-group-item">{{__('general.country')}}
                            <span class="badge pull-right">{{ $model->country }}</span>
                        </li>
                        <li class="list-group-item">{{__('general.state')}}
                            <span class="badge pull-right">{{ $model->state }}</span>
                        </li>
                        <li class="list-group-item">{{__('general.active')}}
                            @if($model->active)
                                <span class="badge badge-success pull-right">Active</span>
                            @else
                                <span class="badge badge-danger pull-right">In Active</span>
                            @endif
                        </li>
                        <li class="list-group-item">{{__('general.created_date')}}
                            <span class="badge pull-right">{{$model->created_at}}</span>
                        </li>
                        <li class="list-group-item">{{__('general.updated_date')}} <span
                                    class="badge pull-right">{{$model->updated_at}}</span></li>
                        <li class="list-group-item text-center">
                            @can('update', $model)
                                <a class="btn btn-xs btn-primary btn-block"
                                   href="{{route('muntazim.user.edit',[$model->id])}}">{{__('general.edit')}}</a>
                            @endcan
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <ul class="nav nav-tabs" user="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#logo">{{__('general.logo')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#bio">{{__('general.bio')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#roles">{{__('general.roles')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#permissions">{{__('general.permissions')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content p-4">
                        <div class="tab-pane fade show active text-center" id="logo">
                            <img class="img img-thumbnail" src="{{$model->avatar}}" alt="avatar">
                        </div>
                        <div class="tab-pane fade show active text-center" id="bio">
                           <div>{{$model->bio}}</div>
                        </div>
                        <div class="tab-pane fade show text-center" id="roles">
                            <p class="text-center">Total Roles are <strong>{{$model->roles()->count()}}</strong></p>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                </tr>
                                @foreach($model->roles as $role)
                                    <tr>
                                        <td>{{$role->id}}</td>
                                        <td>{{$role->name}}</td>
                                        <td>{{$role->label}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="permissions">
                            <p class="text-center">Total Permissions are
                                <strong>{{$model->permissions()->count()}}</strong></p>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                </tr>
                                @foreach($model->permissions as $permission)
                                    <tr>
                                        <td>{{$permission->id}}</td>
                                        <td>{{$permission->name}}</td>
                                        <td>{{$permission->label}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.box -->
@stop
