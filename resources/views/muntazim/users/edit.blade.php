@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('general.edit_user')}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>
    <section class="section">
        <div class="container">

            <form method="post" action="{{route('muntazim.user.update', $user->id)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <div class="row gap-y">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('general.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('general.name')}}"
                                   value="{{old('name', $user->name)}}" autofocus required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.phone')}}</label>
                            <input type="number" name="phone" class="form-control" placeholder="{{__('general.phone')}}"
                                   value="{{old('phone', $user->phone)}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.gender')}}</label>
                            <select class="form-control" name="gender" data-placeholder="{{__('general.gender')}}">
                                <option value="male" @if($user->gender == 'male') selected @endif>
                                    {{__('general.male')}}
                                </option>
                                <option value="female" @if($user->gender == 'female') selected @endif>
                                    {{__('general.female')}}
                                </option>
                                <option value="unspecified" @if($user->gender == 'unspecified') selected @endif>
                                    {{__('general.unspecified')}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.country')}}</label>
                            <select class="form-control" name="country" data-placeholder="{{__('general.country')}}">
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.bio')}}</label>
                            <textarea name="bio" class="form-control" placeholder="{{__('general.bio')}}"
                                      rows="7">{{old('bio', $user->bio)}}</textarea>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1"
                                   @if($user->active) checked @endif>
                            <label class="custom-control-label">{{__('general.active')}}</label>
                        </div>

                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('general.username')}}</label>
                            <input type="text" name="username" class="form-control"
                                   placeholder="{{__('general.username')}}"
                                   value="{{old('username', $user->username)}}" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.email')}}</label>
                            <input type="email" name="email" class="form-control" placeholder="{{__('general.email')}}"
                                   value="{{old('email', $user->email)}}" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.password')}}</label>
                            <input type="password" name="password" class="form-control"
                                   placeholder="{{__('general.password')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.roles')}}</label>
                            <select class="form-control selectpicker show-menu-arrow" name="roles[]"
                                    data-live-search="true" data-selected-text-format="count > 3" data-size="10"
                                    data-actions-box="true" data-placeholder="{{__('general.roles')}}" multiple>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" @if($user->hasRole($role->name)) selected @endif >
                                        {{$role->label}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="customFile"
                                       accept=".png, .jpg, .jpeg">
                                <label class="custom-file-label"
                                       for="customFile">{{__('general.choose_new_avatar')}}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="delete_avatar" class="custom-control-input" value="1">
                                <label class="custom-control-label">{{__('general.delete_avatar')}}</label>
                            </div>
                        </div>

                        <div class="text-center">
                            <img class="w-150 h-150 rounded-circle" src="{{$user->avatar}}" alt="avatar">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <a href="{{route('muntazim.user.delete', $user->id)}}" class="btn btn-label btn-danger"
                           onclick="return confirm('{{__('general.are_you_sure_to_delete')}}');">
                            <label><i class="fa fa-trash"></i></label> {{__('general.delete_user')}}
                        </a>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="pull-right">
                            <a href="{{route('muntazim.users')}}"
                               class="btn btn-label btn-secondary">
                                <label><i class="fa fa-table"></i></label> {{__('general.all_users')}}
                            </a>
                            <button type="submit" class="btn btn-label btn-primary">
                                <label><i class="fa fa-save"></i></label> {{__('general.update')}}
                            </button>
                        </div>
                    </div>

                </div>
            </form>

        </div>
    </section>
@stop