@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            @include('partials.filters', ['modelClass' => \App\Models\User::class, 'route' => 'muntazim.user.create','type'=>'muntazim'])
            @include('partials.messages')
            <div class="row mt-5">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">File</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Misc</th>
                        <th>Date Created</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($users))
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    <img class="avatar avatar-xs" src="{{$user->avatar}}" alt="avatar">
                                </td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->username}}</td>
                                <td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
                                <td class="text-center">{!! $user->genderIcon() !!}</td>
                                <td class="text-center">
                                    @if($user->active)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">InActive</span>
                                    @endif
                                </td>
                                <td>{{$user->created_at}}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <span class="btn btn-primary bg-gradient-primary dropdown-toggle"
                                          data-toggle="dropdown">{{__('general.options')}}</span>
                                        <div class="dropdown-menu">

                                            {{--routes working when the role is active--}}

                                            @if($user->active)
                                                @if(Auth::user()->ability('view_role'))
                                                    <a class="dropdown-item"
                                                       href="{{route('muntazim.roles',['parent_id'=>$user->id,'for'=>'users'])}}">{{__('general.roles')}}</a>
                                                @endif
                                                @if(Auth::user()->ability('view_permission'))
                                                    <a class="dropdown-item"
                                                       href="{{route('muntazim.permissions',['parent_id'=>$user->id,'for'=>'users'])}}">{{__('general.permissions')}}</a>
                                                @endif
                                            @endif

                                            {{--here are the genral routes--}}

                                            @can('update', $user)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.user.edit',[$user->id])}}">{{__('general.edit')}}</a>
                                            @endcan
                                            @can('view', $user)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.users.show',[$user->id])}}">{{__('general.view')}}</a>
                                            @endcan
                                            @php
                                                if(isset($for)){

                                                 $route = route('muntazim.user.delete',[$user->id,'parent_id'=>$parent_id,'for'=>$for]);

                                                } else {

                                                  $route = route('muntazim.user.delete',[$user->id]);

                                                }

                                            @endphp
                                            @can('delete', $user)
                                                <a class="dropdown-item" href="{{$route}}"
                                                   onclick="return confirm('{{__('general.are_you_sure_to_delete')}}');">{{__('general.delete')}}</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $users->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>

        </div>
    </section><!-- /.box -->
@stop