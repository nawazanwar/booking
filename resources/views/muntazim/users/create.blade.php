@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('general.create_user')}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>
    <section class="section">
        <div class="container">

            <form method="post" action="{{route('muntazim.user.store')}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row gap-y">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('general.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('general.name')}}"
                                   value="{{old('name')}}" autofocus required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.phone')}}</label>
                            <input type="number" name="phone" class="form-control" placeholder="{{__('general.phone')}}"
                                   value="{{old('phone')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.gender')}}</label>
                            <select class="form-control" name="gender" data-placeholder="{{__('general.gender')}}">
                                <option value="male" @if(old('gender') == 'male') selected @endif>
                                    {{__('general.male')}}
                                </option>
                                <option value="female" @if(old('gender') == 'female') selected @endif>
                                    {{__('general.female')}}
                                </option>
                                <option value="unspecified" @if(old('gender') == 'unspecified') selected @endif>
                                    {{__('general.unspecified')}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.country')}}</label>
                            <select class="form-control" name="country" data-placeholder="{{__('general.country')}}">
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.state')}}</label>
                            <select class="form-control" name="state" data-placeholder="{{__('general.state')}}">
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('general.username')}}</label>
                            <input type="text" name="username" class="form-control"
                                   placeholder="{{__('general.username')}}"
                                   value="{{old('username')}}" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.email')}}</label>
                            <input type="email" name="email" class="form-control" placeholder="{{__('general.email')}}"
                                   value="{{old('email')}}" required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.password')}}</label>
                            <input type="password" name="password" class="form-control"
                                   placeholder="{{__('general.password')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.roles')}}</label>
                            <select class="form-control" name="roles[]" data-placeholder="{{__('general.roles')}}"
                                    required>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">
                                        {{$role->label}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" name="avatar" class="custom-file-input" id="customFile"
                                       accept=".png, .jpg, .jpeg">
                                <label class="custom-file-label"
                                       for="customFile">{{__('general.choose_new_avatar')}}</label>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1"
                                   @if(old('active')) checked @endif>
                            <label class="custom-control-label">{{__('general.active')}}</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label>{{__('general.bio')}}</label>
                        <textarea name="bio" class="form-control" placeholder="{{__('general.bio')}}"
                                  rows="7">{{old('bio')}}</textarea>
                    </div>
                </div>
                <div class="pull-right">
                    <a href="{{route('muntazim.users')}}"
                       class="btn btn-label btn-secondary">
                        <label><i class="fa fa-table"></i></label> {{__('general.all_users')}}
                    </a>
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('general.create')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop
@section('pageScript')
@stop
