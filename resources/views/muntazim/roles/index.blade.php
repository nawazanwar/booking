@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-6">
        <div class="container">
            @include('partials.filters', ['modelClass' => \App\Models\Role::class, 'route' => 'muntazim.roles.create','extra_parameters'=>$parent_id,'type'=>'muntazim'])
            @include('partials.messages')
            <div class="row mt-5">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Label</th>
                        <th class="text-center">Misc</th>
                        <th>Date Created</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($roles))
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->name}}</td>
                                <td>{{$role->label}}</td>
                                <td class="text-center">
                                    @if($role->active)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">InActive</span>
                                    @endif
                                </td>
                                <td>{{$role->created_at}}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <span class="btn btn-primary bg-gradient-primary dropdown-toggle"
                                          data-toggle="dropdown">{{__('general.options')}}</span>
                                        <div class="dropdown-menu">

                                            {{--routes working when the role is active--}}

                                            @if($role->active)
                                                @if(Auth::user()->ability('view_user'))
                                                    <a class="dropdown-item"
                                                       href="{{route('muntazim.users',['parent_id'=>$role->id,'for'=>'roles'])}}">{{__('general.users')}}</a>
                                                @endif
                                                @if(Auth::user()->ability('view_permission'))
                                                    <a class="dropdown-item"
                                                       href="{{route('muntazim.permissions',['parent_id'=>$role->id,'for'=>'roles'])}}">{{__('general.permissions')}}</a>
                                                @endif
                                            @endif

                                            {{--here are the genral routes--}}

                                            @can('update', $role)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.roles.edit',[$role->id])}}">{{__('general.edit')}}</a>
                                            @endcan
                                            @can('view', $role)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.roles.show',[$role->id])}}">{{__('general.view')}}</a>
                                            @endcan
                                            @php
                                                if(isset($for)){

                                                 $route = route('muntazim.roles.delete',[$role->id,'parent_id'=>$parent_id,'for'=>$for]);

                                                } else {

                                                  $route = route('muntazim.roles.delete',[$role->id]);

                                                }

                                            @endphp

                                            @can('delete', $role)
                                                <a class="dropdown-item" href="{{$route}}"
                                                   onclick="return confirm('{{__('general.are_you_sure_to_delete')}}');">{{__('general.delete')}}</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $roles->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>
        </div>
    </section>
@stop