@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <form method="post" action="{{route('muntazim.roles.store')}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row gap-y">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('general.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('general.name')}}"
                                   value="{{old('name')}}" autofocus required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.label')}}</label>
                            <input type="text" name="label" class="form-control" placeholder="{{__('general.label')}}"
                                   value="{{old('label')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.permissions')}}</label>
                            <select class="form-control selectpicker show-menu-arrow" name="permissions[]"
                                    data-live-search="true" data-selected-text-format="count > 3" data-size="10"
                                    data-actions-box="true" data-placeholder="{{__('general.permissions')}}" multiple>
                                    required multiple>
                                @foreach($permissions as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1"
                                   @if(old('active')) checked @endif>
                            <label class="custom-control-label">{{__('general.active')}}</label>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <a href="{{route('muntazim.roles')}}"
                       class="btn btn-label btn-secondary">
                        <label><i class="fa fa-table"></i></label> {{__('general.all_roles')}}
                    </a>
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('general.create')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop