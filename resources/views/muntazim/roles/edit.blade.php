@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <form method="post" action="{{route('muntazim.roles.update', $role->id)}}" class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}

                <div class="row gap-y">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('general.name')}}</label>
                            <input type="text" name="name" class="form-control" placeholder="{{__('general.name')}}"
                                   value="{{old('name',$role->name)}}" disabled="disabled" autofocus required>
                        </div>
                        <div class="form-group">
                            <label>{{__('general.label')}}</label>
                            <input type="text" name="label" class="form-control" placeholder="{{__('general.label')}}"
                                   value="{{old('label',$role->label)}}" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label>{{__('general.permissions')}}</label>
                            @php
                               $already_permissions =  $role->permissions->pluck('id')->toArray();
                            @endphp
                            <select class="form-control selectpicker show-menu-arrow" name="permissions[]"
                                    data-live-search="true" data-selected-text-format="count > 3" data-size="10"
                                    data-actions-box="true" data-placeholder="{{__('general.permissions')}}" required multiple>
                                @foreach($permissions as $key=>$value)
                                    @php
                                       if (in_array($key,$already_permissions)){
                                         $selected ="selected";
                                       }else{
                                         $selected ="";
                                       }
                                    @endphp
                                    <option value="{{$key}}" {{$selected}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="active" class="custom-control-input" value="1"
                                   @if($role->active) checked @endif>
                            <label class="custom-control-label">{{__('general.active')}}</label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12">
                        <div class="pull-right">
                            <a href="{{route('muntazim.roles')}}"
                               class="btn btn-label btn-secondary">
                                <label><i class="fa fa-table"></i></label> {{__('general.all_roles')}}
                            </a>
                            <button type="submit" class="btn btn-label btn-primary">
                                <label><i class="fa fa-save"></i></label> {{__('general.update')}}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop