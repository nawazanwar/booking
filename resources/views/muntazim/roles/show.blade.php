@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="row gap-y">
                <div class="col-md-4">
                    <h3 class="text-center">{{$model->label}}</h3>
                    <ul class="list-group">
                        <li class="list-group-item">{{__('general.name')}} <span
                                    class="badge pull-right">{{$model->name}}</span></li>
                        <li class="list-group-item">{{__('general.label')}} <span
                                    class="badge pull-right">{{$model->label}}</span></li>
                        <li class="list-group-item">{{__('general.active')}}
                            @if($model->active)
                                <span class="badge badge-success pull-right">Active</span>
                            @else
                                <span class="badge badge-danger pull-right">In Active</span>
                            @endif
                        </li>
                        <li class="list-group-item">{{__('general.created_date')}} <span
                                    class="badge pull-right">{{$model->created_at}}</span></li>
                        <li class="list-group-item">{{__('general.updated_date')}} <span
                                    class="badge pull-right">{{$model->updated_at}}</span></li>
                        <li class="list-group-item text-center">
                            @can('update', $model)
                                <a class="btn btn-xs btn-primary btn-block"
                                   href="{{route('muntazim.roles.edit',[$model->id])}}">{{__('general.edit')}}</a>
                            @endcan
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#users">{{__('general.users')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#permissions">{{__('general.permissions')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content p-4">
                        <div class="tab-pane fade show active text-center" id="users">
                            <p class="text-center">Total Users of <strong>{{$model->label}}</strong>
                                are <strong>{{$model->users()->count()}}</strong></p>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                </tr>
                                @foreach($model->users as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="permissions">
                            <p class="text-center">Total Permissioins of <strong>{{$model->label}}</strong>
                                are <strong>{{$model->permissions()->count()}}</strong></p>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Name</th>
                                    <th>Label</th>
                                </tr>
                                @foreach($model->permissions as $permission)
                                    <tr>
                                        <td>{{$permission->id}}</td>
                                        <td>{{$permission->name}}</td>
                                        <td>{{$permission->label}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
