@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <div class="box box-primary">
                <div class="box-body">

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if ($successMessage = \Illuminate\Support\Facades\Session::get('successMessage'))
                        <div class="callout callout-success">
                            <p>{{ $successMessage }}</p>
                        </div>
                    @endif

                    <table id="accessListTbl" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Label</th>
                            <th>Date Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($permissions as $permission)
                            <tr>
                                <td>{{$permission->name}}</td>
                                <td>{{$permission->label}}</td>
                                <td>{{$permission->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div><!-- /.box-body -->
                <div class="box-footer with-border">
                    <div class="pull-right">
                        {{ $permissions->links() }}
                    </div>
                </div><!-- /.box-header -->
            </div><!-- /.box -->
        </div>
    </section>
@stop
@section('pageScript')
@stop
