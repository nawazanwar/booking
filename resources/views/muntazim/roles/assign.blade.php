@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            <form method="post" action="{{route('muntazim.roles.update',[$parent_id,'for'=>$for])}}"
                  class="input-border"
                  autocomplete="off" enctype="multipart/form-data">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="row gap-y">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('general.permissions')}}</label>
                            <select class="form-control selectpicker show-menu-arrow" name="roles[]"
                                    data-live-search="true" data-selected-text-format="count > 3" data-size="10"
                                    data-actions-box="true" data-placeholder="{{__('general.permissions')}}" multiple>
                                required multiple>
                                @foreach($model as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <button type="submit" class="btn btn-label btn-primary">
                        <label><i class="fa fa-save"></i></label> {{__('general.update')}}
                    </button>
                </div>
            </form>
        </div>
    </section>
@stop