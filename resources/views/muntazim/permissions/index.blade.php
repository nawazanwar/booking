@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{__('general.all_permissions')}}  {{$parentTitle}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>

    <section class="section">

        <div class="container">

            <div class="row gap-y">
                {{-- filters --}}
                @include('partials.filters', ['modelClass' => \App\Models\Permission::class, 'permission' => 'sync_permission', 'route' => 'muntazim.permissions.sync', 'button' => __('faqs.sync_permissions')])

                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">Id</th>
                        <th>Name</th>
                        <th>Label</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($permissions))
                        @foreach($permissions as $permission)
                            <tr>
                                <td class="text-center">{{$permission->id}}</td>
                                <td>{{$permission->name}}</td>
                                <td>{{$permission->label}}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                         <span class="btn btn-primary bg-gradient-primary dropdown-toggle"
                                               data-toggle="dropdown">{{__('general.options')}}</span>
                                        <div class="dropdown-menu">

                                            {{--Here are the some general Routes--}}

                                            @if(Auth::user()->ability('view_user'))
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.users',['parent_id'=>$permission->id,'for'=>'permissions'])}}">{{__('general.users')}}</a>
                                            @endif
                                            @if(Auth::user()->ability('view_role'))
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.roles',['parent_id'=>$permission->id,'for'=>'permissions'])}}">{{__('general.roles')}}</a>
                                            @endif

                                            @if(isset($for))
                                                @can('delete', $permission)
                                                    <a class="dropdown-item"
                                                       href="{{route('muntazim.permission.delete',[$permission->id,'parent_id'=>$parent_id,'for'=>$for])}}"
                                                       onclick="return confirm('{{__('general.are_you_sure_to_delete')}}');">{{__('general.delete')}}</a>
                                                @endcan
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $permissions->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>

        </div>
    </section><!-- /.box -->
@stop