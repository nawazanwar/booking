@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section py-6">
        <div class="container">
            @include('partials.filters', ['modelClass' => \App\Models\Country::class, 'route' => 'muntazim.countries.create','extra_parameters'=>'','type'=>'muntazim'])
            @include('partials.messages')
            <div class="row mt-5">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">{{__('faqs.code')}}</th>
                        <th>{{__('faqs.name')}}</th>
                        <th class="text-center">{{__('faqs.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($countries))
                        @foreach($countries as $country)
                            <tr>
                                <td>{{$country->code}}</td>
                                <td>{{$country->name}}</td>
                                <td class="text-center">
                                    <div class="dropdown">
                                    <span class="btn btn-primary bg-gradient-primary dropdown-toggle"
                                          data-toggle="dropdown">{{__('faqs.options')}}</span>
                                        <div class="dropdown-menu">
                                            @can('view', $country)
                                                <a class="dropdown-item"
                                                   href="{{route('muntazim.cities',[$country->id])}}">{{__('faqs.all_cities')}}</a>
                                            @endcan
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="20" class="text-center">No data Found</td>
                        </tr>
                    @endif
                    </tbody>
                </table>

                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $countries->appends(request()->input())->links('partials.paginator') }}
                </div>
            </div>

        </div>
    </section><!-- /.box -->
@stop