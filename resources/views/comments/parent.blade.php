<div class="parent_comment media p-2 border-top-1 border-right-1  current_comment_row_{{$comment->id}}">
    <img class="avatar avatar-sm img-thumbnail img-circle mr-2 mt-2" src="{{$comment->user->avatar}}" alt="...">
    <div class="media-body">
        <div class="small-1">
            By <strong>{{$comment->user->name}}</strong>
            <time class="ml-4 opacity-70 small-3">{{$comment->created_at->diffForHumans()}}</time>&nbsp;&nbsp;
            @include('pages.votes',['data'=>$comment,'for'=>'comments'])
            <span class="pull-right">
                @include('pages.edit_delete',['for'=>'comment','data'=>$comment,'parent_id'=>$parent_id])
            </span>
        </div>
        <div class="small-1 mb-0 text-justify col-sm-12 load_more">{!!  $comment->content !!}</div>
    </div>
</div>