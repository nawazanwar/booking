<div class="col-sm-12 pl-0 pr-0 pt-2 clearfix">
    <div class="pull-left col-sm-1 pl-0 pr-0">
        <img src="{{$answer->user->avatar}}" class="avatar-xs img-thumbnail img-circle">
    </div>
    <div class="col-sm-11 pull-left pl-0">
        <div class="form-control" id="comment" contentEditable="true"
             placeholder="{{__('general.comment_placeholder')}}" style="height: auto;"></div>
    </div>
    <div class="form-sm-12 pull-right p-3">
        <input type="submit" class="btn btn-xs btn-primary"
               onclick="General.AddComment('{{$answer->id}}','parent','{{route('dashboard.comment.store',$answer->id)}}')"
               value="{{__('general.comment_button')}}">
    </div>
</div>