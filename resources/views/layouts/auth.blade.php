<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle')</title>

    <link href="{{asset('css/vendor.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="layout-centered bg-img" style="background-image: url({{asset('img/bg/4.jpg')}});">

<!-- Main Content -->
<main class="main-content">
    @yield('content')
</main>

<script type="text/javascript" src="{{asset('js/vendor.min.js') }}"></script>
</body>
</html>
