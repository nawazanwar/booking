<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle') | {{__('faqs.site_title')}}</title>

    <link href="{{asset('css/vendor.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="faqs-body">
@if(\Request::get('src') !='lightbox')
    @yield('header')
    @include('partials/header')
@endif
<!-- Main Content -->
<main class="main-content" id="app">
    @yield('content')
</main>
@if(\Request::get('src') !='lightbox')
    @include('partials/footer')
    @yield('footer')
@endif
@if(!Auth::check())
    <div id="login-popup" class="popup col-6 col-md-4" data-position="bottom-right">
        <button type="button" class="close" data-dismiss="popup" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <div class="media">
            <div class="media-body">
                <h5>{{__('faqs.login_required')}}</h5>
                <p class="mb-0">{!! __('faqs.login_required_message', ['login' => route('login'), 'register' => route('register')]) !!}</p>
            </div>
        </div>
    </div>
@endif

<script type="text/javascript" src="{{asset('js/vendor.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/app.min.js') }}"></script>
@yield('scripts')
</body>
</html>
