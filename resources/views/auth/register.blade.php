@extends('layouts.auth')
@section('pageTitle', $pageTitle)
@section('content')
    <div class="bg-white rounded shadow-7 w-400 mw-600 p-6">
        <h5 class="mb-7 text-center">{{__('faqs.create_an_account')}}</h5>

        @if(session('error_status'))
            <div class="alert alert-danger alert-dismissable">
                {{ session('error_status') }}
            </div>
        @endif
        <form role="form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row gap-y">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">{{__('faqs.name')}} <span class="text-danger">*</span></label>
                        <input id="name" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                               name="name" placeholder="{{__('faqs.name')}}" value="{{ old('name') }}"
                               autocomplete="off" tabindex="1" required>
                        @if ($errors->has('name'))
                            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="name">{{__('faqs.username')}} <span class="text-danger">*</span></label>
                        <input id="username" type="text" required
                               class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}"
                               name="username" placeholder="{{__('faqs.username')}}" value="{{ old('username') }}"
                               autocomplete="off" tabindex="3">
                        @if ($errors->has('username'))
                            <div class="invalid-feedback">{{ $errors->first('username') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">{{__('faqs.password')}} <span class="text-danger">*</span></label>
                        <input type="password" autocomplete="off" required
                               class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                               name="password" placeholder="Password" id="password" onautocomplete="off" tabindex="5">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{__('faqs.country')}}</label>
                        <select class="form-control selectpicker show-menu-arrow select-country" name="country"
                                data-live-search="true" data-liveSearchPlaceholder="{{__('faqs.search_country')}}"
                                data-noneSelectedText="{{__('faqs.select_country')}}"
                                data-size="10" data-placeholder="{{__('faqs.country')}}" data-target="#cities"
                                tabindex="7">
                            <option disabled selected value>{{__('faqs.select_country')}}</option>
                            @foreach($countries as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">{{__('faqs.email')}} <span class="text-danger">*</span></label>
                        <input type="text" autocomplete="off"
                               class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email"
                               id="email" placeholder="Email" tabindex="2">
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{__('faqs.gender')}}</label>
                        <select class="form-control selectpicker show-menu-arrow" name="gender" data-size="10"
                                data-placeholder="{{__('faqs.gender')}}" tabindex="4">
                            <option value="male" @if(old('gender') == 'male') selected @endif>
                                {{__('faqs.male')}}
                            </option>
                            <option value="female" @if(old('gender') == 'female') selected @endif>
                                {{__('faqs.female')}}
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="password">{{__('faqs.confirm_password')}} <span class="text-danger">*</span></label>
                        <input type="password" autocomplete="off" required
                               class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                               name="password_confirmation" placeholder="{{__('faqs.confirm_password')}}"
                               id="password_confirmation" onautocomplete="off" tabindex="6">
                        @if ($errors->has('password_confirmation'))
                            <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>{{__('faqs.city')}}</label>
                        <select class="form-control selectpicker show-menu-arrow" name="city"
                                data-live-search="true" data-liveSearchPlaceholder="{{__('faqs.search_city')}}"
                                data-noneSelectedText="{{__('faqs.select_city')}}"
                                data-size="10" data-placeholder="{{__('faqs.city')}}" data-target="#cities"
                                tabindex="7">
                            <option disabled selected value>{{__('faqs.select_city')}}</option>
                            @foreach($cities as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit"
                        style="background-image: linear-gradient(120deg, #667eea 0%, #764ba2 100%);">{{__('faqs.register')}}</button>
            </div>
        </form>
        <hr class="w-30">
        <p class="text-center text-muted small-2">{{__('faqs.do_you_have_an_account')}} <a
                    href="{{route('login')}}">{{__('faqs.login_here')}}</a>
        </p>
    </div>
@stop