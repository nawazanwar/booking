@extends('layouts.auth')
{{--@section('pageTitle', $pageTitle)--}}
@section('content')

    <div class="bg-white rounded shadow-7 w-400 mw-100 p-6">

        @include('partials.messages', ['noPadding' => true])

        <h5 class="mb-7">{{__('faqs.reset_password')}}</h5>

        @if(session('error_status'))
            <div class="alert alert-danger alert-dismissable">
                <strong>Danger:</strong> {{ session('error_status') }}
            </div>
        @endif

        <form role="form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
                <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email"
                       placeholder="{{__('faqs.email')}}">
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form-group">
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                       name="password" placeholder="{{__('faqs.password')}}">
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="form-group">
                <input type="password"
                       class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                       name="password_confirmation" id="password_confirmation"
                       placeholder="{{__('faqs.confirm_password')}}">
                @if ($errors->has('password_confirmation'))
                    <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                @endif
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit"
                        style="background-image: linear-gradient(120deg, #667eea 0%, #764ba2 100%);">{{__('faqs.reset_password')}}</button>
            </div>
        </form>
    </div>
@endsection