@extends('layouts.auth')
{{--@section('pageTitle', $pageTitle)--}}
@section('content')

    <div class="bg-white rounded shadow-7 w-400 mw-100 p-6">

        @include('partials.messages', ['noPadding' => true])

        <h5 class="mb-7 text-center">{{__('faqs.recover_password')}}</h5>

        @if(session('error_status'))
            <div class="alert alert-danger alert-dismissable">
                {{ session('error_status') }}
            </div>
        @endif

        <form role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email"
                       placeholder="{{__('faqs.email')}}">
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit"
                        style="background-image: linear-gradient(120deg, #667eea 0%, #764ba2 100%);">{{__('faqs.recover_password')}}</button>
            </div>
        </form>

        <div class="divider">{{__('faqs.or')}}</div>

        <a class="btn btn-block btn-primary" href="{{route('login')}}">{{__('faqs.login')}}</a></p>
    </div>
@endsection