@extends('layouts.auth')
@section('pageTitle', $pageTitle)
@section('content')

    <div class="bg-white rounded shadow-7 w-400 mw-100 p-6">
        <h5 class="mb-7 text-center">{{__('faqs.sign_in_to_your_account')}}</h5>

        @if(session('error_status'))
            <div class="alert alert-danger alert-dismissable">
                <strong>Danger:</strong> {{ session('error_status') }}
            </div>
        @endif

        <form role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email"
                       placeholder="{{__('faqs.email')}}" autocomplete="off">
                @if ($errors->has('email'))
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @endif
            </div>

            <div class="form-group">
                <input type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}"
                       name="password" placeholder="{{__('faqs.password')}}" autocomplete="off">
                @if ($errors->has('password'))
                    <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                @endif
            </div>

            <div class="form-group flexbox py-3">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" checked>
                    <label class="custom-control-label">Remember me</label>
                </div>

                <a class="text-muted small-2" href="{{route('password.request')}}">Forgot password?</a>
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-primary" type="submit"
                        style="background-image: linear-gradient(120deg, #667eea 0%, #764ba2 100%);">{{__('faqs.login')}}</button>
            </div>
        </form>

        {{--<div class="divider">{{__('faqs.or_login_with')}}</div>
        <div class="text-center">
            <a class="btn btn-circle btn-sm btn-facebook mr-2" href="/auth/facebook"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-circle btn-sm btn-twitter" href="/auth/twitter"><i class="fa fa-twitter"></i></a>
        </div>--}}

        <hr class="w-30">

        <p class="text-center text-muted small-2">{{__('faqs.dont_have_an_account')}} <a
                    href="{{route('register')}}">{{__('faqs.register_here')}}</a>
        </p>
    </div>
@endsection
