@component('mail::message')
# {{__('faqs.notification_mail_greeting')}}

{{__('faqs.welcome_message')}}

@component('mail::button', ['url' => $url])
{{__('faqs.activate_account')}}
@endcomponent

{{__('faqs.looking_forward_message')}}

{{__('faqs.mail_salutation')}}<br>
{{ config('app.name') }}

@component('mail::subcopy')
{{__('faqs.welcome_email_link_message')}}[{{$url}}]({{$url}})
@endcomponent

@endcomponent
