@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')

    <section class="section py-6">
        <div class="container">
            @include('partials.messages')
            <div class="row mt-5">
                @if(count($data))
                    @foreach($data as $d)
                        @if($type=='movie')
                            @include('components.movie.list',['data'=>$d,'type'=>'frontend'])
                        @elseif($type=='food')
                            @include('components.food.list',['data'=>$d,'type'=>'frontend'])
                        @elseif($type=='vehicle')
                            @include('components.vehicle.list',['data'=>$d,'type'=>'frontend'])
                        @endif
                    @endforeach
                @else
                    <div class="col-sm-12 d-flex justify-content-center p-5 bg-gray border shadow-9">
                        <strong>No data Found</strong>
                    </div>
                @endif
            </div>
            @if(count($data))
                <div class="w-100">
                    <!-- pagination nav -->
                    {{ $data->appends(request()->input())->links('partials.paginator') }}
                </div>
            @endif
        </div>
    </section><!-- /.box -->
@stop