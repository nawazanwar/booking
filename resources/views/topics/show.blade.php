@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    {{--<header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{$pageTitle}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>--}}

    <section class="section">
        <div class="container">

            @include('components.questions', ['questions' => $topic->questions])

        </div>
    </section><!-- /.box -->
@stop
