@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    {{--<header class="header bg-gradient-dark text-white">
        <div class="container text-center">
            <h1 class="display-4 mb-6"><strong>{{$pageTitle}}</strong></h1>
            @include('includes.muntazim.breadcrumbs', ['pageTitle' => $pageTitle, 'smallTitle' => $smallTitle, 'breadcrumbs' => $breadcrumbs])
        </div>
    </header>--}}

    <section class="section">
        <div class="container">

            @if($topics->count())
                <div class="row gap-y">
                    @foreach($topics as $topic)
                        <div class="col-12 col-sm-12 col-md-6 col-lg-4">
                            <div class="card border hover-shadow-4 d-block">
                                <img class="card-img-top" src="{{$topic->image}}" alt="{{$topic->title}}">
                                <div class="card-body">
                                    <h4 class="card-title">{{$topic->title}}</h4>
                                    <p>{{$topic->content}}</p>
                                </div>
                                <div class="card-footer d-flex align-content-center align-content-between">
                                    <a class="btn btn-sm btn-round btn-outline-primary"
                                       href="{{route('topics.show', $topic->slug)}}">{{__('faqs.explore')}}</a>
                                    @if($topic->isFollower())
                                        <a class="btn btn-sm follow-it" data-id="{{$topic->id}}"
                                           data-type="topic" href="#">
                                            <i class="fa fa-bell"></i>
                                        </a>
                                    @else
                                        <a class="btn btn-sm text-dark follow-it"
                                           data-id="{{$topic->id}}" data-type="topic" href="#">
                                            <i class="fa fa-bell-o"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                @include('errors.no-records-found')
            @endif

        </div>
    </section><!-- /.box -->
@stop