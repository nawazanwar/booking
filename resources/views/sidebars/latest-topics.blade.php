@if($latestTopics)
    <h4 class="m-b-15">{{__('faqs.latest_topics')}}</h4>

    <div class="media-list">
        @foreach($latestTopics as $topic)
            <div class="d-flex align-items-center media p-1 mb-1">
                <a href="{{route('topics.show', ['slug' => $topic->slug])}}">
                    <img class="avatar avatar-md mr-2 border" src="{{$topic->image}}">
                </a>
                <div class="media-body">
                    <h5 class="">
                        <a href="{{route('topics.show', ['slug' => $topic->slug])}}">{{$topic->title}}</a>
                    </h5>
                </div>
            </div>
        @endforeach
    </div>
@endif