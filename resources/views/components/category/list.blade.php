<div class="col-12 col-md-4">
    <div class="card d-block border mb-6 shadow-9">
        <div class="card-img-top">
            <img src="{{asset($data->image)}}" alt="Card image cap">
            <div class="badges badges-right">
                @if($data->slug=='cinemas')
                    <a class="badge badge-info"
                       href="{{route('muntazim.cinemas',[$data->id])}}">{{__('faqs.all_cinemas')}}</a>
                @elseif($data->slug=='terminals')
                    <a class="badge badge-info"
                       href="{{route('muntazim.terminals',[$data->id])}}">{{__('faqs.all_terminals')}}</a>
                @elseif($data->slug=='food-points')
                    <a class="badge badge-info"
                       href="{{route('muntazim.shops',[$data->id])}}">{{__('faqs.all_food_points')}}</a>
                @endif
            </div>
        </div>
        <div class="card-body card-border">
            <div class="row mb-2 small-2 text-lighter">
                <div class="col-auto">
                    <a class="text-inherit" href="#">by {{$data->user->name}}</a>
                    <span class="align-middle px-1">•</span>
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
        </div>
    </div>
</div>