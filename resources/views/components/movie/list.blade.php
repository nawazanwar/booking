<div class="col-12 col-md-4" data-shuffle="item" data-groups="movie">
    <div class="card d-block border mb-6 shadow-9">
        <div class="card-img-top">
            @php
                $front_image = isset($data->media[0]->src) ? $data->media[0]->src :'img/no_image.png';
                $back_image = isset($data->media[1]->src) ? $data->media[1]->src : 'img/no_image.png';
            @endphp
            <div class="card overflow-hidden h-250">
                <div class="card-body"
                     style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                </div>
                <div class="card-hover text-white bg-img" data-animation="slide-down"
                     style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                     data-overlay="6">
                </div>
            </div>
            <div class="badges badges-left">
                @if($type=='muntazim')
                    <a class="badge badge-primary"
                       href="{{route('muntazim.movies.seats',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Seats <i class="fa fa-plus"></i></a>
                @else
                    <a class="badge badge-primary"
                       href="{{route('movies.seats',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Seats <i class="fa fa-eye"></i></a>
                @endif
            </div>
            <div class="badges badges-right">
                <span class="badge badge-primary">Ticket : {{$data->ticket}} Rs</span>
            </div>
        </div>
        <div class="card-body card-border h-500">
            <div class="row mb-2 small-2 text-lighter">
                <div class="col-auto">
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
            <div class="badges text-right">
                <p>{{$data->cinema->name}}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Started Date</strong> <span
                            class="badge pull-right badge-info">{{$data->date}}</span></li>
                <li class="list-group-item"><strong>Start</strong> <span
                            class="badge pull-right badge-info">{{$data->start}}</span></li>
                <li class="list-group-item"><strong>End</strong> <span
                            class="badge pull-right badge-info">{{$data->end}}</span></li>
            </ul>
            <div class="badges badges-right mt-2">
                @if(isset($type) AND $type=='muntazim')
                    @if($data->active)
                        <span class="badge badge-success">Active</span>
                    @else
                        <span class="badge badge-danger">In Active</span>
                    @endif
                    @if($data->public)
                        <span class="badge badge-success">Public</span>
                    @else
                        <span class="badge badge-danger">Private</span>
                    @endif
                    <a class="badge badge-primary"
                       href="{{route('muntazim.movies.edit',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Detail <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.movies.media',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Media <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>