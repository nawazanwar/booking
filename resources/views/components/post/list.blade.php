<div class="col-12 col-md-4 mb-5">
    <div class="card d-block border hover-shadow-6  product-media">
        <div class="card-img-top">
            <img src="{{asset($data->image)}}" alt="Card image cap">
            <div class="badges badges-right">
                @if($data->type=='movie' OR $data->type=='vehicle')
                    @php
                        if ($data->type=='movie'){
                          $filter_id = $data['movie'][0]->id;
                        }else if ($data->type=='vehicle'){
                          $filter_id = $data['vehicle'][0]->id;
                        }else if ($data->type=='food'){
                          $filter_id = $data['food'][0]->id;
                        }
                    @endphp
                    <a class="badge badge-primary"
                       href="{{route('post.seats',[$filter_id,'pid'=>$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">{{__('faqs.available_seats')}}</a>
                @endif
            </div>
        </div>
        <div class="card-body card-border">
            <div class="row mb-5 small-2 text-lighter">
                <div class="col-auto text-left">
                    <a class="text-inherit" href="#">by {{$data->user->name}}</a>
                    <span class="align-middle px-1">•</span>
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
                <div class="col-auto text-right">
                    <a class="text-inherit" href="#"></a>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
            <div class="text-justify">
                @if(isset($type) AND $type=='muntazim')
                    @php
                        if ($data->type=='movie'){
                          $filter_id = $data['movie'][0]->id;
                          $type_route = 'muntazim.movies.edit';
                        }else if ($data->type=='vehicle'){
                          $filter_id = $data['vehicle'][0]->id;
                          $type_route = 'muntazim.vehicles.edit';
                        }else if ($data->type=='food'){
                          $filter_id = $data['food'][0]->id;
                          $type_route = 'muntazim.foods.edit';
                        }
                    @endphp
                    @if($data->type=='vehicle')
                        <a class="badge badge-primary"
                           href="{{route('muntazim.vehicles.routes',[$filter_id,'pid'=>$data->id,'src'=>'lightbox'])}}"
                           data-provide="lightbox">Routes <span class="fa fa-plus"></span></a>
                    @endif
                    <a class="badge badge-primary"
                       href="{{route(''.$type_route.'',[$filter_id,'src'=>'lightbox','pid'=>$data->id])}}"
                       data-provide="lightbox">Detail <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.media.edit',[$filter_id,'src'=>'lightbox','pid'=>$data->id])}}"
                       data-provide="lightbox">Media <i class="fa fa-plus"></i></a>
                @endif
                @if($type=='muntazim')
                    @can('update', $data)
                        <a class="badge badge-primary"
                           href="{{route('muntazim.posts.edit',[$data->id,'pid'=>$parent->id])}}"><span
                                    class="fa fa-edit"></span> {{__('faqs.edit')}}</a>
                    @endcan
                    @can('delete', $data)
                        <a class="badge badge-danger"
                           href="{{route('muntazim.posts.delete',[$data->id,'pid'=>$parent->id])}}"
                           onclick="return confirm('{{__('faqs.are_you_sure_to_delete')}}');"><span
                                    class="fa fa-trash"></span> {{__('faqs.delete')}}</a>
                    @endcan
                @endif
            </div>
        </div>
    </div>
</div>