<div class="container text-center">
    <h1 class="display-4">{{$data->name}}</h1>
    <p class="lead-2 p-0">{{$data->content}}</p>
</div>

<section class="section pt-3 pb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mb-6 mb-md-0">
                <div data-provide="slider" data-arrows="true" data-autoplay="true" data-autoplay-speed="1000"
                     class="shadow-9 border">
                    <div><img src="{{asset(isset($data->media[0]->src)?$data->media[0]->src:'')}}"></div>
                    <div><img src="{{asset(isset($data->media[1]->src)?$data->media[1]->src:'')}}"></div>
                    <div><img src="{{asset(isset($data->media[0]->src)?$data->media[0]->src:'')}}"></div>
                </div>
            </div>
            <div class="col-md-4">

                <h5>{{$data->name}}</h5>
                <p>{{$data->content}}</p>

                <ul class="project-detail mt-1">

                    @if($data->type=='cinema')
                        <a class="badge badge-info"
                           href="{{route('muntazim.posts',[$data->id])}}">{{__('faqs.all_movies')}}</a>
                    @elseif($data->type=='terminal')
                        <a class="badge badge-info"
                           href="{{route('muntazim.posts',[$data->id])}}">{{__('faqs.all_vehicles')}}</a>
                    @elseif($data->type=='food')
                        <a class="badge badge-info"
                           href="{{route('muntazim.foods',[$data->id])}}">{{__('faqs.all_foods')}}</a>
                    @endif

                    <li>
                        <strong>Date</strong>
                        <span>{{$data->updated_at->diffForHumans()}}</span>
                    </li>

                    <li>
                        <strong>Created By</strong>
                        <span>{{$data->user->name}}</span>
                    </li>

                    <li>
                        <strong>Address</strong>
                        <a href="http://thetheme.io/thesaas">thetheme.io/thesaas</a>
                    </li>

                    <li>
                        <strong>Share</strong>
                        <div class="social social-sm social-gray social-inline mt-2">
                            <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                            <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                            <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                            <a class="social-dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row pt-8 justify-content-start" data-shuffle="list">
            <h3 class="col-md-12">Related Images</h3>
            <div class="col-md-12 mx-auto">
                <div  data-provide="slider" data-arrows="true" data-autoplay="true" data-autoplay-speed="1000"
                      class="shadow-9 border" data-slides-to-show="4" data-slides-to-scroll="2">
                    @foreach($data->media as $media)

                        @php

                            if($data->type=='cinema'){
                                $name = __('faqs.all_movies');
                            }elseif($data->type=='terminal'){
                                $name = __('faqs.all_vehicles');
                            }elseif($data->type=='food'){
                                $name = __('faqs.all_foods');
                            }

                        @endphp

                        <div class="p-2" data-shuffle="item" data-groups="bag,box">
                            <a class="portfolio-1" href="#" data-toggle="modal" data-target="#modal-portfolio">
                                <img class="img-thumbnail border shadow-9" src="{{asset($media->src)}}"
                                     alt="screenshot">
                                <div class="portfolio-detail">
                                    <h5>{{$name}}</h5>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>