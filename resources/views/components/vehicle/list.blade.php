<div class="col-12 col-md-4" data-shuffle="item" data-groups="vehicle">
    <div class="card d-block border mb-6 shadow-9">
        <div class="card-img-top">
            @php
                $front_image = isset($data->media[0]->src) ? $data->media[0]->src :'img/no_image.png';
                $back_image = isset($data->media[1]->src) ? $data->media[1]->src : 'img/no_image.png';
            @endphp
            <div class="card overflow-hidden h-250">
                <div class="card-body"
                     style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                </div>
                <div class="card-hover text-white bg-img" data-animation="slide-down"
                     style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                     data-overlay="6">
                </div>
            </div>
            <div class="badges badges-left">
                @if($type=='muntazim')
                    <a class="badge badge-primary"
                       href="{{route('muntazim.vehicles.routes',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Route <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.vehicles.seats',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Seats <i class="fa fa-plus"></i></a>
                @else
                    <a class="badge badge-primary"
                       href="{{route('vehicles.seats',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Seats <i class="fa fa-eye"></i></a>
                @endif
            </div>
        </div>
        <div class="card-body card-border h-500">
            <div class="row mb-2 small-2 text-lighter">
                <div class="col-auto">
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
            <div class="text-right">
                <p>{{$data->terminal->name}}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>For Transport</strong>  <span class="badge pull-right badge-info">{{($data->transport==1?'Yes':'No')}}</span></li>
                <li class="list-group-item"><strong>For Booking</strong>  <span class="badge pull-right badge-info">{{($data->booking==1?'Yes':'No')}}</span></li>
                <li class="list-group-item"><strong>Time</strong>  <span class="badge pull-right badge-info">{{$data->time}} daily</span></li>
                <li class="list-group-item"><strong>Seats</strong>  <span class="badge pull-right badge-info">{{$data->seats}}</span></li>
                <li class="list-group-item"><strong>Type</strong>  <span class="badge pull-right badge-info">{{($data->property=='no_ac')?'Non Ac':"AC"}}</span></li>
            </ul>
            <div class="badges badges-right">
                @if(isset($type) AND $type=='muntazim')
                    @if($data->active)
                        <span class="badge badge-success">Active</span>
                    @else
                        <span class="badge badge-danger">In Active</span>
                    @endif
                    @if($data->public)
                        <span class="badge badge-success">Public</span>
                    @else
                        <span class="badge badge-danger">Private</span>
                    @endif
                    <a class="badge badge-primary"
                       href="{{route('muntazim.vehicles.edit',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Detail <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.vehicles.media',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Media <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>