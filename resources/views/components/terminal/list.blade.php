<div class="col-12 col-md-4">
    <div class="card d-block border mb-6 shadow-9">
        <div class="card-img-top">
            <div class="card-img-top">
                @if($type=='muntazim')
                    <img src="{{asset($data->category->image)}}" alt="Card image cap">
                @else
                    @php
                        $front_image = isset($data->media[0]->src) ? $data->media[0]->src : $data->category->image;
                        $back_image = isset($data->media[0]->src) ? $data->media[0]->src : $data->category->image;
                    @endphp
                    <div class="card overflow-hidden h-250">
                        <div class="card-body"
                             style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                        </div>
                        <div class="card-hover text-white bg-img" data-animation="slide-down"
                             style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                             data-overlay="6">
                        </div>
                    </div>
                @endif
            </div>
            <div class="badges badges-left">
                @isset($type)
                    <div class="badges badges-left">
                        @if($type=='muntazim')
                            <a href="{{route('muntazim.vehicles',[$data->id])}}"
                               class="badge badge-primary text-white">Vehicles</a>
                        @else
                            <a href="{{route('vehicles',[$data->id])}}"
                               class="badge badge-primary text-white">Vehicles</a>
                        @endif
                    </div>
                @endisset
            </div>
            <div class="badges badges-right">
                <a class="badge badge-info text-white">{{$data->city->name}}</a>
            </div>
        </div>
        <div class="card-body card-border">
            <div class="row mb-2 small-2 text-lighter">
                <div class="col-auto">
                    <a class="text-inherit" href="#">by {{$data->category->user->name}}</a>
                    <span class="align-middle px-1">•</span>
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
            <div class="badges badges-right">
                @if(isset($type) AND $type=='muntazim')
                    @if($data->active)
                        <span class="badge badge-success">Active</span>
                    @else
                        <span class="badge badge-danger">In Active</span>
                    @endif
                    @if($data->public)
                        <span class="badge badge-success">Public</span>
                    @else
                        <span class="badge badge-danger">Private</span>
                    @endif
                    <a class="badge badge-primary"
                       href="{{route('muntazim.terminals.edit',[$data->id,'src'=>'lightbox','pid'=>$parent->id])}}"
                       data-provide="lightbox">Detail <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.terminals.media',[$data->id,'src'=>'lightbox','pid'=>$parent->id])}}"
                       data-provide="lightbox">Media <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>