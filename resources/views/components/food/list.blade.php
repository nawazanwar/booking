<div class="col-12 col-md-4" data-shuffle="item" data-groups="food">
    <div class="card d-block border mb-6 shadow-9">
        <div class="card-img-top">
            @php
                $front_image = isset($data->media[0]->src) ? $data->media[0]->src :'img/no_image.png';
                $back_image = isset($data->media[1]->src) ? $data->media[1]->src : 'img/no_image.png';
            @endphp
            <div class="card overflow-hidden h-250">
                <div class="card-body"
                     style="background-image: url({{asset($front_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;">
                </div>
                <div class="card-hover text-white bg-img" data-animation="slide-down"
                     style="background-image: url({{asset($back_image)}});background-position: center;background-size: cover;background-repeat: no-repeat;"
                     data-overlay="6">
                </div>
            </div>
        </div>
        <div class="card-body card-border h-500">
            <div class="row mb-2 small-2 text-lighter">
                <div class="col-auto">
                    <time datetime="2018-05-15T19:00">{{$data->updated_at->diffForHumans()}}</time>
                </div>
            </div>
            <h5 class="card-title">{{$data->name}}</h5>
            <div class="badges text-right">
                <p>{{$data->shop->name}}</p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><strong>Price: </strong>{{$data->price}}</li>
                <li class="list-group-item"><strong>Phone: </strong>{{isset($data->shop->phone)?$data->shop->phone:mt_rand(10000000, 99999999)}}</li>
                @if(Auth::check())
                    <li class="list-group-item text-right">
                        <a class="badge badge-success text-white" href="{{ url('add-to-cart/'.$data->id) }}">Book it <i
                                    class="fa fa-shopping-cart"></i></a>
                    </li>
                @endif
            </ul>
            <div class="badges badges-right">
                @if(isset($type) AND $type=='muntazim')
                    @if($data->active)
                        <span class="badge badge-success">Active</span>
                    @else
                        <span class="badge badge-danger">In Active</span>
                    @endif
                    @if($data->public)
                        <span class="badge badge-success">Public</span>
                    @else
                        <span class="badge badge-danger">Private</span>
                    @endif
                    <a class="badge badge-primary"
                       href="{{route('muntazim.foods.edit',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Detail <i class="fa fa-plus"></i></a>
                    <a class="badge badge-primary"
                       href="{{route('muntazim.foods.media',[$data->id,'src'=>'lightbox'])}}"
                       data-provide="lightbox">Media <i class="fa fa-plus"></i></a>
                @endif
            </div>
        </div>
    </div>
</div>