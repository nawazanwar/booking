<nav aria-label="breadcrumb">
    @if(isset($breadcrumbs))
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
            @foreach($breadcrumbs as $leaf)
                @if(!empty($leaf['url']))
                    <li class="breadcrumb-item"><a href="{{$leaf['url']}}">{{$leaf['text']}}</a></li>
                @else
                    <li class="breadcrumb-item active">{{$leaf['text']}}</li>
                @endif
            @endforeach
        </ol>
    @endif
</nav>