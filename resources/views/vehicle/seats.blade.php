@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <h5 class="text-center">Available seats of <strong class="text-primary">{{$modal->name}}</strong>
            </h5>
            <div class="row">
                <div class="col-md-7 h-200" style="overflow-y: auto;">
                    <ul class="list-group list-group-flush">
                        <li class="m-0 list-group-item"><strong>Start City:</strong> {{$modal->terminal->city->name}}
                        </li>
                        <li class="m-0 list-group-item"><strong>Start Time:</strong> {{$modal->time}} daily</li>
                        <li class="m-0 list-group-item"><strong>Start Terminal:</strong> {{$modal->terminal->name}}</li>
                        <li class="m-0 list-group-item"><strong>Total Seats:</strong> {{$modal->seats}}</li>
                    </ul>
                </div>
                <div class="col-md-5 h-200" style="overflow-y: auto;">
                    <h4 class="text-center">Hello Customer</h4>
                    <form class="input-round">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Enter phone number" autocomplete="off"
                                   id="vbook_phone">
                        </div>
                        <p>Available routes</p>
                        @foreach($routes as $route)
                            <input type="radio" name="vroute"
                                   value="{{$route->id}}"> {{\App\Models\City::find($route->city_id)->name}}
                            at {{$route->time}}
                            and ticket is {{$route->ticket}}<br>
                        @endforeach
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mx-auto my-4">
                    <div class="gap-x-3 lh-1 text-center py-5 bg-gray border shadow-9">
                        @for($i =1; $i<=$modal->seats;$i++)
                            @php
                                $bvehicle = new \App\Models\Bvehicle();
                                $seat_id = "seat_".$i;
                                $already_booking = $bvehicle->alreadyBooking($modal->id,$seat_id);
                                if ($already_booking>0){
                                   $diabaled = 'disable';
                                   $icon = 'text-white fa fa-check';
                                }else{
                                  $diabaled = '';
                                  $icon ='text-white fa fa-ticket';
                                }
                            @endphp
                            <div class="w-150 m-3 p-3 product-3 product-media text-center {{$diabaled}}"
                                 id="seat_{{$i}}_parent">
                                <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                <span class="badge badge-pill badge-info badge-pos-right"
                                      id="seat_{{$i}}_icon"><i
                                            class="{{$icon}}"></i></span>
                                @if($already_booking>0)
                                    <a class="btn btn-xs btn-warning text-white border-0 mt-2 mb-1"
                                       style="margin-left: -7px !important;cursor: not-allowed">Already Booked</a>
                                @else
                                    @if (!Auth::guest())
                                        <a class="btn badge-primary btn-xs cursor-pointer text-white mt-2 mb-1"
                                           style="margin-left: -7px !important;"
                                           id="seat_{{$i}}" onclick="bookMe('seat_{{$i}}','{{$modal->id}}');">Booked
                                            It</a>
                                    @else
                                        <a class="btn btn-danger btn-xs text-white mt-2 mb-1 disabled"
                                           style="margin-left: -7px !important;">Login to Book</a>
                                    @endif
                                @endif
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        function bookMe(seat_id, vehicle_id) {
            var phone = $("#vbook_phone").val();
            var route = $("input[name='vroute']:checked").val();
            if (phone == '') {
                alert("please enter phone...");
            } else if (route == undefined) {
                alert("first select the route");
            } else {
                Ajax.call('/booking/vehicle/', {
                    seat_id: seat_id,
                    vehicle_id: vehicle_id,
                    phone: phone,
                    route_id: route
                }, 'GET', function (response) {
                    if (response.status == 'true') {
                        alert("Booked Successfully");
                        $("#" + seat_id + "_icon").find('i').removeClass('fa fa-ticket').addClass('fa fa-check');
                        $("#" + seat_id + "_parent").addClass('disable');
                    } else {
                        alert("Already Found");
                    }
                })
            }
        }
    </script>
@stop