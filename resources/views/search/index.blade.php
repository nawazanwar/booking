@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section">
        <div class="container">

            @if($results)
                <div class="row gap-y">
                    <div class="col-md-12">
                        @include('components.questions', ['questions' => $results['questions']])
                    </div>
                    <div class="col-md-12">
                        @include('components.answers', ['answers' => $results['answers']])
                    </div>
                </div>
            @else
                @include('errors.no-records-found')
            @endif

        </div>
    </section><!-- /.box -->
@stop