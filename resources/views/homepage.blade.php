@extends('layouts.app')
@section('pageTitle', $pageTitle)
@section('content')
    <section class="section overflow-hidden py-5">
        <div class="container">
            <div data-provide="shuffle">
                <nav class="nav nav-center nav-pills mb-3" data-shuffle="filter">
                    <a class="nav-link active" href="#" data-shuffle="button">All</a>
                    <a class="nav-link" href="#" data-shuffle="button" data-group="movie">Movies</a>
                    <a class="nav-link" href="#" data-shuffle="button" data-group="vehicle">Vehicles</a>
                    <a class="nav-link" href="#" data-shuffle="button" data-group="food">Foods</a>
                </nav>
                <div class="row gap-y gap-2" data-shuffle="list">
                    @if(count($latestMovies)>0)
                        @foreach($latestMovies as $data)
                            @include('components.movie.list',['data'=>$data,'type'=>'frontend'])
                        @endforeach
                    @endif
                    @if(count($latestVehicles)>0)
                        @foreach($latestVehicles as $data)
                            @include('components.vehicle.list',['data'=>$data,'type'=>'frontend'])
                        @endforeach
                    @endif
                    @if(count($latestFoods)>0)
                        @foreach($latestFoods as $data)
                            @include('components.food.list',['data'=>$data,'type'=>'frontend'])
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <section class="section bg-fixed text-white p-2 h-400"
             style="background-image: url({{asset('img/bg/old_movie.gif')}})" data-overlay="5">
    </section>
    <section class="section py-0 bg-gray">
        <div class="container-fluid px-0">
            @isset($latestCategories)
                @foreach($latestCategories as $lc)
                    @php
                        if($lc->slug=='cinemas'){
                            $order='';
                            $lc_path = asset(isset($generalSetting['cinema-logo'])?$generalSetting['cinema-logo']:'');
                            $heading="All Cinemas of Pakistan";
                        }elseif($lc->slug=='terminals'){
                            $order='order-md-2';
                             $heading="All Terminal of Pakistan";
                            $lc_path = asset(isset($generalSetting['terminal-logo'])?$generalSetting['terminal-logo']:'');
                        }elseif($lc->slug=='food-points'){
                            $order='';
                             $heading="All Food Points of Pakistan";
                            $lc_path = asset(isset($generalSetting['food-point-logo'])?$generalSetting['food-point-logo']:'');
                        }
                    @endphp
                    <div class="row no-gutters">
                        <div class="col-md-6 bg-img mh-300 {{$order}}"
                             style="background-image: url({{$lc_path}})"></div>
                        <div class="col-10 col-md-4 mx-auto py-8 text-center">
                            <h2 class="small-2 text-light">{{$heading}}</h2>
                            @if($lc->slug=='cinemas')
                                <p class="text-justify">The first to present projected moving pictures to a paying
                                    audience (i.e. cinema) were
                                    the Lumière brothers in December 1895 in Paris. At first, films were very short,
                                    sometimes only a few minutes or less. They were shown at fairgrounds and music halls
                                    or
                                    anywhere a screen could be set up and a room darkened.</p>
                            @elseif($lc->slug=='food-points')
                                <p class="text-justify">
                                    Egyptian food included many vegetables, such as marrows, beans, onions, lentils,
                                    leeks, radishes, garlic and lettuces. They also ate fruit like melons, dates and
                                    figs. Pomegranates were quite expensive and were eaten mainly by the rich. The
                                    Egyptians grew herbs and spices and they made cooking oil.
                                </p>
                            @elseif($lc->slug=='terminals')
                                <p class="text-justify">
                                    1950 - On December 15, after two years of construction and $24 million, the Port
                                    Authority Bus Terminal (PABT) opened to serve commuters and residents of New York.
                                    ... The underlying PATH station was completed a year earlier and now serves
                                    approximately 30,000 commuters daily.
                                </p>
                            @endif
                            <br>
                            <a href="{{route('home',[$lc->slug])}}"
                               class="card-title text-center mb-0 badge badge-primary text-white">View
                                All {{$lc->name}}</a>
                        </div>
                    </div>
                @endforeach
            @endisset
        </div>
    </section>
    <section class="section bg-fixed text-white p-2 h-400"
             style="background-image: url({{asset('img/bg/hollywood.gif')}})" data-overlay="5">
    </section>
    <section class="section">
        <div class="container">
            <h2 class="text-center mb-8">Recent Blog Posts</h2>
            <div class="row gap-y">
                <div class="col-md-6 col-lg-4">
                    <div class="card border hover-shadow-6">
                        <a href="#"><img class="card-img-top" src="/img/thumb/1.jpg" alt="Card image cap"></a>
                        <div class="p-6 text-center">
                            <p><a class="small-5 text-lighter text-uppercase ls-2 fw-400" href="#">News</a></p>
                            <h5 class="mb-0"><a class="text-dark" href="#">We relocated our office to a new designed
                                    garage</a></h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="card border hover-shadow-6">
                        <a href="#"><img class="card-img-top" src="/img/thumb/2.jpg" alt="Card image cap"></a>
                        <div class="p-6 text-center">
                            <p><a class="small-5 text-lighter text-uppercase ls-2 fw-400" href="#">Marketing</a></p>
                            <h5 class="mb-0"><a class="text-dark" href="#">Top 5 brilliant content marketing
                                    strategies</a></h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 d-none d-lg-flex">
                    <div class="card border hover-shadow-6">
                        <a href="#"><img class="card-img-top" src="/img/thumb/3.jpg" alt="Card image cap"></a>
                        <div class="p-6 text-center">
                            <p><a class="small-5 text-lighter text-uppercase ls-2 fw-400" href="#">Design</a></p>
                            <h5 class="mb-0"><a class="text-dark" href="#">Best practices for minimalist design with
                                    example</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section bg-fixed text-white p-2 h-400"
             style="background-image: url({{asset('img/bg/bollywood.gif')}})" data-overlay="5">
    </section>
    <section class="section">
        <div class="container">
            <div class="row gap-y">
                <div class="col-md-6 col-lg-3 team-1">
                    <a href="#">
                        <img src="/img/avatar/5.jpg" alt="...">
                    </a>
                    <h6>Morgan Guadis</h6>
                    <small>Co-Founder &amp; CEO</small>
                    <div class="social social-gray">
                        <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 team-1">
                    <a href="#">
                        <img src="/img/avatar/6.jpg" alt="...">
                    </a>
                    <h6>John Senating</h6>
                    <small>Co-Founder &amp; CTO</small>
                    <div class="social social-gray">
                        <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 team-1">
                    <a href="#">
                        <img src="/img/avatar/7.jpg" alt="...">
                    </a>
                    <h6>Sandi Hormez</h6>
                    <small>Lead Developer</small>
                    <div class="social social-gray">
                        <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 team-1">
                    <a href="#">
                        <img src="/img/avatar/8.jpg" alt="...">
                    </a>
                    <h6>Animor Tiruse</h6>
                    <small>Designer</small>
                    <div class="social social-gray">
                        <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section bg-fixed text-white p-2 h-400"
             style="background-image: url({{asset('img/bg/pakistani.gif')}})" data-overlay="5">
    </section>
    <section class="section py-10" style="background-image: url({{asset('img/bg/4.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xl-6 mx-auto">

                    <div class="section-dialog bg-primary text-white shadow-6">
                        <h4>Latest news direct to your inbox</h4>
                        <br><br>
                        <p class="text-right small pr-5">Subscribe Now</p>
                        <form class="input-glass input-round" action="" method="post" target="_blank">
                            <div class="input-group">
                                <input type="text" name="EMAIL" class="form-control" placeholder="Enter Email Address">
                                <span class="input-group-append">
                      <button class="btn btn-glass btn-light" type="button">Sign up <i
                                  class="ti-arrow-right fs-9 ml-2"></i></button>
                    </span>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="section bg-fixed text-white p-2 h-400"
             style="background-image: url({{asset('img/bg/chines.gif')}})" data-overlay="5">
    </section>
    <section class="section bg-white">
        <div class="container">
            <div class="row gap-y align-items-center">
                <div class="col-md-5">
                    <p class="text-uppercase small opacity-70 fw-600 ls-1">Head Office</p>
                    <h5>Seattle, WA</h5>
                    <br>
                    <p>2651 Main Street, Suit 124<br>Seattle, WA, 98101</p>
                    <p>Phone: +1 987 123 6548<br>Email: hello@thetheme.io</p>
                    <br>
                    <h6>Follow Us</h6>
                    <div class="social social-sm social-inverse">
                        <a class="social-twitter" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="social-facebook" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="social-instagram" href="#"><i class="fa fa-instagram"></i></a>
                        <a class="social-dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                    </div>
                </div>
                <div class="col-lg-7">
                    <h3>Contact Us</h3>
                    <br>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input class="form-control form-control-lg" type="text" name="name" placeholder="Name">
                        </div>

                        <div class="form-group col-md-6">
                            <input class="form-control form-control-lg" type="email" name="email" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control form-control-lg" rows="4" placeholder="Message"
                                  name="message"></textarea>
                    </div>

                    <button class="btn btn-lg btn-primary" type="submit">Send message</button>

                </div>
            </div>
        </div>
    </section>
    <section class="section bg-fixed text-white p-10 h-400"
             style="background-image: url({{asset('img/bg/horror.gif')}})" data-overlay="5">
    </section>
    <section class="section bg-gray py-2">
        <div class="container">
            <header class="section-header">
                <small>Work</small>
                <h2 class="display-4">Get A Qoute</h2>
                <hr>
                <p class="lead">They original on mountains, drew the support time. The of to graduate into to is the to she.</p>
            </header>

            <form class="form-row input-border" action="../assets/php/sendmail.php" method="POST" data-form="mailer">
                <div class="col-12">
                    <div class="alert alert-success d-on-success">We received your message and will contact you back soon.</div>
                </div>


                <div class="form-group col-sm-6 col-xl-3">
                    <input class="form-control form-control-lg" type="text" name="name" placeholder="Name">
                </div>

                <div class="form-group col-sm-6 col-xl-3">
                    <input class="form-control form-control-lg" type="email" name="email" placeholder="Email">
                </div>

                <div class="form-group col-sm-6 col-xl-3">
                    <input class="form-control form-control-lg" type="text" name="company" placeholder="Company Name">
                </div>

                <div class="form-group col-sm-6 col-xl-3">
                    <select class="form-control form-control-lg" name="budget">
                        <option>Budget</option>
                        <option>Up to $1,000</option>
                        <option>Up to $3,000</option>
                        <option>Up to $5,000</option>
                        <option>Above $5,000</option>
                    </select>
                </div>

                <div class="form-group col-12">
                    <textarea class="form-control form-control-lg" rows="4" placeholder="Project Requirements" name="message"></textarea>
                </div>

                <div class="col-12 text-center">
                    <button class="btn btn-xl btn-block btn-primary" type="submit">Submit Inquiry</button>
                </div>
            </form>

        </div>
    </section>
    <section class="section py-4 shadow-9 border">
        <div data-provide="slider" data-autoplay="true" data-slides-to-show="2" data-css-ease="linear"
             data-speed="12000" data-autoplay-speed="0" data-pause-on-hover="false">
            <div class="p-2">
                <div class="rounded bg-img h-200" style="background-image: url({{asset('img/bg/jungle.gif')}})"></div>
            </div>
            <div class="p-2">
                <div class="rounded bg-img h-200" style="background-image: url({{asset('img/bg/snake.gif')}})"></div>
            </div>
            <div class="p-2">
                <div class="rounded bg-img h-200" style="background-image: url({{asset('img/bg/panda.gif')}})"></div>
            </div>
            <div class="p-2">
                <div class="rounded bg-img h-200" style="background-image: url({{asset('img/bg/cars.gif')}})"></div>
            </div>
        </div>
    </section>
    {{--changed--}}
@stop
