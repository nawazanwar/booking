@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mx-auto my-4">
                    <h5 class="text-center">Available seats of <strong class="text-primary">{{$modal->name}}</strong>
                    </h5>
                    <div class="row">
                        <div class="col-md-7 h-200" style="overflow-y: auto;">
                            <ul class="list-group list-group-flush">
                                <li class="m-0 list-group-item"><strong>City:</strong> {{$modal->cinema->city->name}}
                                </li>
                                <li class="m-0 list-group-item"><strong>Cinema:</strong> {{$modal->cinema->name}}</li>
                                <li class="m-0 list-group-item"><strong>Date:</strong> {{$modal->date}}</li>
                                <li class="m-0 list-group-item"><strong>Start:</strong> {{$modal->start}}</li>
                                <li class="m-0 list-group-item"><strong>End:</strong> {{$modal->end}}</li>
                                <li class="m-0 list-group-item"><strong>Duration:</strong> {{$modal->duration}} minutes
                                </li>
                                <li class="m-0 list-group-item"><strong>Break:</strong> {{$modal->break}} minute</li>
                                <li class="m-0 list-group-item"><strong>Ticket:</strong> {{$modal->ticket}} RS</li>
                                <li class="m-0 list-group-item"><strong>Type:</strong> {{$modal->type}}</li>
                            </ul>
                        </div>
                        <div class="col-md-5 h-200" style="overflow-y: auto;">
                            <h4 class="text-center">Hello Customer</h4>
                            <form class="input-round">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Enter phone number" autocomplete="off"
                                           id="mbook_phone">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 mx-auto my-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab"
                               href="#tab-front_seats-{{$modal->id}}">Front</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab-back_seats-{{$modal->id}}">Back</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab"
                               href="#tab-gallery_seats-{{$modal->id}}">Gallery</a>
                        </li>
                    </ul>
                    <div class="tab-content p-4 text-left">
                        <div class="tab-pane fade show active" id="tab-front_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5 bg-gray border shadow-9">
                                @for($i =1; $i<=$modal->cinema->front;$i++)
                                    @php
                                        $bmovie = new \App\Models\Bmovie();
                                        $front_seat_id = "front_seat_".$i;
                                        $already_booking = $bmovie->alreadyBooking($modal->id,$front_seat_id);
                                        if ($already_booking>0){
                                           $diabaled = 'disable';
                                           $icon = 'text-white fa fa-check';
                                        }else{
                                          $diabaled = '';
                                          $icon ='text-white fa fa-ticket';
                                        }
                                    @endphp
                                    <div class="w-150 m-3 p-3 product-3 product-media text-center {{$diabaled}}"
                                         id="front_seat_{{$i}}_parent">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="front_seat_{{$i}}_icon"><i
                                                    class="{{$icon}}"></i></span>
                                        @if($already_booking>0)
                                            <a class="btn btn-xs btn-warning text-white border-0 mt-2 mb-1"
                                               style="margin-left: -7px !important;cursor: not-allowed">Already Booked</a>
                                        @else
                                            @if (!Auth::guest())
                                                <a class="btn badge-primary btn-xs cursor-pointer text-white mt-2 mb-1"
                                                   style="margin-left: -7px !important;"
                                                   id="front_seat_{{$i}}" onclick="bookMe('front_seat_{{$i}}','{{$modal->id}}');">Booked
                                                    It</a>
                                            @else
                                                <a class="btn btn-danger btn-xs text-white mt-2 mb-1 disabled"
                                                   style="margin-left: -7px !important;">Login to Book</a>
                                            @endif
                                        @endif
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-back_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5 bg-gray border shadow-9">
                                @for($i =1; $i<=$modal->cinema->back;$i++)
                                    @php
                                        $bmovie = new \App\Models\Bmovie();
                                        $back_seat_id = "back_seat_".$i;
                                        $already_booking = $bmovie->alreadyBooking($modal->id,$back_seat_id);
                                        if ($already_booking>0){
                                           $diabaled = 'disable';
                                           $icon = 'text-white fa fa-check';
                                        }else{
                                          $diabaled = '';
                                          $icon ='text-white fa fa-ticket';
                                        }
                                    @endphp
                                    <div class="w-150 m-3 p-3 product-3 product-media text-center {{$diabaled}}"
                                         id="back_seat_{{$i}}_parent">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="back_seat_{{$i}}_icon"><i
                                                    class="{{$icon}}"></i></span>
                                        @if($already_booking>0)
                                            <a class="btn btn-xs btn-warning text-white border-0 mt-2 mb-1"
                                               style="margin-left: -7px !important;cursor: not-allowed">Already Booked</a>
                                        @else
                                            @if (!Auth::guest())
                                                <a class="btn badge-primary btn-xs cursor-pointer text-white mt-2 mb-1"
                                                   style="margin-left: -7px !important;"
                                                   id="back_seat_{{$i}}" onclick="bookMe('back_seat_{{$i}}','{{$modal->id}}');">Booked
                                                    It</a>
                                            @else
                                                <a class="btn btn-danger btn-xs text-white mt-2 mb-1 disabled"
                                                   style="margin-left: -7px !important;">Login to Book</a>
                                            @endif
                                        @endif
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab-gallery_seats-{{$modal->id}}">
                            <div class="gap-x-3 lh-1 text-center py-5 bg-gray border shadow-9">
                                @for($i =1; $i<=$modal->cinema->gallery;$i++)
                                    @php
                                        $bmovie = new \App\Models\Bmovie();
                                        $gallery_seat_id = "gallery_seat_".$i;
                                        $already_booking = $bmovie->alreadyBooking($modal->id,$gallery_seat_id);
                                        if ($already_booking>0){
                                           $diabaled = 'disable';
                                           $icon = 'text-white fa fa-check';
                                        }else{
                                          $diabaled = '';
                                          $icon ='text-white fa fa-ticket';
                                        }
                                    @endphp
                                    <div class="w-150 m-3 p-3 product-3 product-media text-center {{$diabaled}}"
                                         id="gallery_seat_{{$i}}_parent">
                                        <img src="{{asset('img/icon/chair.png')}}" class="w-40px h-40px">
                                        <span class="font-weight-bold badge badge-primary badge-pill badge-pos-left">{{$i}}</span>
                                        <span class="badge badge-pill badge-info badge-pos-right"
                                              id="gallery_seat_{{$i}}_icon"><i
                                                    class="{{$icon}}"></i></span>
                                        @if($already_booking>0)
                                            <a class="btn btn-xs btn-warning text-white border-0 mt-2 mb-1"
                                               style="margin-left: -7px !important;cursor: not-allowed">Already Booked</a>
                                        @else
                                            @if (!Auth::guest())
                                                <a class="btn badge-primary btn-xs cursor-pointer text-white mt-2 mb-1"
                                                   style="margin-left: -7px !important;"
                                                   id="gallery_seat_{{$i}}" onclick="bookMe('gallery_seat_{{$i}}','{{$modal->id}}');">Booked
                                                    It</a>
                                            @else
                                                <a class="btn btn-danger btn-xs text-white mt-2 mb-1 disabled"
                                                   style="margin-left: -7px !important;">Login to Book</a>
                                            @endif
                                        @endif
                                    </div>
                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script>
        function bookMe(seat_id, movie_id) {
            var phone = $("#mbook_phone").val();
            if (phone == '') {
                alert("please enter phone...");
            }  else {
                Ajax.call('/booking/movie/', {
                    seat_id: seat_id,
                    movie_id: movie_id,
                    phone: phone
                }, 'GET', function (response) {
                    if (response.status == 'true') {
                        alert("Booked Successfully");
                        $("#" + seat_id + "_icon").find('i').removeClass('fa fa-ticket').addClass('fa fa-check');
                        $("#" + seat_id + "_parent").addClass('disable');
                    } else {
                        alert("Already Found");
                    }
                })
            }
        }
    </script>
@stop