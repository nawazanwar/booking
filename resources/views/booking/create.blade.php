@extends('layouts.app')
@section('pageTitle', $pageTitle)

@section('content')
    <section class="section py-3">
        <div class="container">
            <header class="section-header p-0 m-0">
                <h2>Ready for Booking</h2>
            </header>
            <div class="row">
                @if($modal->categoryType()=='cinema')
                    <div class="col-md-6 mx-auto">
                        {{$modal->parent()->front_seats}}
                        <div class="row gap-y">
                            <p>Front Seats</p>
                            <div class="col-md-2 col-lg-2 h-100px">
                                <a class="card card-body border hover-shadow-6 text-center py-6" style="background-image: url({{asset('img/icon/front_chair.png')}});background-position: center center;background-repeat: no-repeat;background-size: contain">
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="col-md-6 mx-auto">
                    <form class="input-round">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Text input">
                        </div>
                        <div class="form-group">
                            <select class="form-control" placeholder="Select input">
                                <option>Select</option>
                                <option>Startup</option>
                                <option>Business</option>
                                <option>Enterprise</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" placeholder="Textarea"></textarea>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@</span>
                            </div>
                            <input type="text" class="form-control" placeholder="Username">
                        </div>

                        <div class="form-group input-group">
                            <input type="text" class="form-control" placeholder="Email address">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <div class="input-group-prepend">
                                <button class="btn btn-primary" type="button">Subscribe</button>
                            </div>
                            <input type="text" class="form-control" placeholder="Your Email Address">
                        </div>
                        <div class="form-group input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button">Go!</button>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <input class="form-control is-valid" type="text" value="Valid input">
                            <div class="valid-feedback">Looks good.</div>
                        </div>

                        <div class="form-group">
                            <input class="form-control is-invalid" type="text" value="Invalid input">
                            <div class="invalid-feedback">Provide a valid value.</div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </section>
@stop