@extends('layouts.app')
@section('pageTitle','404')
@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <main class="main-content text-center pb-lg-8">
                        <div class="container">

                            <h1 class="display-1 text-muted mb-7">Page Not Found</h1>
                            <p class="lead">Seems you're looking for something that doesn't exist.</p>
                            <br>
                            <button class="btn btn-secondary w-150 mr-2" type="button" onclick="window.history.back();">
                                Go back
                            </button>
                            <a class="btn btn-secondary w-150" href="{{route('home')}}">Return Home</a>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </section>
@endsection