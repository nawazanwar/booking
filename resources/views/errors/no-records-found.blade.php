<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <main class="main-content text-center pb-lg-8">
                    <div class="container">
                        <h1 class="display-1 text-muted mb-7">{{__('faqs.nothing_found_here')}}</h1>
                        <p class="lead">{{__('faqs.create_an_account_start_posting')}}</p>
                        <br>
                        <button class="btn btn-secondary w-150 mr-2" type="button"
                                onclick="window.history.back();">{{__('faqs.go_back')}}</button>
                        <a class="btn btn-secondary w-150 mr-2" href="{{route('home')}}">{{__('faqs.home')}}</a>
                    </div>
                </main>
            </div>
        </div>
    </div>
</section>