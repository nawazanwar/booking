<?php
return [
    'title_homepage' => 'Home',
    'custom_upload_heading' => 'Graphic Information',
    'site_name' => 'Meme',
    'dashboard_title' => 'Dashboard',
    /*this is for the comment*/
    'all' => 'All',
    'upload_post' => 'Upload a Post',
    'upload_post_size' => 'Maximum file upload size 2MB',
    'title_placeholder' => 'Enter Title',
    'post_placeholder' => 'Start your question with "What", "How", "Why", etc.',
    'title' => 'Title',
    'slug' => 'Slug',
    'slug_placeholder' => 'Enter Slug',
    'tag_placeholder' => 'tag1,tag2,tag3,...',
    'description_placeholder' => 'Enter Description',
    'meta_keywords' => 'Meta Keywords',
    'meta_keywords_placeholder' => 'Enter Meta Keywords',
    'meta_description' => 'Meta Description',
    'meta_description_placeholder' => 'Enter Meta Description',
    'post_content' => 'Question',
    'answer_content' => 'Answer',
    'content' => 'Description',
    'content_placeholder' => 'Enter Description',
    'featured' => 'Featured',
    'order' => 'Order',
    'order_placeholder' => 'Enter Order',
    'parent' => 'Parent',
    'short_url' => 'Short Url',
    /*code for the view of single*/

    'media' => 'Media',
    'description' => 'Description',
    'meta' => 'Meta',
    //code for the pic section
    'pick_heading' => 'Pick a section',
    'pick_description' => 'Submitting to the right section to make sure your post gets the right exposure it deserves!',
    //code for buttons
    'next_button' => 'Next',
    'login_button' => "Login",
    'register_button' => 'Register',
    'upload_meme_button' => 'Upload',
    'create_meme_button' => 'Create Meme',
    'post_button' => 'Post',
    'back_button' => 'Back',
    'show' => 'Show',
    //code for the some messages used in profiles section for displaying posts
    'create_post_message' => 'List of the posts created by',
    'upvote_post_message' => 'List of the post Upvoted by',
    'downvote_post_message' => 'List of the post Downvoted by',
    'comment_post_message' => 'List of the post Commented by',
    //some important texts
    'comments' => 'Comments',
    'up_votes' => 'Up Votes',
    'down_votes' => 'Down Votes',
    //some messages about auth
    'already_register_message' => 'Sory you have already register to this email so please login',
    //here are the some placeholders
    'cpassword' => 'Confirm Password',
    'name_placeholder' => 'First-Last Name....',
    'username_placeholder' => 'Enter User Name...',
    'phone_placeholder' => 'Enter Phone...',
    'mobile_placeholder' => 'Enter Mobile ...',
    'country_placeholder' => 'Enter Country...',
    'state_placeholder' => 'Enter State...',
    'city_placeholder' => 'Enter City...',
    'zip_placeholder' => 'Enter Zip Code...',
    'email_placeholder' => 'Enter email...',
    'password_placeholder' => 'Enter Password...',
    'cpassword_placeholder' => 'Confirmed Password...',
    'address_placeholder' => 'Enter Address...',
    //some heading related to form
    'new_register_social_heading' => 'Use the social login buttons If you have already Account with Facebook or Twitter',
    'signin_facebook' => 'Sign In With FaceBook',
    'signin_twitter' => 'Sign In With Twitter',
    'login_form_heading' => 'Login Form',
    'register_form_heading' => 'Register Form',
    'name_heading' => 'Name',
    'fname_heading' => 'First Name',
    'lname_heading' => 'Last Name',
    'phone_heading' => 'Phone Number',
    'mobile_heading' => 'Mobile Number',
    'gender_heading' => 'Gender',
    'country_heading' => 'Country',
    'state_heading' => 'State',
    'city_heading' => 'City',
    'zip_heading' => 'Zip Code',
    'male_heading' => 'Male',
    'female_heading' => 'Female',
    'address_heading' => 'Address',
    'email_heading' => 'User Name',
    'password_heading' => 'Password',
    'cpassword_heading' => 'Confirm Password',
    'image_heading' => 'Profile Image',
    //some main heading for the forms
    'user_setting' => 'User Settings',
    'all_settings' => 'All Settings',
    'general_details' => 'General Details',
    'login_details' => 'Login Details',
    'profile_details' => 'Profile Details',
    'profile' => 'Profile',
    'address_details' => 'Address Details',
    'update_setting' => 'Update Settings',
    'twitter_setting_update' => 'Update settings for Twitter',
    'facebook_setting_update' => 'Update settings for Facebook',
    //some headings for the optional settings
    'optional_profile' => 'Profile Image is optional if you leave them blank then use the orignal Image and if you are updating the settings for the social media then please donot update the profile image because its take from the live url',
    'optional_passwords' => 'Profile Image is optionals if you leave them blank then use the orignal password and if you are updating the settings for the social media then please donot update the password because its take from the live',
    //some profile page headings
    'total_posts_heading' => 'Total Posts',
    'total_comments_heading' => 'Total Comments',
    'total_upvotes_heading' => 'Total UpVotes',
    'total_downvotes_heading' => 'Total DownVotes',
    'tags' => 'Tags',
    'tags_placeholder' => 'Add tags...',

    // Top Menu
    'access' => 'Access',
    'roles' => 'Roles',
    'all_roles' => 'All Roles',
    'new_role' => 'New Role',
    'permissions' => 'Permissions',
    'all_permissions' => 'All Permissions',
    'sync_permissions' => 'Sync Permissions',
    'add_question' => 'Add Question',
    'notifications' => 'Notifications',

    'analytics' => 'Analytics',

    'settings' => 'Settings',

    /*topics*/

    'topics' => 'Topics',
    'all_topics' => 'All Topics',
    'new_topic' => 'New Topic',
    'create_topic' => 'Create Topic',
    'edit_topic' => 'Edit Topic',
    'delete_topic' => 'Delete Topic',
    'created_this_topic' => 'Created this topic',

    /*posts*/

    'posts' => 'Questions',
    'post' => 'View Question',
    'all_posts' => 'All Questions',
    'new_post' => 'New Question',
    'create_post' => 'Create Question',
    'edit_post' => 'Edit Question',
    'delete_post' => 'Delete Question',
    'created_by' => 'Created By',
    'click_to_view_complete_post' => 'Click to View the complete question',
    'click_to_view_topic_posts' => 'Click to view all posts',
    'search_post' => 'Search Qestion by title here....',

    /*answers*/

    'answers' => 'Answers',
    'all_answers' => 'All Answers',
    'new_answer' => 'New Answer',
    'create_answer' => 'Create Answer',
    'edit_answer' => 'Edit Answer',
    'delete_answer' => 'Delete Answer',
    'no_answer_yet' => 'No answer yet',
    'total_answers' => 'Total answers',
    'ans_this_question' => 'Ans this Question',
    'click_to_view_complete_answer' => 'Click to View the complete answer',
    'view_your_answers' => 'View Your Answers',

    /*users*/

    'users' => 'Users',
    'all_users' => 'All Users',
    'new_user' => 'New User',

    // buttons
    'options' => 'Options',
    'edit' => 'Edit',
    'view' => 'View',
    'delete' => 'Delete',
    'view' => 'View',
    'update' => 'Update',
    'create' => 'Create',
    'save' => 'Save',
    'edit_profile' => 'Edit Profile',
    'view_profile' => 'View Profile',

    // labels
    'no_data_found' => 'No Data Found',
    'filter' => 'Filter',
    'filter_all' => 'Filter All',
    'logo' => 'Logo',
    'created_date' => 'Created Date',
    'updated_date' => 'Updated Date',
    'label' => 'Label',
    'name' => 'Name',
    'password' => 'Password',
    'email' => 'Email',
    'roles' => 'Roles',
    'phone' => 'Phone',
    'gender' => 'Gender',
    'country' => 'Country',
    'state' => 'State',
    'bio' => 'Bio',
    'username' => 'Username',
    'active' => 'Active',
    'inactive' => 'In Active',
    'public' => 'Public',
    'private' => 'Private',
    'delete_avatar' => 'Delete Avatar',
    'choose_new_avatar' => 'Choose New Avatar',
    'male' => 'Male',
    'female' => 'Female',
    'unspecified' => 'Unspecified',
    'no_featured' => 'Not Featured',
    'detail' => 'Detail',
    'question' => 'Question',
    'answer' => 'Post Answer',
    'choose' => 'Choose Action',
    'created_at' => 'Date Created',
    'updated_at' => 'Date Updated',

    //publish

    'publish' => 'Publish',
    'unpublish' => 'Un Publish',
    'publish_at' => 'Date Published',
    'publish_posts' => 'Publish Posts',
    'unpublish_posts' => 'Unpublish Posts',

    // user
    'edit_user' => 'Edit User',
    'create_user' => 'Create User',
    'delete_user' => 'Delete User',
    'assign_user' => 'Assign User',

    //roles
    'assign_role' => 'Assign Role',
    'edit_role' => 'Edit Role',
    'create_role' => 'Create Role',
    'delete_role' => 'Delete Role',

    //permissions

    'assign_permission' => 'Assign Permission',

    // messages
    'are_you_sure_to_delete' => 'Are you sure you want to delete this item?',
    'are_you_sure_to_publish' => 'Are you sure you want to publish this item?',
    'are_you_sure_to_un_publish' => 'Are you sure you want to Un publish this item?',
    'publish_answer_message' => 'Your Answer has been submitted and will be publish within few minutes',
    'unpublish_answer_message' => 'Your Answer has been approved.',
    'post_has_been_created' => 'New question has been created successfully',

    //sharing buttons

    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'linked_in' => 'Linked In',
    'google_plus' => 'Google Plus ',

    /*votes*/

    'vote_up' => 'Vote Up',
    'vote_down' => 'Vote Down',
    'latest_votes' => 'Latest votes',
    'total_votes' => 'Total votes',
    'like' => 'Like',
    'dislike' => 'Dislike',
    'this_post' => 'this Question',
    'no_vote_yet' => 'No vote yet',

    /*Report*/

    'report' => 'Report',

    /*Follow*/

    'follow' => 'Follow',
    'follows_post' => 'Followed Questions',

    /*profile*/

    'all_notifications' => 'All Notifications',

    /*Favorite*/
    'favorite_topics' => 'Favorite Topics',
    'favorite_post' => 'Favorite Question',
    'favorite' => 'Favorite',
    'add_to_favorite' => 'Add Favorite',

    /*Book Mark*/

    'bookmark' => 'Book Mark',
    'bookmark_post' => 'Bookmark Question',

    /*Comments*/

    'comment_placeholder' => 'Please Add Comment...',
    'profile_posts' => 'All Posts',
    'comment_button' => 'Comment',
    'all_comments' => 'All Comments',

    /*Report*/

    'report_reason' => 'Reporting Reason',
    'report_reason_placeholder' =>'Enter the reporting reason....'

];
