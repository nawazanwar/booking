<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model implements Permissions
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_follow');
                    break;
                case 'create':
                case 'store':
                    return array('add_follow');
                    break;
                case 'edit':
                default:
                    return array();
            }
        }
        return array(
            'view_follow',
            'add_follow',
        );
    }
}
