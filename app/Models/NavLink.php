<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class NavLink extends Model implements Permissions
{

    protected $table = 'nav_links';

    public function nav() {
        return $this->belongsTo(Nav::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_nav_link');
                    break;
                case 'create':
                case 'store':
                    return array('create_nav_link');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_nav_link');
                    break;
                case 'delete':
                    return array('delete_nav_link');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_nav_link',
            'create_nav_link',
            'edit_nav_link',
            'delete_nav_link',
        );
    }
}
