<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model implements Permissions
{
    protected $fillable = ['id', 'cinema_id', 'post_id','name','created_at', 'updated_at'];

    public function cinema()
    {
        return $this->belongsTo(Cinema::class);
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function media()
    {
        return $this->belongsToMany(Media::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_movie');
                    break;
                case 'create':
                case 'store':
                    return array('create_movie');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_movie');
                    break;
                case 'delete':
                    return array('delete_movie');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_movie',
            'create_movie',
            'edit_movie',
            'delete_movie',
        );
    }
}

