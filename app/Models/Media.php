<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Media extends Model implements Permissions
{
    protected $fillable = ['name', 'src'];

    public function cinemas()
    {
        return $this->belongsToMany(Cinema::class);
    }

    public function terminals()
    {
        return $this->belongsToMany(Terminal::class);
    }

    public function shops()
    {
        return $this->belongsToMany(Shop::class);
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class);
    }

    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function foods()
    {
        return $this->belongsToMany(Food::class);
    }


    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_media');
                    break;
                case 'create':
                case 'store':
                    return array('create_media');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_media');
                    break;
                case 'delete':
                    return array('delete_media');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_media',
            'create_media',
            'edit_media',
            'delete_media',
        );
    }
}
