<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;

use App\Models\User;

class SocialProvider extends Model
{
    protected $fillable = ['provider_id', 'provider', 'user_id'];

    function User()
    {
        return $this->belongsTo(User::class);
    }
}
