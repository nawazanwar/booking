<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model implements Permissions
{
    protected $fillable = ['name', 'value', 'type'];

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_setting');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_setting');
                    break;
                case 'delete':
                    return array('delete_setting');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_setting',
            'create_setting',
            'edit_setting',
            'delete_setting',
        );
    }
}
