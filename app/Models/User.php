<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Interfaces\Permissions;

class User extends Authenticatable implements Permissions
{
    use Notifiable, SoftDeletes;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function getAvatarAttribute($value)
    {

        if ($value) {

            $s3 = \Storage::disk('s3');

            $client = $s3->getDriver()->getAdapter()->getClient();

            $expiry = "+10 minutes";

            $command = $client->getCommand('GetObject', [

                'Bucket' => \Config::get('filesystems.disks.s3.bucket'),

                'Key' => config('faqs.s3.avatars') . '/' . $value,

            ]);

            $request = $client->createPresignedRequest($command, $expiry);

            return (string)$request->getUri();

        }

        return '/img/defaults/user.jpg';
    }

    public function genderIcon()
    {
        if ($this->gender == 'female') {
            return '<i class="fa fa-female"></i>';
        } else if ($this->gender == 'male') {
            return '<i class="fa fa-male"></i>';
        } else {
            return '<i class="fa fa-question"></i>';
        }
    }

    public function publishedQuestions()
    {

        return $this->hasMany('App\Models\Question')->where('is_publish', 1);

    }

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function unpublishedQuestions()
    {

        return $this->hasMany('App\Models\Question')->where('is_publish', 0);

    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\Favorite');
    }

    public function votes()
    {
        return $this->hasManyThrough('App\Models\Vote', 'App\Models\Question');
    }

    public function upVotes()
    {
        return $this->hasMany('App\Models\Vote')->where('type', 'up');
    }

    public function downVotes()
    {
        return $this->hasMany('App\Models\Vote')->where('type', 'down');
    }

    function providers()
    {
        return $this->belongsTo(SocialProvider::class);
    }

    public function roleName()
    {
        return $this->roles->pluck('name')->first();
    }

    public function roleTitle()
    {
        return $this->roles->pluck('label')->first();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function getId()
    {
        return $this->id;
    }

    public function assignRole($role)
    {
        $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$role->intersect($this->roles);

    }

    public function ability($permission = null)
    {

        $id = $this->id;

        $already_permission = Permission::whereHas('users', function ($query) use ($id) {

            $query->where('id', $id);

        })->where('name', $permission)->count();

        if ($already_permission == 1) {

            return true;

        } else {

            return !is_null($permission) && $this->checkPermission($permission);

        }
    }

    protected function checkPermission($permission)
    {
        $permissions = $this->getAllPermissionsFromAllRoles();
        if ($permissions === true) {
            return true;
        }
        $permissionArray = is_array($permission) ? $permission : [$permission];
        return count(array_intersect($permissions, $permissionArray));
    }

    protected function getAllPermissionsFromAllRoles()
    {
        $permissions = array();
        $roles = $this->roles->load('permissions');
        if (!$roles) {
            return true;
        }
        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions[] = $permission->toArray();
            }
        }
        return array_map('strtolower', array_unique(array_flatten(array_map(function ($permission) {
            return $permission['name'];
        }, $permissions))));

    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_user', 'handle_dashboard');
                    break;
                case 'create':
                case 'store':
                    return array('create_user');
                    break;
                case 'edit':
                case 'update':
                case 'assign':
                    return array('edit_user', 'assign_user');
                    break;
                case 'delete':
                    return array('delete_user');
                    break;
                default:
                    return array();
            }

        }
        return array(
            'handle_dashboard',
            'view_user',
            'create_user',
            'edit_user',
            'delete_user',
            'assign_user',
        );
    }
}
