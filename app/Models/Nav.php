<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Nav extends Model implements Permissions
{
    protected $table = 'nav';

    public function links() {
        return $this->hasMany(NavLink::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_nav');
                    break;
                case 'create':
                case 'store':
                    return array('create_nav');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_nav');
                    break;
                case 'delete':
                    return array('delete_nav');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_nav',
            'create_nav',
            'edit_nav',
            'delete_nav',
        );
    }
}
