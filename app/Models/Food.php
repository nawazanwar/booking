<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Food extends Model implements Permissions
{
    protected $fillable = ['id', 'shop_id','name','post_id', 'created_at', 'updated_at'];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_food');
                    break;
                case 'create':
                case 'store':
                    return array('create_food');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_food');
                    break;
                case 'delete':
                    return array('delete_food');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_food',
            'create_food',
            'edit_food',
            'delete_food',
        );
    }
}

