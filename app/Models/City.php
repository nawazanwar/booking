<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class City extends Model implements Permissions
{
    public $timestamps = false;
    protected $fillable = ['id', 'country_id', 'name'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function routes()
    {
        return $this->hasMany(Route::class);
    }

    public function terminals()
    {
        return $this->hasMany(Terminal::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_city');
                    break;
                case 'create':
                case 'store':
                    return array('add_city');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_city');
                    break;
                case 'delete':
                    return array('delete_city');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_city',
            'add_city',
            'edit_city',
            'delete_city',
        );
    }
}
