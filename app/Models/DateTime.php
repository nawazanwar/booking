<?php

namespace App\Models;

use Carbon\Carbon;


trait DateTime
{

    function getFormattedTime($date, $type = null)
    {
        $days = [
            'Friday' => 'Friday',
            'Saturday' => 'Saturday',
            'Sunday' => 'Sunday',
            'Monday' => 'Monday',
            'Tuesday' => 'Tuesday',
            'Wednesday' => 'Wednesday',
            'Thursday' => 'Thursday',
        ];

        $months = [
            1 => 'جنوری',
            2 => 'فروری',
            3 => 'مارچ',
            4 => 'اپریل',
            5 => 'مئ',
            6 => 'جون',
            7 => 'جولائ',
            8 => 'اگست',
            9 => 'ستمبر',
            10 => 'اکتوبر',
            11 => 'نومبر',
            12 => 'دسمبر',
        ];

        $today = Carbon::now();
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $date);

        if ($type) {

            switch ($type) {
                case 'd':
                    return $datetime->day;
                    break;
                case 'F':
                    return $months[$datetime->month];
                    break;
                case 'Y':
                    return $datetime->year;
                    break;
                default:
                    return $datetime->day . ' ' . $months[$datetime->month] . ' ' . $datetime->year;
                    break;
            }

        }

        if ($today->day == $datetime->day) {
            return 'Today';
        } else if (($today->day - $datetime->day) == 1) {
            return 'Yesterday';
        } else {
            return $datetime->day . ' ' . $months[$datetime->month] . ' ' . $datetime->year;
        }
    }

}
