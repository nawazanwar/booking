<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model implements Permissions
{
    use SoftDeletes, DateTime;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'slug','type','updated_at','created_at'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryName()
    {
        return $this->categories->pluck('name')->first();
    }

    public function parent()
    {
        return $this->categories[0];
    }

    public function categoryType()
    {
        return $this->categories->pluck('type')->first();
    }

    public function frontSeats()
    {
        return $this->categories->pluck('front_seats')->first();
    }

    public function backSeats()
    {
        return $this->categories->pluck('back_seats')->first();
    }

    public function gallerySeats()
    {
        return $this->categories->pluck('gallery_seats')->first();
    }

    public function categorySlug()
    {
        return $this->categories->pluck('slug')->first();
    }

    public function seats()
    {
        return $this->categories->pluck('seats')->first();
    }

    public function movies()
    {
        return $this->hasMany(Movie::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function foods()
    {
        return $this->hasMany(Food::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getImage($size)
    {
        /* $images = $this->media->pluck('path', 'size');
         return '/uploads/posts/' . $images[$size];*/
    }

    public function getSocialMediaImage()
    {
        return $this->social_media_image ? '/uploads/posts/' . $this->social_media_image : $this->getImage('full');
    }

    function getTime($type = null)
    {
        return $this->getFormattedTime($this->created_at, $type);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_post');
                    break;
                case 'create':
                case 'store':
                    return array('create_post', 'publish_post');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_post');
                    break;
                case 'delete':
                    return array('delete_post');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_post',
            'create_post',
            'publish_post',
            'edit_post',
            'delete_post',
        );
    }

}
