<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Terminal extends Model implements Permissions
{
    protected $fillable = ['id', 'category_id', 'city_id','name','created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_terminal');
                    break;
                case 'create':
                case 'store':
                    return array('create_terminal');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_terminal');
                    break;
                case 'delete':
                    return array('delete_terminal');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_terminal',
            'create_terminal',
            'edit_terminal',
            'delete_terminal',
        );
    }
}

