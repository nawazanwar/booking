<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Country extends Model implements Permissions
{
    public $timestamps = false;
    protected $fillable = ['id', 'name', 'code'];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_country');
                    break;
                case 'create':
                case 'store':
                    return array('add_country');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_country');
                    break;
                case 'delete':
                    return array('delete_country');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_country',
            'add_country',
            'edit_country',
            'delete_country',
        );
    }
}
