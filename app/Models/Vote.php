<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model implements Permissions
{

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_vote');
                    break;
                case 'create':
                case 'store':
                    return array('add_vote');
                    break;
                case 'edit':
                default:
                    return array();
            }
        }
        return array(
            'view_vote',
            'add_vote',
        );
    }
}
