<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Favorite extends Model implements Permissions
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function topic()
    {
        return $this->belongsTo(Comment::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function answer()
    {
        return $this->belongsTo(Answer::class);
    }

    public function comment()
    {
        return $this->belongsTo(Comment::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_favorite');
                    break;
                case 'create':
                case 'store':
                    return array('add_favorite');
                    break;
                case 'edit':
                default:
                    return array();
            }
        }
        return array(
            'view_favorite',
            'add_favorite',
        );
    }
}
