<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model implements Permissions
{

    use SoftDeletes;

    // Carbon instance fields
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function answers()
    {
        return $this->belongsTo(Answer::class, 'answer_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function userImage($user_id)
    {
        return $this->user->pluck('image')->first();
    }

    public function votes()
    {
        return $this->hasMany(Vote::class)->orderBy('id', 'DESC');
    }

    public function upVotes()
    {
        return $this->hasMany(Vote::class)->whereType('up');
    }

    public function downVotes()
    {
        return $this->hasMany(Vote::class)->whereType('down');
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_comment');
                    break;
                case 'create':
                case 'store':
                    return array('create_comment', 'publish_comment');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_comment');
                    break;
                case 'delete':
                    return array('delete_comment');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_comment',
            'create_comment',
            'publish_comment',
            'edit_comment',
            'delete_comment',
        );
    }

}
