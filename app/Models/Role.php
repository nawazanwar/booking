<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Role extends Model implements Permissions
{
    use Notifiable, SoftDeletes;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public function getCreatedAtAttribute($value)
    {
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $datetime->format('d M, Y - H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $value);
        return $datetime->format('d M, Y - H:i:s');
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_role');
                    break;
                case 'create':
                case 'store':
                    return array('create_role');
                    break;
                case 'edit':
                case 'update':
                case 'assign':
                    return array('edit_role', 'assign_role');
                    break;
                case 'delete':
                    return array('delete_role');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_role',
            'create_role',
            'edit_role',
            'assign_role',
            'delete_role',
        );
    }

}
