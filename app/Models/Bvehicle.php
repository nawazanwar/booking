<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Bvehicle extends Model
{
    use SoftDeletes, DateTime;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function alreadyBooking($vehicle_id, $seat_id)
    {
        $already_booked = DB::table('bvehicles')->where('vehicle_id', '=', $vehicle_id)->where('seat_id', '=', $seat_id)->whereDate('created_at', Carbon::today());
        return $already_booked->count();
    }
}
