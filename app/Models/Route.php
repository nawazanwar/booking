<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Route extends Model implements Permissions
{
    protected $fillable = ['id', 'vehicle_id', 'city_id', 'time', 'duration', 'created_at', 'updated_at'];

    public function vehicles()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function cities()
    {
        return $this->belongsTo(City::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_route');
                    break;
                case 'create':
                case 'store':
                    return array('create_route');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_route');
                    break;
                case 'delete':
                    return array('delete_route');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_route',
            'create_route',
            'edit_route',
            'delete_route',
        );
    }
}

