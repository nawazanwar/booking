<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model implements Permissions
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'slug','created_at'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryName()
    {
        return $this->categories->pluck('name')->first();
    }

    public function categorySlug()
    {
        return $this->categories->pluck('slug')->first();
    }

    

    function getTime($type = null)
    {
        $days = [
            'Friday' => 'جمعہ',
            'Saturday' => 'ہفتہ',
            'Sunday' => 'اتوار',
            'Monday' => 'پیر',
            'Tuesday' => 'منگل',
            'Wednesday' => 'بدھ',
            'Thursday' => 'جمعرات',
        ];

        $months = [
            1 => 'جنوری',
            2 => 'فروری',
            3 => 'مارچ',
            4 => 'اپریل',
            5 => 'مئ',
            6 => 'جون',
            7 => 'جولائ',
            8 => 'اگست',
            9 => 'سپتمبر',
            10 => 'اکتوبر',
            11 => 'نومبر',
            12 => 'دسمبر',
        ];

        $today = Carbon::now();
        $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at);

        if ($type) {

            switch ($type) {
                case 'd':
                    return $datetime->day;
                    break;
                case 'F':
                    return $months[$datetime->month];
                    break;
                case 'Y':
                    return $datetime->year;
                    break;
                default:
                    return $datetime->day . ' ' . $months[$datetime->month] . ' ' . $datetime->year;
                    break;
            }

        }

        if ($today->day == $datetime->day) {
            return 'آج';
        } else if (($today->day - $datetime->day) == 1) {
            return 'کل';
        } else {
            return $datetime->day . ' ' . $months[$datetime->month] . ' ' . $datetime->year;
        }
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_page');
                    break;
                case 'create':
                case 'store':
                    return array('create_page', 'publish_page');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_page');
                    break;
                case 'delete':
                    return array('delete_page');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_page',
            'create_page',
            'publish_page',
            'edit_page',
            'delete_page',
        );
    }

}
