<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model implements Permissions
{
    protected $fillable = ['id', 'terminal_id', 'post_id','name','time', 'seats', 'transport', 'booking', 'property', 'created_at', 'updated_at'];

    public function terminal()
    {
        return $this->belongsTo(Terminal::class);
    }
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }


    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_vehicle');
                    break;
                case 'create':
                case 'store':
                    return array('create_vehicle');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_vehicle');
                    break;
                case 'delete':
                    return array('delete_vehicle');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_vehicle',
            'create_vehicle',
            'edit_vehicle',
            'delete_vehicle',
        );
    }
}

