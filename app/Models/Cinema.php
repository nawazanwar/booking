<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Cinema extends Model implements Permissions
{
    protected $fillable = ['id', 'category_id', 'city_id', 'name', 'created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public function movies()
    {
        return $this->hasMany(Movie::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_cinema');
                    break;
                case 'create':
                case 'store':
                    return array('create_cinema');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_cinema');
                    break;
                case 'delete':
                    return array('delete_cinema');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_cinema',
            'create_cinema',
            'edit_cinema',
            'delete_cinema',
        );
    }
}

