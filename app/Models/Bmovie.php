<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Bmovie extends Model
{
    use SoftDeletes, DateTime;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function alreadyBooking($movie_id, $seat_id)
    {
        $already_booked = DB::table('bmovies')->where('movie_id', '=', $movie_id)->where('seat_id', '=', $seat_id)->whereDate('created_at', Carbon::today());
        return $already_booked->count();
    }
}
