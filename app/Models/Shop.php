<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model implements Permissions
{
    protected $fillable = ['id', 'category_id','city_id','name','created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public function foods()
    {
        return $this->hasMany(Food::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {
        if ($middleware) {
            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_shop');
                    break;
                case 'create':
                case 'store':
                    return array('create_shop');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_shop');
                    break;
                case 'delete':
                    return array('delete_shop');
                    break;
                default:
                    return array();
            }
        }
        return array(
            'view_shop',
            'create_shop',
            'edit_shop',
            'delete_shop',
        );
    }
}

