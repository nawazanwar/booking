<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model implements Permissions
{
    use SoftDeletes, DateTime;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'slug', 'created_at'];

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function vehicle()
    {
        return $this->belongsToMany(Vehicle::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null)
    {

        if ($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_booking');
                    break;
                case 'create':
                case 'store':
                    return array('create_booking', 'publish_booking');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_booking');
                    break;
                case 'delete':
                    return array('delete_booking');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_booking',
            'create_booking',
            'publish_booking',
            'edit_booking',
            'delete_booking',
        );
    }

}
