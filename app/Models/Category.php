<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements Permissions
{
    protected $fillable = ['name', 'slug', 'order','parent_id','user_id','image'];

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    public function posts() {
        return $this->belongsToMany(Post::class);
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }
    
    public function pages() {
        return $this->belongsToMany(Page::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function terminals()
    {
        return $this->hasMany(Terminal::class);
    }

    public function cinemas()
    {
        return $this->hasMany(Cinema::class);
    }

    public function shops()
    {
        return $this->hasMany(Shop::class);
    }

    public static function modulePermissions($middleware = false, $route = null)
    {

        if($middleware) {

            switch ($route) {
                case 'manage':
                case 'show':
                    return array('view_category');
                    break;
                case 'create':
                case 'store':
                    return array('create_category');
                    break;
                case 'edit':
                case 'update':
                    return array('edit_category');
                    break;
                case 'delete':
                    return array('delete_category');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_category',
            'create_category',
            'edit_category',
            'delete_category',
        );
    }
}
