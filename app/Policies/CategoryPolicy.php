<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the category.
     *
     * @param \App\Models\User $user
     * @param \App\Category $category
     * @return mixed
     */
    public function view(User $user, Category $category)
    {
        return $user->ability('view_category');
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_category');
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param \App\Models\User $user
     * @param \App\Category $category
     * @return mixed
     */
    public function update(User $user, Category $category)
    {
        return $user->ability('edit_category');
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param \App\Models\User $user
     * @param \App\Category $category
     * @return mixed
     */
    public function delete(User $user, Category $category)
    {
        return $user->ability('delete_category');
    }
}
