<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Shop;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShopPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function view(User $user, Shop $shop)
    {
        return $user->ability('view_shop');
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_shop');
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function update(User $user, Shop $shop)
    {
        return $user->ability('edit_shop');
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function delete(User $user, Shop $shop)
    {
        return $user->ability('delete_shop');
    }
}
