<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Booking;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the category.
     *
     * @param \App\Models\User $user
     * @param \App\Booking $booking
     * @return mixed
     */
    public function view(User $user, Booking $booking)
    {
        return $user->ability('view_booking');
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_booking');
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param \App\Models\User $user
     * @param \App\Booking $booking
     * @return mixed
     */
    public function update(User $user, Booking $booking)
    {
        return $user->ability('edit_booking');
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param \App\Models\User $user
     * @param \App\Booking $booking
     * @return mixed
     */
    public function delete(User $user, Booking $booking)
    {
        return $user->ability('delete_booking');
    }
}
