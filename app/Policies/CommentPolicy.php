<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the comment.
     *
     * @param  \App\Models\User $user
     * @param  \App\Comment $comment
     * @return mixed
     */
    public function view(User $user, Comment $comment)
    {
        return $user->ability('view_comment');
    }

    /**
     * Determine whether the user can create comments.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_comment');
    }

    /**
     * Determine whether the user can update the comment.
     *
     * @param  \App\Models\User $user
     * @param  \App\Comment $comment
     * @return mixed
     */
    public function update(User $user, Comment $comment)
    {
        return $user->ability('edit_comment');
    }

    /**
     * Determine whether the user can delete the comment.
     *
     * @param  \App\Models\User $user
     * @param  \App\Comment $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        return $user->ability('delete_comment');
    }
}
