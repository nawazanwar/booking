<?php

namespace App\Policies;

use App\Models\User;
use App\NavLink;
use Illuminate\Auth\Access\HandlesAuthorization;

class NavLinkPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the navLink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\NavLink  $navLink
     * @return mixed
     */
    public function view(User $user, NavLink $navLink)
    {
        //
    }

    /**
     * Determine whether the user can create navLinks.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the navLink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\NavLink  $navLink
     * @return mixed
     */
    public function update(User $user, NavLink $navLink)
    {
        //
    }

    /**
     * Determine whether the user can delete the navLink.
     *
     * @param  \App\Models\User  $user
     * @param  \App\NavLink  $navLink
     * @return mixed
     */
    public function delete(User $user, NavLink $navLink)
    {
        //
    }
}
