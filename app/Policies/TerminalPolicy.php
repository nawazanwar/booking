<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Terminal;
use Illuminate\Auth\Access\HandlesAuthorization;

class TerminalPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function view(User $user, Terminal $terminal)
    {
        return $user->ability('view_terminal');
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_terminal');
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function update(User $user, Terminal $terminal)
    {
        return $user->ability('edit_terminal');
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function delete(User $user, Terminal $terminal)
    {
        return $user->ability('delete_terminal');
    }
}
