<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Media;
use Illuminate\Auth\Access\HandlesAuthorization;

class MediaPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the Media.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Media $media
     * @return mixed
     */
    public function view(User $user, Media $media)
    {
        return $user->ability('view_media');
    }

    /**
     * Determine whether the user can create Medias.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_media');
    }

    /**
     * Determine whether the user can update the Media.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Media $media
     * @return mixed
     */
    public function update(User $user, Media $media)
    {
        return $user->ability('edit_media');
    }

    /**
     * Determine whether the user can delete the Media.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Media $media
     * @return mixed
     */
    public function delete(User $user, Media $media)
    {
        return $user->ability('delete_media');
    }
}
