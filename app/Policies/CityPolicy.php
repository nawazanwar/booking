<?php

namespace App\Policies;

use App\Models\User;
use App\Models\City;
use Illuminate\Auth\Access\HandlesAuthorization;

class CityPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the city.
     *
     * @param  \App\Models\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function view(User $user, City $city)
    {
        return $user->ability('view_city');
    }

    /**
     * Determine whether the user can create cities.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('add_city');
    }

    /**
     * Determine whether the user can update the city.
     *
     * @param  \App\Models\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function update(User $user, City $city)
    {
        return $user->ability('edit_city');
    }

    /**
     * Determine whether the user can delete the city.
     *
     * @param  \App\Models\User  $user
     * @param  \App\City  $city
     * @return mixed
     */
    public function delete(User $user, City $city)
    {
        return $user->ability('delete_city');
    }
}
