<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Movie;
use Illuminate\Auth\Access\HandlesAuthorization;

class MoviePolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the Movie.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Movie $movie
     * @return mixed
     */
    public function view(User $user, Movie $movie)
    {
        return $user->ability('view_movie');
    }

    /**
     * Determine whether the user can create Movies.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_movie');
    }

    /**
     * Determine whether the user can update the Movie.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Movie $movie
     * @return mixed
     */
    public function update(User $user, Movie $movie)
    {
        return $user->ability('edit_movie');
    }

    /**
     * Determine whether the user can delete the Movie.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Movie $movie
     * @return mixed
     */
    public function delete(User $user, Movie $movie)
    {
        return $user->ability('delete_movie');
    }
}
