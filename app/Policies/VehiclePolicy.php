<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Auth\Access\HandlesAuthorization;

class VehiclePolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function view(User $user, Vehicle $vehicle)
    {
        return $user->ability('view_vehicle');
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_vehicle');
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function update(User $user, Vehicle $vehicle)
    {
        return $user->ability('edit_vehicle');
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Post $post
     * @return mixed
     */
    public function delete(User $user, Vehicle $vehicle)
    {
        return $user->ability('delete_vehicle');
    }
}
