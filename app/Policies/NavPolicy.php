<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Nav;
use Illuminate\Auth\Access\HandlesAuthorization;

class NavPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the nav.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Nav  $nav
     * @return mixed
     */
    public function view(User $user, Nav $nav)
    {
        return $user->ability('view_nav');
    }

    /**
     * Determine whether the user can create navs.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_nav');
    }

    /**
     * Determine whether the user can update the nav.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Nav  $nav
     * @return mixed
     */
    public function update(User $user, Nav $nav)
    {
        return $user->ability('edit_nav');
    }

    /**
     * Determine whether the user can delete the nav.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Nav  $nav
     * @return mixed
     */
    public function delete(User $user, Nav $nav)
    {
        return $user->ability('delete_nav');
    }
}
