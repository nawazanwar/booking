<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Food;
use Illuminate\Auth\Access\HandlesAuthorization;

class FoodPolicy
{
    use HandlesAuthorization;
    /**
     * Determine whether the user can view the Food.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Food $food
     * @return mixed
     */
    public function view(User $user, Food $food)
    {
        return $user->ability('view_food');
    }

    /**
     * Determine whether the user can create Foods.
     *
     * @param  \App\Models\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->ability('create_food');
    }

    /**
     * Determine whether the user can update the Food.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Food $food
     * @return mixed
     */
    public function update(User $user, Food $food)
    {
        return $user->ability('edit_food');
    }

    /**
     * Determine whether the user can delete the Food.
     *
     * @param  \App\Models\User $user
     * @param  \App\Models\Food $food
     * @return mixed
     */
    public function delete(User $user, Food $food)
    {
        return $user->ability('delete_food');
    }
}
