<?php

namespace App\Console\Commands;

use App\Models\Permission;
use Illuminate\Console\Command;

class SyncPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bilzit:sync:permissions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs exported permissions from Models into Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!\Illuminate\Support\Facades\Schema::hasTable('permissions')) {
            $this->error('Permissions Table Not Found!');
        } else {
            Permission::syncPermissions();
            $this->info('Permissions Sync Complete');
        }
    }
}
