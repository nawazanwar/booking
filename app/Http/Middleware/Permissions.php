<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
class Permissions{
    public function handle($request, Closure $next){
        $route = Route::getRoutes()->match($request);
        $actions = $route->getAction();
        $permissions = isset($actions['permissions']) ? $actions['permissions'] : array();
        if (!$permissions) {
            return $next($request);
        } else {
            $user = Auth::user();
            if($user && $user->ability($permissions)) {
                return $next($request);
            }
        }
        return redirect()->route('home')
            ->withErrors('Unauthorized Access!');

    }
}
