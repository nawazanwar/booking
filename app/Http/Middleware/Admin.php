<?php
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
class Admin{
    public function handle($request, Closure $next, $guard = null){
	    if(Auth::user()->hasRole("super_admin") || Auth::user()->hasRole("admin") || Auth::user()->hasRole("adviser") ){
			return redirect("/dashboard");
		}
        return $next($request);
    }
}
