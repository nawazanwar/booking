<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Traits\S3;
use App\Models\Answer;
use App\Models\Permission;
use App\Models\Question;
use App\Models\Role;
use App\Models\Subscriber;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends DashboardController
{
    use S3;

    public function __construct()
    {
        parent::__construct();
    }

    private $model,$title,$message,$search_url = null;
    private $data = array();
    private $posts = array();
    private $topics = array();

    public function index(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        if (isset($for)) {

            $this->data = User::whereHas($for, function ($q) use ($parent_id) {
                $q->where('id', '=', $parent_id);
            });

            if ($for == "roles") {

                $this->title = Role::find($parent_id)->name;

            } else if ($for == "permissions") {

                $this->title = Permission::find($parent_id)->name;
            }

        } else {

            $this->data = new User();
        }

        if ($field && $keyword) {

            $this->data = $this->data->where('users.' . $field, 'like', '%' . $keyword . '%');

        }

        if (!empty($filters)) {

            $this->data = $this->data->where(function ($q) use ($filters) {

                foreach ($filters as $key => $value) {

                    $q->where($key, '=', $value);

                }

            });

        }

        $this->data = $this->data->orderby('users.id', 'DESC')->paginate(10);

        /*here is the code for the breadcrumbs*/

        if (isset($for)) {

            $breadcrumbs = [
                ['text' => $this->title, 'url' => route('muntazim.' . $for . '.show', ['parent_id' => $parent_id])],
                ['text' => __('general.all_users'), 'url' => route('muntazim.' . $for, ['parent_id' => $parent_id, 'for' => $for])]
            ];

        } else {

            $breadcrumbs = [['text' => __('general.all_users')]];

        }

        /*here is the code for the view Parameters*/

        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'users' => $this->data,
            'parentTitle' => (isset($for)) ? "of " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => __('general.all_users'),
            'smallTitle' => '(' . $this->data->count() . ')',
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.users.index', $viewParams);
    }

    /**
     * Display the Profile Section.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function profile(Request $request)
    {
        $for = $request->input('for');
        $key = $request->input('key');
        $value = $request->input('value');
        $type = $request->input('type');
        $this->model = Auth::user();

        if ($for=="posts" && $value=="votes") {

            $this->data = Question::with('user', 'answers', 'topics')
                ->withCount('answers', 'topics')->join('votes', function ($join) use ($type) {

                $join->on('posts.id', '=', 'votes.post_id');

                if ($type == "up") {

                    $join->where('votes.type', 'up');

                } else if ($type == "down") {

                    $join->where('votes.type', 'down');

                }
            });

        }else if (!isset($for) || $for == "posts") {

            $this->data = Question::with('user', 'answers', 'topics')
                ->withCount('answers', 'topics')
                ->where('posts.user_id', $this->model->id)
                ->where(function ($query) use ($key, $value) {
                    if (isset($key)) {
                        $query->where($key, $value);
                    }
                });

        }else if ($for == "answers") {

            $this->data = Answer::with('posts', 'user')->withCount('posts')
                ->where('answers.user_id', $this->model->id)
                ->where(function ($query) use ($key, $value) {
                    if (isset($key)) {
                        $query->where($key, $value);
                    }
                });

        }else if ($for=="notifications"){

            $this->data =Question::join('follows','posts.id','follows.post_id')
                ->join('answer_post','answer_post.post_id','follows.post_id')
                ->join('answers','answer_post.answer_id','answers.id')
                ->where('follows.user_id',$this->model->id)
                ->select('answers.*','posts.id as post_id')
                ->orderby('answers.id','DESC');
        }

        $this->data = $this->data->paginate(10);

        $viewParams = [
            'model' => $this->model,
            'data' => $this->data,
            'for' =>$for,
            'key' => $key,
            'value' => $value,
            'pageTitle' => __('general.profile'),
        ];
        return view('profile', $viewParams);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->model = User::find($id);
        $pageTitle = !empty($this->model) ? $this->model->name : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_users'), 'url' => route('muntazim.users')],
            ['text' => $this->model->name],
            ['text' => __('general.detail')],
        ];
        $viewParams = [
            'model' => $this->model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.users.show', $viewParams);
    }


    public function assign(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        $this->model = User::whereDoesntHave($for, function ($query) use ($parent_id) {

            $query->where('id', '=', $parent_id);

        })->pluck('email', 'id')->all();

        if ($for == "roles") {

            $this->title = Role::find($parent_id)->name;

        } else if ($for == "permissions") {

            $this->title = Permission::find($parent_id)->name;

        }

        $pageTitle = __('general.assign_user');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => $this->title, 'url' => route('muntazim.' . $for . '.show', ['parent_id' => $parent_id, 'for' => $for])],
            ['text' => __('general.assign_user')],
        ];
        $viewParams = [
            'model' => $this->model,
            'parentTitle' => (isset($for)) ? "to " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.users.assign', $viewParams);
    }

    public function edit($id)
    {
        $loggedInUserRole = Auth::user()->roleName();

        if ($loggedInUserRole == "super_admin") {

            $roles = Role::get();

        } else if ($loggedInUserRole == "admin") {

            $roles = Role::get();

        } else if ($loggedInUserRole == "adviser") {

            $roles = Role::whereIn('name', ['adviser'])->get();

        } else {

            $roles = Role::whereIn('name', ['user'])->get();

        }

        $user = User::find($id);
        $pageTitle = !empty($user) ? 'Edit ' . $user->username : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_users'), 'url' => route('muntazim.users')],
            ['text' => __('general.edit_user')],
        ];
        $viewParams = [
            'user' => $user,
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.users.edit', $viewParams);
    }

    public function create()
    {
        $loggedInUserRole = Auth::user()->roleName();
        if ($loggedInUserRole == "admin") {
            $roles = Role::whereIn('name', ['adviser', 'user'])->get();
        } else if ($loggedInUserRole == "super_admin") {
            $roles = Role::whereIn('name', ['admin', 'adviser', 'user'])->get();
        } else if ($loggedInUserRole == "adviser") {
            $roles = Role::whereIn('name', ['adviser'])->get();
        } else {
            $roles = Role::whereIn('name', ['user'])->get();
        }

        $pageTitle = 'Create User';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_users'), 'url' => route('muntazim.users')],
            ['text' => 'Create User']
        ];
        $viewParams = [
            'roles' => $roles,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.users.create', $viewParams);
    }

    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|max:512',
            'username' => 'required|max:191|unique:users,username',
            'email' => 'required|max:191|unique:users,email',
            'password' => 'required|between:6,20',
            'bio' => 'nullable|max:1021',
            'phone' => 'nullable|max:191',
            'gender' => 'nullable|in:male,female,unspecified',
            'country' => 'nullable|integer',
            'state' => 'nullable|integer',
            'active' => 'nullable',
            'avatar' => 'mimes:jpeg,jpg,png,gif|nullable|max:5000',
            'roles' => 'required|array',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $user = new User();

            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->bio = $request->input('bio', null);
            $user->phone = $request->input('phone', null);
            $user->gender = $request->input('gender', 'unspecified');
            $user->country = $request->input('country', null);
            $user->state = $request->input('state', null);
            $user->active = $request->input('active', 0);
            $user->password = Hash::make($request->input('password'));

            $roles = $request->input('roles', array());

            if ($user->save()) {

                $user->roles()->sync($roles);

                if ($request->hasFile("avatar")) {
                    $filename = $this->upload($request->file('avatar'), config('law.s3.avatar_dir'));
                    $user->avatar = $filename;
                    $user->save();
                }

                Session::flash('successMessage', 'User has been created!');
                return redirect()->route('muntazim.users');

            } else {

                return redirect()->back()
                    ->withErrors('Failed to create the user!')
                    ->withInput();
            }
        }

    }

    public function update(Request $request, $id)
    {

        $for = $request->input('for');

        if (isset($for)) {

            if ($for == "roles") {

                $this->model = Role::find($id);

            } else if ($for == "permissions") {

                $this->model = Permission::find($id);

            }

            $users = $request->input('users', array());

            foreach ($users as $user_id) {

                $user = User::find($user_id);
                $this->model->users()->save($user);
            }

            Session::flash('successMessage', 'User has been assigned Successfully assigned to ' . $this->model->name);
            return redirect()->route('muntazim.users', ['parent_id' => $id, 'for' => $for]);

        } else {

            $rules = [
                'name' => 'required|max:512',
                'username' => 'required|max:191|unique:users,username,' . $id,
                'email' => 'required|max:191|unique:users,email,' . $id,
                'password' => 'nullable|between:6,20',
                'bio' => 'nullable|max:1021',
                'phone' => 'nullable|max:191',
                'gender' => 'nullable|in:male,female,unspecified',
                'country' => 'nullable|integer',
                'state' => 'nullable|integer',
                'active' => 'required',
                'roles' => 'required|array',
            ];

            if (!$request->input('delete_avatar')) {
                $rules['avatar'] = 'mimes:jpeg,jpg,png,gif|nullable|max:5000';
            }

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            } else {

                $user = User::find($id);

                $user->name = $request->input('name');
                $user->username = $request->input('username');
                $user->email = $request->input('email');
                $user->bio = $request->input('bio', null);
                $user->phone = $request->input('phone', null);
                $user->gender = $request->input('gender', 'unspecified');
                $user->country = $request->input('country', null);
                $user->state = $request->input('state', null);
                $user->active = $request->input('active', 0);

                $roles = $request->input('roles', array());
                $password = $request->input('password');

                // change password
                if (!empty($password)) {
                    $user->password = Hash::make($password);
                }

                if ($request->input('delete_avatar')) {
                    $this->delete($user->avatar, config('law.s3.avatar_dir'));
                    $user->avatar = null;
                }

                if ($user->save()) {

                    $user->roles()->sync($roles);

                    if ($request->hasFile("avatar")) {
                        $oldImageName = $user->avatar;
                        $filename = $this->upload($request->file('avatar'), config('law.s3.avatar_dir'));
                        $user->avatar = $filename;
                        $user->save();
                        $this->delete($oldImageName, config('law.s3.avatar_dir'));
                    }


                    Session::flash('successMessage', 'User has been updated!');
                    return redirect()->route('muntazim.users');

                } else {
                    return redirect()->back()
                        ->withErrors('Failed to update the user!')
                        ->withInput();
                }
            }

        }
    }

    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        if (isset($for)) {

            /*delete relationship*/

            if ($for == "roles") {

                $this->model = Role::find($parent_id);
                $this->model->users()->detach($id);
                $this->message = $user->name . " has been deleted Successfully from " . $this->model->label;

            }
            /*ready to redirect back*/
            Session::flash('successMessage', $this->message);
            return redirect()->route('muntazim.users', [$parent_id, 'for' => $for]);

        } else {

            /*delete the single row*/

            if ($user->delete()) {

                $this->message = $user->name . " has been deleted Successfully";
                Session::flash('successMessage', $this->message);
                return redirect()->route('muntazim.users');

            }

        }

    }

}
