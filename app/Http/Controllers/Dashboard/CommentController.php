<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Answer;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends DashboardController
{
    private $view = null;
    private $model = array();

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $type = $request->input('type');
        $value = $request->input('value');
        $user = Auth::user();
        $comment = new Comment();
        $comment->content = $value;
        if ($type == "parent") {
            $comment->parent = false;
        }
        $comment->user_id = $user->id;
        $comment->answer_id = $id;
        if ($comment->save()) {

            $this->view = view('comments.parent', ['comment' => $comment,'parent_id'=>$id])->render();
            return response()->json(['status' => 'success', 'view' => $this->view, 'id' => $id, 'type' => $type, 'ajax_for' => 'comments']);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $this->model = Comment::find($id);
        $this->model->delete();
        if ($request->ajax()) {

            if (Auth::user()->ability('delete_comment')) {

                return response()->json(['ajax_for' => 'delete',
                    'is_deleted' => 'comment',
                    'status' => 'success',
                    'message' => 'Comment Deleted Successfully',
                    'deleted_id' => 'current_comment_row_' . $id,
                    'parent_id'=>$request->input('parent_id')
                ]);

            } else {

                return response()->json(['status' => 'error', 'message' => 'No Permission for delete the Comment']);

            }
        } else {

        }
    }
}
