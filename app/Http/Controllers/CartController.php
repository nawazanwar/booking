<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Food;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{

    public function index()
    {
        $pageTitle = 'Detail of cart';
        $smallTitle = $pageTitle;
        $breadcrumbs = [['text' => $pageTitle]];
        $countries = Country::orderBy('name', 'ASC')->get()->pluck('name', 'id');
        $cities = City::orderBy('name', 'ASC')->get()->pluck('name', 'id');
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'countries' => $countries,
            'cities' => $cities
        ];

        return view('cart.index', $viewParams);

    }

    public function checkout()
    {

        $pageTitle = 'Checkout of cart';
        $smallTitle = $pageTitle;
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('cart.checkout', $viewParams);

    }

    public function addToCart($id)
    {
        $modal = Food::with('media')->find($id);
        $front_image = isset($modal->media[0]->src) ? $modal->media[0]->src : 'img/no_image.png';
        $back_image = isset($modal->media[1]->src) ? $modal->media[1]->src : 'img/no_image.png';
        $cart = session()->get('cart');
        // if cart is empty then this the first product
        if (!$cart) {
            $cart = [
                $id => [
                    "order_id" => $modal->id,
                    "name" => $modal->name,
                    "quantity" => 1,
                    "price" => ($modal->price != "" ? $modal->price : 0),
                    "front_image" => $front_image,
                    "back_image" => $back_image
                ]
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Food added to cart successfully!');
        }
        // if cart not empty then check if this product exist then increment quantity
        if (isset($cart[$id])) {
            $cart[$id]['quantity']++;
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Food added to cart successfully!');
        }
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $modal->name,
            "order_id" => $modal->id,
            "quantity" => 1,
            "price" => $modal->price,
            "front_image" => $front_image,
            "back_image" => $back_image
        ];
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Food added to cart successfully!');
    }

    public function update(Request $request)
    {
        if ($request->has('id') and $request->has('quantity')) {
            $id = $request->input('id');
            $quantity = $request->input('quantity');
            $cart = session()->get('cart');
            $cart[$id]["quantity"] = $quantity;
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->has('id')) {
            $id = $request->input('id');
            $cart = session()->get('cart');
            if (isset($cart[$id])) {
                unset($cart[$id]);
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Food removed successfully from cart');
        }
    }

    public function proceeded(Request $request)
    {
        $rules = [
            'country' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'bio' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $user = User::find(Auth::user()->id);
            $user->city_id = $request->input('city');
            $user->country_id = $request->input('country');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->bio = $request->input('bio');
            $cart = session()->get('cart');
            if ($user->save()) {
                foreach ($cart as $c) {
                    $order = new Order();
                    $order->user()->associate($user);
                    $order->order_id = $c['order_id'];
                    $order->quantities = $c['quantity'];
                    $order->save();
                    if (isset($cart[$c['order_id']])) {
                        unset($cart[$c['order_id']]);
                        session()->put('cart', $cart);
                    }
                }
                return redirect()->back()->with('success', 'Your order has been placed successfully');
            }
        }
    }
}
