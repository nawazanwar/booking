<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Cinema;
use App\Models\Movie;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MovieController extends MuntazimController
{

    public function index(Request $request, $pid = null)
    {

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        $data = Movie::with('cinema')->whereHas('post', function ($query) {
            $query->where('posts.type', 'movie');
        });
        if ($pid != null) {
            $data = $data->whereCinemaId($pid);
        }
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);
        $pageTitle = "All Movies";
        $smallTitle = $data->count();
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => __('faqs.all_posts'), 'url' => route('muntazim.posts')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'data' => $data,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.movie.index', $viewParams);
    }

    public function edit(Request $request, $id)
    {
        $modal = Movie::with('cinema')->find($id);
        $pageTitle ='Edit Movie';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.movie.edit', $viewParams);
    }

    public function seats(Request $request, $id)
    {
        $modal = Movie::with('cinema')->find($id);
        $pageTitle ='Movie Seats';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.movie.seats', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'date' => 'required',
            'start' => 'required',
            'end' => 'required',
            'duration' => 'required',
            'break' => 'required',
            'ticket' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $modal = Movie::find($id);

            $modal->name = $request->input('name');
            $modal->date = $request->input('date');
            $modal->start = $request->input('start');
            $modal->end = $request->input('end');
            $modal->duration = $request->input('duration');
            $modal->break = $request->input('break');
            $modal->type = $request->input('type');
            $modal->ticket = $request->input('ticket');

            if ($modal->save()) {

                Session::flash('success', 'Movie has been updated');
                return redirect()->back();

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.city_update_failed'))
                    ->withInput();
            }

        }
    }

    public function media(Request $request, $id)
    {
        $modal = Movie::with('cinema', 'media')->find($id);
        $pageTitle = "Edit Media";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'type' => 'movie',
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.media.edit', $viewParams);
    }

}
