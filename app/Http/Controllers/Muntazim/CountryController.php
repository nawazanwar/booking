<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CountryController extends MuntazimController
{

    public function index(Request $request)
    {
        // TODO: Make filters work
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        $countries = Country::orderBy('name', 'ASC');

        if ($keyword && $field) {
            $countries->where($field, $keyword);
        }

        $countries = $countries->paginate(15);

        $pageTitle = __('faqs.all_countries');
        $smallTitle = '(' . $countries->count() . ')';
        $breadcrumbs = [['text' => __('faqs.all_countries')]];
        $viewParams = [
            'countries' => $countries,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,

            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.countries.index', $viewParams);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = __('faqs.create_country');
        $smallTitle = '';

        $breadcrumbs = [
            ['text' => __('faqs.all_countries'), 'url' => route('muntazim.countries')],
            ['text' => __('faqs.create_country')],
        ];

        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.countries.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'code' => 'max:3|alpha|unique:countries,code',
            'name' => 'max:100|alpha_spaces|unique:countries,name',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $country = new Country();
            $country->code = $request->input('code');
            $country->name = $request->input('name');

            if ($country->save()) {

                Session::flash('successMessage', __('faqs.create_country_success'));
                return redirect()->route('muntazim.countries');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.create_country_failed'))
                    ->withInput();
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        $pageTitle = !empty($country) ? __('faqs.edit') . $country->name : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('faqs.all_countries'), 'url' => route('muntazim.countries')],
            ['text' => __('faqs.edit_country')],
        ];

        $viewParams = [
            'country' => $country,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.countries.edit', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'code' => 'max:3|alpha|unique:countries,code,' . $id,
            'name' => 'max:100|alpha_spaces|unique:countries,name,' . $id,
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $country = Country::find($id);;
            $country->code = $request->input('code');
            $country->name = $request->input('name');

            if ($country->save()) {

                Session::flash('successMessage', __('faqs.country_update_success'));
                return redirect()->route('muntazim.countries');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.country_update_failed'))
                    ->withInput();
            }

        }
    }

    public function destroy($id)
    {
        $country = Country::find($id);

        if ($country->delete()) {

            Session::flash('successMessage', __('faqs.country_delete_success'));
            return redirect()->route('muntazim.countries');

        }
    }

}
