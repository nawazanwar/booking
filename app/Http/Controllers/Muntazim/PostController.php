<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Category;
use App\Models\Cinema;
use App\Models\Food;
use App\Models\Movie;
use App\Models\Post;
use App\Models\Shop;
use App\Models\Terminal;
use App\Models\Vehicle;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PostController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageTitle = "All posts";
        $smallTitle = "All posts";
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.post.index', $viewParams);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return redirect()->route('muntazim.posts');

        $post = Post::find($id);
        Event::fire('posts.view', $post);
        $pageTitle = 'Post Details';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Post Details']];
        $viewParams = [
            'post' => $post,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.post.show', $viewParams);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        $this->authorize('update', $post);

        $categories = Category::all()->pluck('name', 'id');

        $pageTitle = !empty($post) ? 'Edit ' . $post->title : '';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Edit Post']];
        $viewParams = [
            'post' => $post,
            'categories' => $categories,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.post.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request, $type)
    {
        if ($type == 'cinema') {
            $data = Cinema::with('city')->whereHas('category', function ($query) use ($type) {
                $query->where('categories.type', $type);
            })->get();
            $pageTitle = 'Create Movie';
        } else if ($type == 'terminal') {
            $data = Terminal::with('city')->whereHas('category', function ($query) use ($type) {
                $query->where('categories.type', $type);
            })->get();
            $pageTitle = 'Create Vehicle';
        } else if ($type == 'shop') {
            $data = Shop::with('city')->whereHas('category', function ($query) use ($type) {
                $query->where('categories.type', $type);
            })->get();
            $pageTitle = 'Create Food';
        }
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('faqs.all_posts'), 'url' => route('muntazim.posts')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'data' => $data,
            'type' => $type
        ];
        return view('muntazim.post.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $type)
    {
        $rules = [
            'name' => 'required|max:255',
            'slug' => 'nullable|max:255|alpha_dash|unique:posts,slug'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $post = new Post();

            if ($type == 'terminal') {
                $post->type = 'vehicle';
            } else if ($type == 'shop') {
                $post->type = 'food';
            } else if ($type == 'cinema') {
                $post->type = 'movie';
            }
            $post->name = $request->input('name');
            $post->slug = $request->input('slug');
            $post->user()->associate(Auth::user());

            if ($post->save()) {

                //code for saving the image
                $this->makeDirectory('posts', ['default', 'front', 'back', 'slides']);
                if ($request->file('image')) {
                    $image = $request->file('image');
                    $image_name = sha1($post->id . "default") . '.' . $image->getClientOriginalExtension();
                    $image_path = public_path('uploads/posts/default') . '/' . $image_name;
                    $this->saveMedia($image, $image_path);
                    $post->image = 'uploads/posts/default/' . $image_name;
                    $post->save();
                }

                /*save the categories of the post*/
                $categories = $request->input('categories');
                foreach ($categories as $cid) {
                    /*manage the terminal*/
                    if ($type == 'terminal') {

                        $terminal = Terminal::find($cid);
                        $post->categories()->save($terminal);
                        $post->vehicles()->save(new Vehicle([
                            'terminal_id' => $terminal->id,
                            'post_id' => $post->id,
                            'name' => $post->name . " in " . $terminal->name
                        ]));

                    } else if ($type == 'cinema') {
                        $cinema = Cinema::find($cid);
                        $post->categories()->save($cinema);
                        $post->movies()->save(new Movie([
                            'cinema_id' => $cinema->id,
                            'post_id' => $post->id,
                            'name' => $post->name . " in " . $cinema->name
                        ]));

                    } else if ($type == 'shop') {
                        $shop = Shop::find($cid);
                        $post->categories()->save($shop);
                        $post->foods()->save(new Food([
                            'shop_id' => $shop->id,
                            'post_id' => $post->id,
                            'name' => $post->name . " in " . $shop->name
                        ]));
                    }
                }

                if ($type == 'terminal') {
                    Session::flash('success', 'New Vehicle has been create successfully');
                    return redirect()->route('muntazim.vehicles', [$terminal->id]);
                } else if ($type == 'cinema') {
                    Session::flash('success', 'New Movies has been create successfully');
                    return redirect()->route('muntazim.movies', [$cinema->id]);
                } else if ($type == 'shop') {
                    Session::flash('success', 'New Foods has been create successfully');
                    return redirect()->route('muntazim.foods', [$shop->id]);
                }

            } else {

                return redirect()->back()
                    ->withErrors('Failed to create post!')
                    ->withInput();
            }
        }
    }

    function makeDirectory($parent, $childs)
    {

        foreach ($childs as $child) {
            $path = public_path() . '/uploads/' . $parent . '/' . $child . '/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        }
    }

    function saveMedia($file, $path)
    {

        $saved_file = Image::make($file);
        $saved_file->resize(960, 640);
        $saved_file->save($path);

    }

    function deleteExistingFile($file)
    {
        $path = public_path() . "/" . $file;
        if (File::exists($path)) {
            File::delete($path);
        }

    }

    /**
     * Remove the specified Sawti.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request, $id)
    {
        $model = Post::find($id);
        foreach ($model->media()->get() as $media) {
            $this->deleteExistingFile($media->src);
        }

        if ($model->delete()) {

            Session::flash('danger', __('faqs.post_delete_success'));
            return redirect()->route('muntazim.posts', [$request->input('parent_id')]);

        }

    }
}
