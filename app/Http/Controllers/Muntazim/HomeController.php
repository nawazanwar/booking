<?php
namespace App\Http\Controllers\Muntazim;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
class HomeController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index(Request $request){
        $pageTitle = 'Dashboard';
        $viewParams = [
            'pageTitle' => $pageTitle,
        ];
		return view('muntazim.index', ['pageTitle' => __('general.dashboard_title')]);
    }
}
