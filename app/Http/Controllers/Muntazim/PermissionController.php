<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PermissionController extends MuntazimController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $model = null;
    private $title = null;
    private $parent_title = null;
    private $parent_url = null;
    private $message = null;
    private $url = null;
    private $data = array();

    public function index(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        if (isset($for)) {

            $this->data = Permission::whereHas($for, function ($q) use ($parent_id) {
                $q->where('id', '=', $parent_id);
            });

            if ($for == "users") {

                $this->title = User::find($parent_id)->name;

            } else if ($for == "roles") {

                $this->title = Role::find($parent_id)->label;
            }

        } else {

            $this->data = new Permission();
        }

        if ($field && $keyword) {

            $this->data = $this->data->where('permissions.' . $field, 'like', '%' . $keyword . '%');

        }

        if (!empty($filters)) {

            $this->data = $this->data->where(function ($q) use ($filters) {

                foreach ($filters as $key => $value) {

                    $q->where($key, '=', $value);

                }

            });

        }

        $this->data = $this->data->orderby('permissions.id', 'DESC')->paginate(10);

        /*here is the code for the breadcrumbs*/

        if (isset($for)) {

            $breadcrumbs = [
                ['text' => $this->title, 'url' => route('muntazim.' . $for . '.show', ['parent_id' => $parent_id])],
                ['text' => __('general.all_permissions'), 'url' => route('muntazim.' . $for, ['parent_id' => $parent_id, 'for' => $for])]
            ];

        } else {

            $breadcrumbs = [['text' => __('general.all_permissions')]];

        }

        /*here is the code for the view Parameters*/

        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'permissions' => $this->data,
            'parentTitle' => (isset($for)) ? "of " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => __('general.all_permissions'),
            'smallTitle' => '(' . $this->data->count() . ')',
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.permissions.index', $viewParams);
    }

    public function assign(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        $this->model = Permission::whereDoesntHave($for, function ($query) use ($parent_id) {

            $query->where('id', '=', $parent_id);

        })->pluck('label', 'id')->all();

        if ($for == "roles") {

            $this->title = Role::find($parent_id)->label;

        } else if ($for == "users") {

            $this->title = User::find($parent_id)->name;

        }

        $pageTitle = __('general.assign_permission');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => $this->title, 'url' => route('muntazim.'.$for.'.show', ['parent_id' => $parent_id, 'for' => $for])],
            ['text' => __('general.assign_permission')],
        ];
        $viewParams = [
            'model' => $this->model,
            'parentTitle' => (isset($for)) ? "to " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.permissions.assign', $viewParams);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->model = Permission::find($id);
        $pageTitle = !empty($this->model) ? $this->model->label : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_permissions'), 'url' => route('muntazim.permissions')],
            ['text' => $this->model->name],
            ['text' => __('general.detail')],
        ];
        $viewParams = [
            'model' => $this->model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.permissions.show', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $for = $request->input('for');

        $parent_id = $request->input('parent_id');

        if ($for == "roles") {

            $this->model = Role::find($parent_id);

        } else if ($for == "users") {

            $this->model = User::find($parent_id);

        }

        /*ready to save the update permissions*/

        $permissions = $request->input('permissions', array());

        foreach ($permissions as $permission_id) {

            $permission = Permission::find($permission_id);
            $this->model->permissions()->save($permission);
        }

        Session::flash('successMessage', 'Permission has been assigned Successfully');

        return redirect()->route('muntazim.permissions', ['parent_id' => $parent_id, 'for' => $for]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $permission = Permission::find($id);

        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        if ($for == "roles") {

            $this->model = Role::find($parent_id);
            $this->model->permissions()->detach($id);
            $this->message = $permission->label . " has been deleted Successfully from " . $this->model->label;

        } else if ($for == "users") {

            $this->model = User::find($parent_id);
            $this->model->permissions()->detach($id);
            $this->message = $permission->label . " has been deleted Successfully from " . $this->model->name;

        }

        /*ready to redirect back*/
        Session::flash('successMessage', $this->message);
        return redirect()->route('muntazim.permissions', ['parent_id' => $parent_id, 'for' => $for]);
    }

    /**
     * Read permissions from all models and sync with db
     *
     * @return \Illuminate\Http\Response
     */
    public function syncPermissions()
    {

        if (Permission::syncPermissions()) {

            Session::flash('successMessage', 'Permissions synced!');

            return redirect()->back();

        } else {

            return redirect()->back()
                ->withErrors('Failed to sync permissions!');

        }

    }
}
