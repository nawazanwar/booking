<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Category;
use App\Models\Cinema;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CinemaController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $pid = null)
    {
        $parent = Category::find($pid);

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        $data = Cinema::with('city')->whereHas('category', function ($query) use ($pid) {
            $query->where('categories.parent_id', $pid)->with('cities');
        });
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);

        $pageTitle = "All Cinemas";
        $smallTitle = $data->count();
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'data' => $data,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'parent' => $parent,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.cinema.index', $viewParams);
    }

    public function edit(Request $request, $id)
    {
        $modal = Cinema::with('category')->find($id);
        $pageTitle = "Edit Cinema";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.cinema.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $modal = Cinema::find($id);
        $modal->address = $request->input('address');
        $modal->name = $request->input('name');
        $modal->front = $request->input('front');
        $modal->back = $request->input('back');
        $modal->gallery = $request->input('gallery');
        $modal->active = $request->input('active');
        $modal->public = $request->input('public');
        $modal->featured = $request->input('featured');
        if ($modal->save()) {
            Session::flash('success', 'Cinema has been updated');
            return redirect()->back();
        }
    }

    public function media(Request $request, $id)
    {
        $modal = Cinema::with('category', 'media')->find($id);
        $pageTitle = "Edit Media";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'type' => 'cinema',
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.media.edit', $viewParams);
    }
}
