<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Cinema;
use App\Models\Food;
use App\Models\Media;
use App\Models\Movie;
use App\Models\Shop;
use App\Models\Terminal;
use App\Models\Vehicle;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class MediaController extends MuntazimController
{

    public function update(Request $request, $id)
    {
        $type = $request->input('type');
        if ($type == 'movie') {
            $modal = Movie::find($id);
        } else if ($type == 'vehicle') {
            $modal = Vehicle::find($id);
        } else if ($type == 'food') {
            $modal = Food::find($id);
        } else if ($type == 'cinema') {
            $modal = Cinema::find($id);
        } else if ($type == 'terminal') {
            $modal = Terminal::find($id);
        } else if ($type == 'shop') {
            $modal = Shop::find($id);
        }
        $this->makeDirectory($type, ['front', 'back', 'slides']);
        //handle the front nad back images
        if (isset($request->file('image')['front'])) {
            if (isset($modal->media[0]->src)) {
                $this->deleteExistingFile($modal->media[0]->src);
            }
            $front_image = $request->file('image')['front'];
            $front_image_name = sha1($modal->id . "front") . '.' . $front_image->getClientOriginalExtension();
            $front_image_path = public_path('uploads/' . $type . '/front') . '/' . $front_image_name;
            $this->saveMedia($front_image, $front_image_path);
            $modal->media()->save(new Media([
                'name' => 'front',
                'src' => 'uploads/' . $type . '/front/' . $front_image_name
            ]));
        }
        if (isset($request->file('image')['back'])) {
            if (isset($modal->media[0]->src)) {
                $this->deleteExistingFile($modal->media[0]->src);
            }
            $back_image = $request->file('image')['back'];
            $back_image_name = sha1($modal->id . "back") . '.' . $back_image->getClientOriginalExtension();
            $back_image_path = public_path('uploads/' . $type . '/back') . '/' . $back_image_name;
            $this->saveMedia($back_image, $back_image_path);
            $modal->media()->save(new Media([
                'name' => 'back',
                'src' => 'uploads/' . $type . '/back/' . $back_image_name
            ]));
        }

        if ($type == 'movie') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.movies.media', [$modal->id, 'src' => 'lightbox']);
        } else if ($type == 'vehicle') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.vehicles.media', [$modal->id, 'src' => 'lightbox']);
        } else if ($type == 'food') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.foods.media', [$modal->id, 'src' => 'lightbox']);
        } else if ($type == 'cinema') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.cinemas.media', [$modal->id, 'src' => 'lightbox']);
        } else if ($type == 'terminal') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.terminals.media', [$modal->id, 'src' => 'lightbox']);
        } else if ($type == 'shop') {
            Session::flash('success', 'Media has been updated');
            return redirect()->route('muntazim.shops.media', [$modal->id, 'src' => 'lightbox']);
        }
    }

    function makeDirectory($parent, $childs)
    {

        foreach ($childs as $child) {
            $path = public_path() . '/uploads/' . $parent . '/' . $child . '/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        }
    }

    function saveMedia($file, $path)
    {

        $saved_file = Image::make($file);
        $saved_file->resize(960, 640);
        $saved_file->save($path);

    }

    function deleteExistingFile($file)
    {
        $path = public_path() . "/" . $file;
        if (File::exists($path)) {
            File::delete($path);
        }

    }
}
