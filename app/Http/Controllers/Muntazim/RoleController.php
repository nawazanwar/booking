<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\Auth;

class RoleController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    private $model = null;
    private $message = null;
    private $title = null;
    private $data = array();

    public function index(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        if (isset($for)) {

            $this->data = Role::whereHas($for, function ($q) use ($parent_id) {
                $q->where('id', '=', $parent_id);
            });

            if ($for == "users") {

                $this->title = User::find($parent_id)->name;

            } else if ($for == "permissions") {

                $this->title = Permission::find($parent_id)->name;
            }

        } else {

            $this->data = new Role();
        }

        if ($field && $keyword) {

            $this->data = $this->data->where('roles.' . $field, 'like', '%' . $keyword . '%');

        }

        if (!empty($filters)) {

            $this->data = $this->data->where(function ($q) use ($filters) {

                foreach ($filters as $key => $value) {

                    $q->where($key, '=', $value);

                }

            });

        }

        $this->data = $this->data->orderby('roles.id', 'DESC')->paginate(10);

        /*here is the code for the breadcrumbs*/

        if (isset($for)) {

            $breadcrumbs = [
                ['text' => $this->title, 'url' => route('muntazim.' . $for . '.show', ['parent_id' => $parent_id])],
                ['text' => __('general.all_roles'), 'url' => route('muntazim.' . $for, ['parent_id' => $parent_id, 'for' => $for])]
            ];

        } else {

            $breadcrumbs = [['text' => __('general.all_roles')]];

        }

        /*here is the code for the view Parameters*/

        $viewParams = [
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'roles' => $this->data,
            'parentTitle' => (isset($for)) ? "of " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => __('general.all_roles'),
            'smallTitle' => '(' . $this->data->count() . ')',
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.roles.index', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::pluck('label', 'id')->all();
        $pageTitle = 'Create Role';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Create Role']];
        $viewParams = [
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.roles.create', $viewParams);

    }
    public function assign(Request $request)
    {
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        $this->model = Role::whereDoesntHave($for, function ($query) use ($parent_id) {

            $query->where('id', '=', $parent_id);

        })->pluck('label', 'id')->all();

        if ($for == "users") {

            $this->title = User::find($parent_id)->name;

        } else if ($for == "permissions") {

            $this->title = Permission::find($parent_id)->name;

        }

        $pageTitle = __('general.assign_role');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => $this->title, 'url' => route('muntazim.'.$for.'.show', ['parent_id' => $parent_id, 'for' => $for])],
            ['text' => __('general.assign_role')],
        ];
        $viewParams = [
            'model' => $this->model,
            'parentTitle' => (isset($for)) ? "to " . $this->title : '',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'for' => $for,
            'parent_id' => $parent_id
        ];

        return view('muntazim.roles.assign', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|max:255|unique:roles,name',
            'label' => 'required|max:255',
            'active' => 'nullable',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $role = new Role;

            $name = $request->input('name');
            $label = $request->input('label');
            $permissions = $request->input('permissions', array());
            $role->active = $request->input('active', 0);
            $role->name = str_replace(' ', '-', strtolower($name));
            $role->label = $label;

            if ($role->save()) {

                foreach ($permissions as $permission_id) {

                    $permission = Permission::find($permission_id);

                    $role->permissions()->save($permission);

                }

                Session::flash('successMessage', 'Role has been created!');

                return redirect()->route('muntazim.roles');

            } else {

                return redirect()->back()
                    ->withErrors('Failed to create role!')
                    ->withInput();
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->model = Role::find($id);
        $pageTitle = !empty($this->model) ? $this->model->label : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_roles'), 'url' => route('muntazim.roles')],
            ['text' => $this->model->name],
            ['text' => __('general.detail')],
        ];
        $viewParams = [
            'model' => $this->model,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.roles.show', $viewParams);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all()->pluck('label', 'id');
        $pageTitle = !empty($role) ? 'Edit ' . $role->label : '';
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('general.all_roles'), 'url' => route('muntazim.roles')],
            ['text' => __('general.edit_role')],
        ];
        $viewParams = [
            'role' => $role,
            'permissions' => $permissions,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.roles.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $for = $request->input('for');

        if (isset($for)) {

            if ($for=="users"){

                $this->model = User::find($id);

            }else if ($for=="permissions") {

                $this->model = Permission::find($id);

            }

            $roles = $request->input('roles', array());

            foreach ($roles as $role_id) {

                $role = Role::find($role_id);
                $this->model->roles()->save($role);
            }

            Session::flash('successMessage', 'Roles has been assigned Successfully assigned to ' . $this->model->name);
            return redirect()->route('muntazim.roles', ['parent_id'=>$id, 'for' => $for]);

        } else {

            $permissions = $request->input('permissions', array());
            $role = Role::find($id);
            $role->active = $request->input('active', 0);


            if ($role->save()) {

                if (count($permissions) > 0) {

                    $role->permissions()->sync($permissions);
                }

                Session::flash('successMessage', 'Role has been updated!');
                return redirect()->route('muntazim.roles');

            } else {

                return redirect()->back()
                    ->withErrors('Failed to update the role!')
                    ->withInput();
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $role = Role::find($id);
        $for = $request->input('for');
        $parent_id = $request->input('parent_id');

        if (isset($for)) {

            /*delete relationship*/

            if ($for == "users") {

                $this->model = User::find($parent_id);
                $this->model->roles()->detach($id);
                $this->message = $role->name . " has been deleted Successfully from " . $this->model->name;

            }
            /*ready to redirect back*/
            Session::flash('successMessage', $this->message);
            return redirect()->route('muntazim.roles', [$parent_id, 'for' => $for]);

        } else {

            /*delete the single row*/

            if ($role->delete()) {

                $this->message = $role->label . " has been deleted Successfully";
                Session::flash('successMessage', $this->message);
                return redirect()->route('muntazim.roles');

            }

        }
    }

    /**
     * Read permissions from all models and sync with db
     *
     * @return \Illuminate\Http\Response
     */
    public function syncPermissions()
    {

        if (Permission::syncPermissions()) {

            Session::flash('successMessage', 'Permissions synced!');

            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors('Failed to sync permissions!');
        }

    }
}
