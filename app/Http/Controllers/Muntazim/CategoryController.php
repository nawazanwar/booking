<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Category;
use App\Models\Cinema;
use App\Models\City;
use App\Models\Shop;
use App\Models\Terminal;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CategoryController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $categories = Category::whereParentId(0)->get();

        $pageTitle = 'All Categories';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'All Categories']];
        $viewParams = [
            'categories' => $categories,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.category.index', $viewParams);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $category = Category::find($id);

        $pageTitle = 'Category Details';
        $smallTitle = '';
        $breadcrumbs = [['text' => 'Category Details']];
        $viewParams = [
            'category' => $category,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'parent_id' => $request->input('parent_id')
        ];

        return view('muntazim.category.show', $viewParams);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $parent_id = $request->input('parent_id');
        $category = Category::find($id);

        $pageTitle = !empty($category) ? __('faqs.edit') . $category->name : __('faqs.edit_category');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories', [$parent_id])],
            ['text' => __('faqs.edit_category')],
        ];
        $viewParams = [
            'category' => $category,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'parent_id' => $parent_id
        ];

        return view('muntazim.category.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pid)
    {

        $parent = Category::find($pid);
        $cities = City::where('country_id', 165)->pluck('name', 'id');
        $pageTitle = !empty($category) ? __('faqs.create') . $category->name : __('faqs.create_category');
        $smallTitle = '';
        if ($parent->slug == 'cinemas') {
            $bread_text = 'Create Cinema';
        } else if ($parent->slug == 'terminals') {
            $bread_text = 'Create Terminal';
        } else if ($parent->slug == 'food-points') {
            $bread_text = 'Create Food points';
        }
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => $parent->name, 'url' => route('muntazim.categories', [$pid])],
            ['text' => $bread_text],
        ];
        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'parent' => $parent,
            'cities' => $cities,
        ];
        return view('muntazim.category.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $pid)
    {
        $parent = Category::find($pid);
        $rules = [
            'name' => 'required|max:255',
            'slug' => 'nullable|max:255|alpha_dash|unique:posts,slug'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $category = new Category();

            $category->parent_id = $parent->id;
            $category->type = $request->input('type');
            $category->name = $request->input('name');
            $category->slug = $request->input('slug');
            $category->user()->associate(Auth::user());

            if ($category->save()) {

                //code for saving the image
                $this->makeDirectory('categories', ['default', 'front', 'back', 'slides']);
                if ($request->file('image')) {
                    $image = $request->file('image');
                    $image_name = sha1($category->id . "default") . '.' . $image->getClientOriginalExtension();
                    $image_path = public_path('uploads/categories/default') . '/' . $image_name;
                    $this->saveMedia($image, $image_path);
                    $category->image = 'uploads/categories/default/' . $image_name;
                    $category->save();
                }

                /*save the categories of the post*/
                $cities = $request->input('cities');
                foreach ($cities as $cid) {
                    $city = City::find($cid);
                    //save the posts based on the categories
                    $category->cities()->save($city);
                    if ($category->type == 'terminal') {
                        $category->terminals()->save(new Terminal([
                            'category_id' => $category->id,
                            'city_id' => $city->id,
                            'name' => $category->name . " " . $city->name
                        ]));
                    } else if ($category->type == 'cinema') {
                        $category->cinemas()->save(new Cinema([
                            'category_id' => $category->id,
                            'city_id' => $city->id,
                            'name' => $category->name . " " . $city->name
                        ]));
                    } else if ($category->type == 'shop') {
                        $category->shops()->save(new Shop([
                            'category_id' => $category->id,
                            'city_id' => $city->id,
                            'name' => $category->name . " " . $city->name
                        ]));
                    }
                }

                if ($category->type == 'terminal') {
                    Session::flash('success', 'New Terminals has been create successfully');
                    return redirect()->route('muntazim.terminals', [$parent->id]);
                } else if ($category->type == 'cinema') {
                    Session::flash('success', 'New Cinemas has been create successfully');
                    return redirect()->route('muntazim.cinemas', [$parent->id]);
                } else if ($category->type == 'shop') {
                    Session::flash('success', 'New Shop has been create successfully');
                    return redirect()->route('muntazim.shops', [$parent->id]);
                }

            } else {

                return redirect()->back()
                    ->withErrors('Failed to create post!')
                    ->withInput();
            }
        }
    }

    function makeDirectory($parent, $childs)
    {

        foreach ($childs as $child) {
            $path = public_path() . '/uploads/' . $parent . '/' . $child . '/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        }
    }

    function saveMedia($file, $path)
    {

        $saved_file = Image::make($file);
        $saved_file->resize(960, 640);
        $saved_file->save($path);

    }

    function deleteExistingFile($file)
    {
        $path = public_path() . "/" . $file;
        if (File::exists($path)) {
            File::delete($path);
        }

    }

    public function destroy(Request $request, $id)
    {
        $model = Category::find($id);
        $pid = $request->input('pid');
        foreach ($model->media()->get() as $media) {
            $this->deleteExistingFile($media->src);
        }
        if ($model->delete()) {
            Session::flash('danger', __('faqs.category_delete_success'));
            return redirect()->route('muntazim.categories', [$pid]);
        }
    }
}
