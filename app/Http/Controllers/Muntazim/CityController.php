<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\City;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CityController extends MuntazimController
{
    public function index(Request $request, $pid)
    {
        $parent = Country::find($pid);
        // TODO: Make filters work
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        $cities = City::where('country_id', $parent->id)->orderBy('name', 'ASC')->paginate(25);

        $pageTitle = __('faqs.all_cities');
        $smallTitle = '(' . $cities->count() . ')';
        $breadcrumbs = [['text' => __('faqs.all_cities')]];
        $viewParams = [
            'cities' => $cities,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'parent' => $parent
        ];

        return view('muntazim.cities.index', $viewParams);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = __('faqs.create_city');
        $smallTitle = '';

        $countries = Country::orderBy('name', 'ASC')->get()->pluck('name', 'id');

        $breadcrumbs = [
            ['text' => __('faqs.all_cities'), 'url' => route('muntazim.cities')],
            ['text' => __('faqs.create_city')],
        ];

        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'countries' => $countries,
        ];
        return view('muntazim.cities.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'country' => 'integer|required',
            'name' => 'max:100|required|alpha_spaces|unique:cities,name',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $city = new City();
            $city->name = $request->input('name');

            $country = Country::find($request->input('country'));

            if (!$country) {
                Session::flash('errorMessage', __('faqs.country_not_found'));
                return redirect()->back();
            }

            $city->country()->associate($country);

            if ($city->save()) {

                Session::flash('successMessage', __('faqs.create_city_success'));
                return redirect()->route('muntazim.cities');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.create_city_failed'))
                    ->withInput();
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::find($id);
        $countries = Country::orderBy('name', 'ASC')->get()->pluck('name', 'id');

        $pageTitle = !empty($city) ? __('faqs.edit') . $city->name : __('faqs.edit_city');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('faqs.all_cities'), 'url' => route('muntazim.cities')],
            ['text' => __('faqs.edit_city')],
        ];

        $viewParams = [
            'city' => $city,
            'countries' => $countries,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.cities.edit', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'country' => 'integer|required',
            'name' => 'max:100|alpha_spaces|unique:cities,name,' . $id,
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $city = City::find($id);

            if (!$city) {
                Session::flash('errorMessage', __('faqs.city_not_found'));
                return redirect()->back();
            }

            $city->name = $request->input('name');

            $country = Country::find($request->input('country'));

            if (!$country) {
                Session::flash('errorMessage', __('faqs.country_not_found'));
                return redirect()->back();
            }

            $city->country()->associate($country);

            if ($city->save()) {

                Session::flash('successMessage', __('faqs.city_update_success'));
                return redirect()->route('muntazim.cities');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.city_update_failed'))
                    ->withInput();
            }

        }
    }

    public function destroy($id)
    {
        $city = City::find($id);

        if ($city->delete()) {

            Session::flash('successMessage', __('faqs.city_delete_success'));
            return redirect()->route('muntazim.cities');

        }
    }
}
