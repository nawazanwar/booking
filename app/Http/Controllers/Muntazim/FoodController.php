<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Food;
use App\Models\Movie;
use App\Models\Post;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FoodController extends MuntazimController
{

    public function index(Request $request, $pid = null)
    {

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        $data = Food::with('shop')->whereHas('post', function ($query) {
            $query->where('posts.type', 'food');
        });
        if ($pid != null) {
            $data = $data->whereShopId($pid);
        }
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);
        $pageTitle = "All Foods";
        $smallTitle = $data->count();
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => __('faqs.all_posts'), 'url' => route('muntazim.posts')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'data' => $data,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.food.index', $viewParams);
    }



    public function edit(Request $request, $id)
    {
        $modal = Food::with('shop')->find($id);
        $pageTitle ='Edit Food';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.food.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $modal = Food::find($id);
        $modal->name = $request->input('name');
        $modal->price = $request->input('price');
        $modal->active = $request->input('active');
        $modal->public = $request->input('public');
        $modal->featured = $request->input('featured');
        if ($modal->save()) {
            Session::flash('success', 'Food has been updated');
            return redirect()->back();
        }
    }

    public function media(Request $request, $id)
    {
        $modal = Food::with('shop', 'media')->find($id);
        $pageTitle = "Edit Media";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'type' => 'food',
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.media.edit', $viewParams);
    }

}
