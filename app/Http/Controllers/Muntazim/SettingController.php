<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request){

        $settings = Setting::all();

        $pageTitle = __('faqs.all_settings');
        $smallTitle = '(' . count($settings) . ')';
        $breadcrumbs = [['text' => __('faqs.all_settings')]];

        $viewParams = [
            'settings' => $settings,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.settings.index', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        $input = $request->all();
        $settings = Setting::where('type', 'checkbox')->get();
        foreach($settings as $setting) {
            $input[$setting->name] = $request->input($setting->name, 0);
        }
        foreach ($input as $key => $value) {
            if (!strpos($key, '_token') && $key) {
                if ($request->file($key)) {
                    $imageName = sha1(time()) . '.' . $request->file($key)->getClientOriginalExtension();
                    $postImageFolder = public_path('images/settings');
                    \Intervention\Image\Facades\Image::make($request->file($key))->save($postImageFolder . '/' . $imageName);
                    $value = $imageName;
                }
                Setting::where('name', $key)
                    ->update(['value' => $value]);
            }
        }
        Session::flash('successMessage', 'Settings have been saved!');
        return redirect()->route('muntazim.settings');

    }
}
