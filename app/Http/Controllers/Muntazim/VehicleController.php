<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\City;
use App\Models\Post;
use App\Models\Route;
use App\Models\Terminal;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VehicleController extends MuntazimController
{
    public function index(Request $request, $pid = null)
    {
        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        $data = Vehicle::with('terminal')->whereHas('post', function ($query) {
            $query->where('posts.type', 'vehicle');
        });
        if ($pid != null) {
            $data = $data->whereTerminalId($pid);
        }
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);
        $pageTitle = "All Vehicles";
        $smallTitle = $data->count();
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => __('faqs.all_posts'), 'url' => route('muntazim.posts')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'data' => $data,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.vehicle.index', $viewParams);
    }

    public function seats(Request $request, $id)
    {
        $modal = Vehicle::with('terminal')->find($id);
        $pageTitle ='Vehicle Seats';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.vehicle.seats', $viewParams);
    }

    public function edit(Request $request, $id)
    {
        $modal = Vehicle::with('terminal')->find($id);
        $pageTitle ='Edit Vehicle';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.vehicle.edit', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $up_type = $request->input('up_type');
        $data = $request->all();

        if ($up_type == 'detail') {

            $modal = Vehicle::find($id);
            $modal->time = $request->input('time');
            $modal->seats = $request->input('seats');
            $modal->transport = $request->input('transport');
            $modal->booking = $request->input('booking');
            $modal->property = $request->input('property');
            $modal->name = $request->input('name');
            $modal->active = $request->input('active');
            $modal->public = $request->input('public');
            $modal->featured = $request->input('featured');
            if ($modal->save()) {
                Session::flash('success', 'Vehicle has been updated');
                return redirect()->back();
            }
        } else {
            $input = $request->all();

            $data = array();
            $length = count($input['city']);
            for ($i = 0; $i < $length; $i++) {
                if (isset($input['id'][$i]) && !empty($input['id'][$i])) {

                    Route::where('id', '=', $input['id'][$i])
                        ->update([
                            'city_id' => $input['city'][$i],
                            'time' => $input['time'][$i],
                            'duration' => $input['duration'][$i],
                            'ticket' => $input['ticket'][$i],
                            'vehicle_id' => $id
                        ]);

                } else {
                    $data[] = array(
                        'city_id' => $input['city'][$i],
                        'time' => $input['time'][$i],
                        'duration' => $input['duration'][$i],
                        'ticket' => $input['ticket'][$i],
                        'vehicle_id' => $id
                    );
                }
            }
            if (Route::insert($data)) {
                Session::flash('success', 'Route has been Added!');
                return redirect()->back();

            }
        }
    }

    public function routes(Request $request, $id)
    {
         $modal = Vehicle::with('terminal')->find($id);
         $routes = Route::whereVehicleId($modal->id)->get();
         $cities = City::where('country_id', 165)->pluck('name', 'id');
         $pageTitle ="All routes";
         $smallTitle = '';
         $breadcrumbs = [
             ['text' =>$pageTitle],
         ];

         $viewParams = [
             'modal' => $modal,
             'breadcrumbs' => $breadcrumbs,
             'pageTitle' => $pageTitle,
             'smallTitle' => $smallTitle,
             'cities' => $cities,
             'routes' => $routes,
             'src' => $request->input('src')
         ];

         return view('muntazim.vehicle.routes', $viewParams);
    }

    public function media(Request $request, $id)
    {
        $modal = Vehicle::with('terminal', 'media')->find($id);
        $pageTitle = "Edit Media";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'type' => 'vehicle',
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.media.edit', $viewParams);
    }

}
