<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Nav;
use App\Models\NavLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class NavController extends MuntazimController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of main nav.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        $navs = Nav::paginate(15);

        $pageTitle = __('faqs.navigation');
        $smallTitle = '(' . count($navs) . ')';
        $breadcrumbs = [['text' => __('faqs.navigation')]];
        $viewParams = [
            'navs' => $navs,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,

            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
        ];

        return view('muntazim.nav.index', $viewParams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = __('faqs.create_nav');
        $smallTitle = '';

        $breadcrumbs = [
            ['text' => __('faqs.all_navs'), 'url' => route('muntazim.nav')],
            ['text' => __('faqs.create_nav')],
        ];

        $viewParams = [
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.nav.create', $viewParams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'identifier' => 'max:50|alpha_spaces|unique:nav,identifier',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $nav = new Nav();
            $nav->identifier = $request->input('identifier');

            if ($nav->save()) {

                Session::flash('successMessage', __('faqs.create_nav_success'));
                return redirect()->route('muntazim.nav');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.create_nav_failed'))
                    ->withInput();
            }

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nav = Nav::find($id);
        $pageTitle = !empty($nav) ? __('faqs.edit') . $nav->identifier : __('faqs.edit_nav');
        $smallTitle = '';
        $breadcrumbs = [
            ['text' => __('faqs.all_navs'), 'url' => route('muntazim.nav')],
            ['text' => __('faqs.edit_nav')],
        ];

        $viewParams = [
            'nav' => $nav,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];

        return view('muntazim.nav.edit', $viewParams);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'identifier' => 'max:50|alpha_spaces|unique:nav,identifier,' . $id,
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $nav = Nav::find($id);
            $nav->identifier = $request->input('identifier');
            $nav->active = $request->input('active', 0);

            if ($nav->save()) {

                Session::flash('successMessage', $nav->identifier . __('faqs.nav_update_success'));
                return redirect()->route('muntazim.nav');

            } else {

                return redirect()->back()
                    ->withErrors(__('faqs.nav_update_failed'))
                    ->withInput();

            }

        }

    }

    /**
     * Destroy a nav item.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nav = Nav::find($id);

        if ($nav->delete()) {

            Session::flash('successMessage', __('faqs.nav_delete_success'));
            return redirect()->route('muntazim.nav');

        }
    }

    /**
     * Display a listing of nav links.
     *
     * @return \Illuminate\Http\Response
     */
    public function links(Request $request, $id)
    {
        $navLinks = NavLink::where('nav_id', $id)->get();

        $pageTitle = __('faqs.navigation_links');
        $smallTitle = '(' . count($navLinks) . ')';
        $breadcrumbs = [
            ['text' => __('faqs.all_navs'), 'url' => route('muntazim.nav')],
            ['text' => __('faqs.navigation_links')]
        ];
        $viewParams = [
            'navLinks' => $navLinks,
            'navId' => $id,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.nav.links', $viewParams);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function storeLinks(Request $request, $id)
    {

        $rules = [
            'title' => 'array|required',
            'url' => 'array|required',
            'order' => 'array|required',
            'is_blank' => 'array|nullable',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator)->withInput();

        } else {

            $input = $request->all();

            $data = array();
            $length = count($input['title']);

            for ($i = 0; $i < $length; $i++) {

                if (isset($input['id'][$i]) && !empty($input['id'][$i])) {

                    NavLink::where('id', $input['id'][$i])
                        ->where('nav_id', $id)
                        ->update([
                            'nav_id' => $id,
                            'title' => $input['title'][$i],
                            'url' => $input['url'][$i],
                            'order' => $input['order'][$i],
                            'is_blank' => isset($input['is_blank'][$i]) ? $input['is_blank'][$i] : 0,
                        ]);

                } else {

                    $data[] = array(
                        'nav_id' => $id,
                        'title' => $input['title'][$i],
                        'url' => $input['url'][$i],
                        'order' => $input['order'][$i],
                        'is_blank' => isset($input['is_blank'][$i]) ? $input['is_blank'][$i] : 0,
                    );

                }

            }

            if (NavLink::insert($data)) {

                NavLink::whereNotIn('id', $input['id'])->where('nav_id', $id)->delete();

                Session::flash('successMessage', __('faqs.nav_links_success_message'));

                return redirect()->back();

            } else {
                return redirect()->back()
                    ->withErrors(__('faqs.nav_links_error_message'))
                    ->withInput();
            }
        }

    }

    /**
     * Display a listing of top nav.
     *
     * @return \Illuminate\Http\Response
     */
    public function top(Request $request)
    {
        $nav = Nav::where('type', 'top')->get();

        $pageTitle = 'Navigation';
        $smallTitle = '(' . count($nav) . ')';
        $breadcrumbs = [['text' => 'Navigation']];
        $viewParams = [
            'nav' => $nav,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle
        ];

        return view('muntazim.nav.top', $viewParams);
    }

}
