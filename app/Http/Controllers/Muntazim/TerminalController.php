<?php

namespace App\Http\Controllers\Muntazim;

use App\Models\Category;
use App\Models\Terminal;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TerminalController extends MuntazimController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $pid = null)
    {
        $parent = Category::find($pid);

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);
        $data = Terminal::with('city')->whereHas('category', function ($query) use ($pid) {
            $query->where('categories.parent_id', $pid)->with('cities');
        });
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);

        $pageTitle = "All Terminals";
        $smallTitle = $data->count();
        $breadcrumbs = [
            ['text' => __('faqs.all_categories'), 'url' => route('muntazim.categories')],
            ['text' => $pageTitle],
        ];
        $viewParams = [
            'data' => $data,
            'field' => $field,
            'keyword' => $keyword,
            'filters' => $filters,
            'parent' => $parent,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('muntazim.terminal.index', $viewParams);
    }

    public function edit(Request $request, $id)
    {
        $modal = Terminal::with('category')->find($id);
        $pageTitle = "Edit Terminal";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('muntazim.terminal.edit', $viewParams);
    }

    public function update(Request $request, $id)
    {
        $modal = Terminal::find($id);
        $modal->address = $request->input('address');
        $modal->name = $request->input('name');
        $modal->active = $request->input('active');
        $modal->public = $request->input('public');
        $modal->featured = $request->input('featured');
        if ($modal->save()) {
            Session::flash('success', 'Terminal has been updated');
            return redirect()->back();
        }
    }

    public function media(Request $request, $id)
    {
        $modal = Terminal::with('category','media')->find($id);
        $pageTitle = "Edit Media";
        $smallTitle = '';
        $breadcrumbs = [];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'type' => 'terminal',
            'src' => $request->input('src')
        ];

        return view('muntazim.media.edit', $viewParams);
    }
}
