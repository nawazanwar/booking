<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Cinema;
use App\Models\City;
use App\Models\Shop;
use App\Models\Terminal;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * view the single and all resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request, $slug = null)
    {

        $field = $request->query('field', null);
        $keyword = $request->query('keyword', null);
        $filters = $request->query('filter', null);

        if ($slug != '') {


            if ($slug == 'cinemas') {
                $data = Cinema::with('city');
            } else if ($slug == 'terminals') {
                $data = Terminal::with('city');
            } else if ($slug == 'food-points') {
                $data = Shop::with('city');
            }
            $data = $data->orderBy('created_at', 'ASC')->paginate(20);

            $pageTitle = 'All Categories';
            $smallTitle = '(' . count($data) . ')';
            $breadcrumbs = [['text' => 'All Categories']];
            $viewParams = [
                'field' => $field,
                'keyword' => $keyword,
                'filters' => $filters,
                'data' => $data,
                'slug' => $slug,
                'breadcrumbs' => $breadcrumbs,
                'pageTitle' => $pageTitle,
                'smallTitle' => $smallTitle,
            ];

            return view('category.index', $viewParams);

        } else {
            $viewParams = [
                'pageTitle' => "home"
            ];
            return view('homepage', $viewParams);

        }
    }

    public function answer(Request $request, $slug)
    {

        $parent_id = $request->input('parent_id');

        $answers = Answer::with('posts', 'user')->whereHas('posts', function ($q) use ($parent_id) {

            $q->where('id', '=', $parent_id);

        })->where(['slug' => $slug, 'public' => 1, 'publish' => 1])->first();

        echo "pleas answer";

        /*$this->viewParams = [
            'answer' => $answers,
            'pageTitle' => 'View Answer',
            'for' => 'answers'
        ];

        return view('article', $this->viewParams);*/

    }

    /*public function topic($slug)
    {
        $model = Topic::whereSlug($slug)->first();
        $page_title = $model->title;
        $posts = Question::with('topics', 'user', 'votes', 'answers')
            ->withCount('answers', 'topics', 'upvotes', 'downvotes', 'votes')
            ->wherehas('topics', function ($query) use ($slug) {
                $query->whereSlug($slug);
            })->where(['public' => true, 'publish' => true])->paginate(10);

        $viewParams = [
            'model' => $model,
            'data' => $posts,
            'pageTitle' => $page_title,
            'for' => 'TopicPosts'
        ];

        return view('article', $viewParams);
    }*/

    public function getCities($id)
    {

        $cities = City::orderBy('name', 'ASC')->where('country_id', $id)->get()->pluck('name', 'id');

        if ($cities) {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'cities' => $cities,
                ]
            ]);
        }

        return response()->json([
            'status' => 'error',
            'errors' => ['An error occurred with current data.'],
            'data' => [],
        ]);

    }

    public function autoCities(Request $request)
    {
        $search = $request->get('term');

        $result = City::where('name', 'LIKE', '%' . $search . '%')->get();

        return response()->json($result);

    }
}
