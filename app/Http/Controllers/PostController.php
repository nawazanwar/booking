<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Movie;
use App\Models\Route;
use App\Models\Vehicle;
use Auth;
use Illuminate\Http\Request;

class PostController extends Controller
{
    //list of all movies

    public function movies(Request $request, $pid = null)
    {
        $data = Movie::with('cinema')->whereHas('post', function ($query) {
            $query->where('posts.type', 'movie');
        });
        if ($pid != null) {
            $data = $data->whereCinemaId($pid);
        }
        $data = $data->orderBy('created_at', 'DESC')->paginate(20);
        $pageTitle = "All Movies";
        $smallTitle = $data->count();
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'data' => $data,
            'type' => 'movie',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('post.index', $viewParams);
    }

    //list of all foods
    public function foods(Request $request, $pid = null)
    {
        $data = Food::with('shop')->whereHas('post', function ($query) {
            $query->where('posts.type', 'food');
        });
        if ($pid != null) {
            $data = $data->whereShopId($pid);
        }
        $data = $data->orderBy('created_at', 'ASC')->paginate(12);
        $pageTitle = "All Foods";
        $smallTitle = $data->count();
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'data' => $data,
            'type' => 'food',
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
        ];
        return view('post.index', $viewParams);
    }

    //list of all vehicles
    public function vehicles(Request $request, $pid = null)
    {
        $data = Vehicle::with('terminal')->whereHas('post', function ($query) {
            $query->where('posts.type', 'vehicle');
        });
        if ($pid != null) {
            $data = $data->whereTerminalId($pid);
        }
        $data = $data->orderBy('created_at', 'DESC')->paginate(20);
        $pageTitle = "All Vehicles";
        $smallTitle = $data->count();
        $breadcrumbs = [['text' => $pageTitle]];
        $viewParams = [
            'data' => $data,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'type' => 'vehicle'
        ];
        return view('post.index', $viewParams);
    }

    //list of movie seats

    public function movieSeats(Request $request, $id)
    {
        $modal = Movie::with('cinema')->find($id);
        $pageTitle ='Movie Seats';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('movie.seats', $viewParams);
    }

    public function vehicleSeats(Request $request,$id){
        $routes = Route::with('cities')->whereVehicleId($id)->get();
        $modal = Vehicle::with('terminal')->find($id);
        $pageTitle ='Vehicle Seats';
        $smallTitle = '';
        $breadcrumbs = [
            ['text'=>$pageTitle],
        ];
        $viewParams = [
            'routes'=>$routes,
            'modal' => $modal,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle' => $pageTitle,
            'smallTitle' => $smallTitle,
            'src' => $request->input('src')
        ];

        return view('vehicle.seats', $viewParams);
    }
}
