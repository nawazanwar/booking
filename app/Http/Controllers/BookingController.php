<?php

namespace App\Http\Controllers;

use App\Models\Bmovie;
use App\Models\Bvehicle;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vehicle(Request $request)
    {
        $bvehicle = new Bvehicle();
        $vehicle_id = $request->input('vehicle_id');
        $seat_id = $request->input('seat_id');
        $route_id = $request->input('route_id');
        $already_booking = $bvehicle->alreadyBooking($vehicle_id, $seat_id);
        if ($already_booking > 0) {
            return response()->json(['status' => 'false']);
        } else {
            $bvehicle->user()->associate(Auth::user());
            $bvehicle->vehicle_id = $vehicle_id;
            $bvehicle->seat_id = $seat_id;
            $bvehicle->route_id = $route_id;
            $bvehicle->phone = $request->input('phone');
            if ($bvehicle->save()) {
                return response()->json(['status' => 'true']);
            } else {
                return response()->json(['status' => 'false']);
            }
        }
    }

    public function movie(Request $request)
    {
        $bmovis = new Bmovie();
        $movie_id = $request->input('movie_id');
        $seat_id = $request->input('seat_id');
        $already_booking = $bmovis->alreadyBooking($movie_id, $seat_id);
        if ($already_booking > 0) {
            return response()->json(['status' => 'false']);
        } else {
            $bmovis->user()->associate(Auth::user());
            $bmovis->movie_id = $movie_id;
            $bmovis->seat_id = $seat_id;
            $bmovis->phone = $request->input('phone');
            if ($bmovis->save()) {
                return response()->json(['status' => 'true']);
            } else {
                return response()->json(['status' => 'false']);
            }
        }
    }
}
