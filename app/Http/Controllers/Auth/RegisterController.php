<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\S3;
use App\Mail\VerifyMail;
use App\Models\City;
use App\Models\Country;
use App\Models\Role;
use App\Models\User;
use App\Models\VerifyUser;
use DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Image;
use Socialite;

class RegisterController extends Controller
{
    use RegistersUsers, S3;

    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @param Request $request
     * @return User
     */
    protected function create(array $data)
    {

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->username = $data['username'];
        $user->gender = $data['gender'];
        $user->password = bcrypt($data['password']);
        $user->active = 1;

        if ($user->save()) {

            if (isset($data['country'])) {
                $country = Country::find($data['country']);
                $user->country()->associate($country);
            }

            if (isset($data['city'])) {
                $city = City::find($data['city']);
                $user->city()->associate($city);
            }

            $role = Role::find(2);
            $user->roles()->save($role);
            $user->save();
            return $user;
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    public function showRegistrationForm()
    {
        $pageTitle = "Registration";

        $countries = Country::orderBy('name', 'ASC')->get()->pluck('name', 'id');
        $cities = City::orderBy('name', 'ASC')->get()->pluck('name', 'id');

        $viewParams = [
            'pageTitle' => $pageTitle,
            'countries' => $countries,
            'cities' => $cities
        ];

        return view('auth.register', $viewParams);
    }

    public function activate($token)
    {

        if (!$token) {
            return abort(404);
        }

        $verifyUser = VerifyUser::with('user')->where('token', $token)->first();

        if (!$verifyUser) {
            return abort(404);
        }

        if ($verifyUser) {

            $user = $verifyUser->user;
            $user->active = true;

            $verifyUser->delete();

            if ($user->save()) {

                Session::flash('successMessage', __('faqs.account_activated'));
                return redirect('/');

            }

        }

        return abort(404);

    }

}
