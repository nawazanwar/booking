<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/';

    public function __construct()
    {

        $this->middleware('guest', ['except' => 'logout']);

    }

    public function showLoginForm()
    {
        $viewParams = [
            'pageTitle' => __('faqs.login'),
        ];
        return view('auth.login', $viewParams);
    }

    protected function sendLoginResponse(Request $request)
    {

        return redirect('/');
    }

    protected function credentials(Request $request)
    {
        return ['email' => $request->email, 'password' => $request->password, 'active' => 1];
    }

    public function logout()
    {
        \Auth::logout();
        return redirect("/");
    }
}
