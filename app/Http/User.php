<?php
namespace App;
use Illuminate\Notifications\Notifiable;
class User{
    use Notifiable;
    protected $fillable = [
        'fname','lname','country','state','city','zip','phone','mobile','gender','image','email','password','active'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
