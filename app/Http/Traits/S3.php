<?php

namespace App\Http\Traits;


trait S3
{

    function getObject($object)
    {

        if ($object) {
            return \Storage::disk('s3')->get($object);
        }

    }

    function upload($file, $folder, $fromFile = true, $name = '')
    {

        if ($name) {
            $imageFileName = $name;
        } else {
            $imageFileName = time() . '.' . $file->getClientOriginalExtension();
        }

        $s3 = \Storage::disk('s3');
        $filePath = $folder . '/' . $imageFileName;
        if ($fromFile) {
            $s3->put($filePath, file_get_contents($file), 'public');
        } else {
            $s3->put($filePath, $file, 'public');
        }


        return $imageFileName;

    }

    function delete($file, $folder)
    {
        \Storage::disk('s3')->delete($folder . '/' . $file);
    }

}