<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Food;
use App\Models\Movie;
use App\Models\Nav;
use App\Models\Question;
use App\Models\Setting;
use App\Models\Topic;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Provide Settings for All Views
        if (Schema::hasTable('settings')) {
            $settings = Setting::pluck('value', 'name')->all();
            View::share('generalSetting', $settings);
        } else {
            View::share('generalSetting', array());
        }

        // Navigation
        if (Schema::hasTable('nav') && Schema::hasTable('nav_links')) {
            View::share('mainNav', Nav::with('links')->where('identifier', 'top-nav')->first());
            View::share('footerNav', Nav::with('links')->where('identifier', 'footer-nav')->first());
        } else {
            View::share('mainNav', array());
            View::share('footerNav', array());
        }

        // Provide Topics to be displayed on sidebars
        if (Schema::hasTable('categories')) {
            $latestCategories = Category::whereParentId(0)->orderBy('id', 'ASC')->get();
            View::share('latestCategories', $latestCategories);
        } else {
            View::share('latestCategories', array());
        }

        //Latest movies,foods,vehicles

        if (Schema::hasTable('movies')) {
            $latestMovies =  Movie::with('cinema')->whereHas('post', function ($query) {
                $query->where('posts.type', 'movie');
            })->orderBy('created_at','desc')->take(3)->get();
            View::share('latestMovies', $latestMovies);
        } else {
            View::share('latestMovies', array());
        }
        //latest foods
        if (Schema::hasTable('foods')) {
            $latestFoods = Food::with('shop')->whereHas('post', function ($query) {
                $query->where('posts.type', 'food');
            })->orderBy('created_at','desc')->take(3)->get();
            View::share('latestFoods', $latestFoods);
        } else {
            View::share('latestFoods', array());
        }
        //latest vehicles
        if (Schema::hasTable('vehicles')) {
            $latestMovies =  Vehicle::with('terminal')->whereHas('post', function ($query) {
                $query->where('posts.type', 'vehicle');
            })->orderBy('created_at','desc')->take(3)->get();
            View::share('latestVehicles', $latestMovies);
        } else {
            View::share('latestVehicles', array());
        }
        // alpha_spaces validation rule
        Validator::extend('alpha_spaces', function ($attributes, $value) {
            return preg_match('/^[\pL\s-]+$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
