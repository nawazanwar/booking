<?php

namespace App\Providers;

use App\Models\Answer;
use App\Models\Booking;
use App\Models\Category;
use App\Models\Cinema;
use App\Models\City;
use App\Models\Comment;
use App\Models\Country;
use App\Models\Food;
use App\Models\Media;
use App\Models\Movie;
use App\Models\Nav;
use App\Models\NavLink;
use App\Models\Page;
use App\Models\Permission;
use App\Models\Post;
use App\Models\Role;
use App\Models\Shop;
use App\Models\Terminal;
use App\Models\User;
use App\Models\Vehicle;
use App\Policies\BookingPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\CinemaPolicy;
use App\Policies\CityPolicy;
use App\Policies\CommentPolicy;
use App\Policies\CountryPolicy;
use App\Policies\FoodPolicy;
use App\Policies\MediaPolicy;
use App\Policies\MoviePolicy;
use App\Policies\NavLinkPolicy;
use App\Policies\NavPolicy;
use App\Policies\PagePolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PostPolicy;
use App\Policies\RolePolicy;
use App\Policies\ShopPolicy;
use App\Policies\TerminalPolicy;
use App\Policies\UserPolicy;
use App\Policies\VehiclePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Comment::class => CommentPolicy::class,
        Country::class => CountryPolicy::class,
        City::class => CityPolicy::class,
        Nav::class => NavPolicy::class,
        NavLink::class => NavLinkPolicy::class,
        Page::class => PagePolicy::class,
        Category::class => CategoryPolicy::class,
        Post::class => PostPolicy::class,
        Booking::class => BookingPolicy::class,
        Movie::class => MoviePolicy::class,
        Vehicle::class => VehiclePolicy::class,
        Food::class => FoodPolicy::class,
        Media::class => MediaPolicy::class,
        Shop::class => ShopPolicy::class,
        Terminal::class => TerminalPolicy::class,
        Cinema::class => CinemaPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
