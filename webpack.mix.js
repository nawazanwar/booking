let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix
//.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/mail.scss', 'public/css')
    .styles([
        'resources/assets/theme/css/page.min.css',
        'resources/assets/theme/css/style.min.css',
        'node_modules/bootstrap-select/dist/css/bootstrap-select.min.css',
        'node_modules/jquery-ui-dist/jquery-ui.min.css',
        'node_modules/jquery-timepicker/jquery.timepicker.css',
    ], 'public/css/vendor.min.css')
    .scripts([
        'resources/assets/theme/js/page.min.js',
        'resources/assets/theme/js/script.js',
        'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        'node_modules/jquery.cookie/jquery.cookie.js',
        'node_modules/jquery-ui-dist/jquery-ui.min.js',
        'node_modules/jquery-timepicker/jquery.timepicker.js',
        'resources/assets/js/ajax.js',
        'resources/assets/js/cart.js'
    ], 'public/js/vendor.min.js')
    .scripts([
        'resources/assets/js/custom.js'
    ], 'public/js/app.min.js')
    .copy([
        'resources/assets/theme/fonts',
    ], 'public/fonts')
    .copy([
        'resources/assets/theme/img',
    ], 'public/img');
