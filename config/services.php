<?php

return [

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id'     =>'880451465406163',
        'client_secret' => '4e4658245103d2b739f50b08eb307525',
        'redirect'      => 'http://meme.local/auth/facebook/callback',
    ],
    'twitter' => [
        'client_id'     =>'fflpzyAB0apNXQwebyR4z45Wj',
        'client_secret' => '384ZQwfV4R6XLBqUbeucTpTfXn2fAs6ph9KUP4PEO32eiycTDi',
        'redirect'      => 'http://meme.local/auth/twitter/callback',
    ],

];
