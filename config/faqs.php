<?php

return [

    'twitter' => [
        'CONSUMER_KEY' => env('TWITTER_KEY', 'Qwiyd29fQctkYUUOQMGEya566'),
        'CONSUMER_SECRET' => env('TWITTER_SECRET', 'Va3S49UeYzuXQo8dnbOa2i34IWLntpxhgFfer93sRc0z1UXTGs'),
        'OAUTH_CALLBACK' => env('TWITTER_CALLBACK', 'http://alifbey.news/giveaway/additional/twitter/'),
    ],

    's3' => [
        'avatars' => env('S3_AVATAR', 'avatars'),
        'topics' => env('S3_TOPIC', 'topics'),
        'questions' => env('S3_QUESTION', 'questions'),
        'answers' => env('S3_ANSWER', 'answers')
    ],

    'shorty' => env('SHORTY', true),

    'analytics' => env('ANALYTICS', false),

    'adsense' => env('ADSENSE', false),
    'adsense_id' => env('ADSENSE_ID', ''),

    'facebook_page_id' => env('FACEBOOK_PAGE_ID', '279368135805666'),
];