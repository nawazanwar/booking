<?php
//here are our routes that are used for Law project
Auth::routes();
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\LoginController@logout']);


Route::get('/{slug?}', ['as' => 'home', 'uses' => 'HomeController@index', 'middleware' => 'maintenance']);
// get cities by country id
Route::get('/cities/autocomplete', ['as' => 'cities.autocomplete', 'uses' => 'HomeController@autoCities', 'middleware' => 'maintenance']);
Route::get('/cities/{id}', ['as' => 'home.cities', 'uses' => 'HomeController@getCities', 'middleware' => 'maintenance']);
// Posts
Route::get('/posts/{id?}', ['as' => 'posts', 'uses' => 'PostController@index']);
Route::get('/post/seats/{id}', ['as' => 'post.seats', 'uses' => 'PostController@seats']);

// General
Route::get('search', ['as' => 'search', 'uses' => 'SearchController@index']);

// Muntazim Routes
Route::group(['prefix' => 'muntazim', 'middleware' => ['permissions']], function () {

    // Settings
    Route::get('/settings', ['as' => 'muntazim.settings', 'uses' => 'Muntazim\SettingController@index',
        'permissions' => \App\Models\Setting::modulePermissions(true, 'manage')]);
    Route::put('/settings/update', ['as' => 'muntazim.settings.update', 'uses' => 'Muntazim\SettingController@update',
        'permissions' => \App\Models\Setting::modulePermissions(true, 'edit')]);

    // Users
    Route::get('/users', ['as' => 'muntazim.users', 'uses' => 'Dashboard\UserController@index',
        'permissions' => \App\Models\User::modulePermissions(true, 'manage')]);
    Route::get('/users/show/{id}', ['as' => 'muntazim.users.show', 'uses' => 'Dashboard\UserController@show',
        'permissions' => \App\Models\Role::modulePermissions(true, 'show')]);
    Route::get('/user/create', ['as' => 'muntazim.user.create', 'uses' => 'Dashboard\UserController@create',
        'permissions' => \App\Models\User::modulePermissions(true, 'create')]);
    Route::get('/user/assign', ['as' => 'muntazim.user.assign', 'uses' => 'Dashboard\UserController@assign',
        'permissions' => \App\Models\Role::modulePermissions(true, 'assign')]);
    Route::post('/user/store', ['as' => 'muntazim.user.store', 'uses' => 'Dashboard\UserController@store',
        'permissions' => \App\Models\User::modulePermissions(true, 'store')]);
    Route::get('/user/edit/{id}', ['as' => 'muntazim.user.edit', 'uses' => 'Dashboard\UserController@edit',
        'permissions' => \App\Models\User::modulePermissions(true, 'edit')]);
    Route::put('/user/update/{id}', ['as' => 'muntazim.user.update', 'uses' => 'Dashboard\UserController@update',
        'permissions' => \App\Models\User::modulePermissions(true, 'update')]);
    Route::get('/user/delete/{id}', ['as' => 'muntazim.user.delete', 'uses' => 'Dashboard\UserController@destroy',
        'permissions' => \App\Models\User::modulePermissions(true, 'delete')]);
    // Roles
    Route::get('/roles', ['as' => 'muntazim.roles', 'uses' => 'Muntazim\RoleController@index',
        'permissions' => \App\Models\Role::modulePermissions(true, 'manage')]);
    Route::get('/roles/show/{id}', ['as' => 'muntazim.roles.show', 'uses' => 'Muntazim\RoleController@show',
        'permissions' => \App\Models\Role::modulePermissions(true, 'show')]);
    Route::get('/role/create', ['as' => 'muntazim.roles.create', 'uses' => 'Muntazim\RoleController@create',
        'permissions' => \App\Models\Role::modulePermissions(true, 'create')]);
    Route::get('/role/assign', ['as' => 'muntazim.roles.assign', 'uses' => 'Muntazim\RoleController@assign',
        'permissions' => \App\Models\Role::modulePermissions(true, 'assign')]);
    Route::post('/role/store', ['as' => 'muntazim.roles.store', 'uses' => 'Muntazim\RoleController@store',
        'permissions' => \App\Models\Role::modulePermissions(true, 'store')]);
    Route::get('/role/edit/{id}', ['as' => 'muntazim.roles.edit', 'uses' => 'Muntazim\RoleController@edit',
        'permissions' => \App\Models\Role::modulePermissions(true, 'edit')]);
    Route::put('/role/update/{id?}', ['as' => 'muntazim.roles.update', 'uses' => 'Muntazim\RoleController@update',
        'permissions' => \App\Models\Role::modulePermissions(true, 'update')]);
    Route::get('/role/delete/{id}', ['as' => 'muntazim.roles.delete', 'uses' => 'Muntazim\RoleController@destroy',
        'permissions' => \App\Models\User::modulePermissions(true, 'delete')]);
    //permissions
    Route::get('/permissions', ['as' => 'muntazim.permissions', 'uses' => 'Muntazim\PermissionController@index',
        'permissions' => \App\Models\Permission::modulePermissions(true, 'manage')]);
    Route::get('/permissions/show/{id}', ['as' => 'muntazim.permissions.show', 'uses' => 'Muntazim\PermissionController@show',
        'permissions' => \App\Models\Role::modulePermissions(true, 'show')]);
    Route::get('/permissions/assign', ['as' => 'muntazim.permissions.assign', 'uses' => 'Muntazim\PermissionController@assign',
        'permissions' => \App\Models\Role::modulePermissions(true, 'assign')]);
    Route::put('/permission/update', ['as' => 'muntazim.permissions.update', 'uses' => 'Muntazim\PermissionController@update',
        'permissions' => \App\Models\Role::modulePermissions(true, 'update')]);
    Route::get('/permission/delete/{id}', ['as' => 'muntazim.permissions.delete', 'uses' => 'Muntazim\PermissionController@destroy',
        'permissions' => \App\Models\User::modulePermissions(true, 'delete')]);
    Route::get('/permission/sync', ['as' => 'muntazim.permissions.sync', 'uses' => 'Muntazim\PermissionController@syncPermissions',
        'permissions' => \App\Models\Permission::modulePermissions(true, 'sync')]);
    // Nav
    Route::get('/nav', ['as' => 'muntazim.nav', 'uses' => 'Muntazim\NavController@index',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'manage')]);

    Route::get('/nav/links/{id}', ['as' => 'muntazim.nav.links', 'uses' => 'Muntazim\NavController@links',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'manage')]);
    Route::put('/nav/links/store/{id}', ['as' => 'muntazim.nav.links.store', 'uses' => 'Muntazim\NavController@storeLinks',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'manage')]);

    Route::get('/nav/create', ['as' => 'muntazim.nav.create', 'uses' => 'Muntazim\NavController@create',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'create')]);
    Route::post('/nav/store', ['as' => 'muntazim.nav.store', 'uses' => 'Muntazim\NavController@store',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'create')]);

    Route::get('/nav/edit/{id}', ['as' => 'muntazim.nav.edit', 'uses' => 'Muntazim\NavController@edit',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'edit')]);
    Route::put('/nav/update/{id}', ['as' => 'muntazim.nav.update', 'uses' => 'Muntazim\NavController@update',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'update')]);

    Route::get('/nav/delete/{id}', ['as' => 'muntazim.nav.delete', 'uses' => 'Muntazim\NavController@destroy',
        'permissions' => \App\Models\Nav::modulePermissions(true, 'delete')]);

    // Countries
    Route::get('/countries', ['as' => 'muntazim.countries', 'uses' => 'Muntazim\CountryController@index',
        'permissions' => \App\Models\Country::modulePermissions(true, 'manage')]);

    Route::get('/country/create', ['as' => 'muntazim.countries.create', 'uses' => 'Muntazim\CountryController@create',
        'permissions' => \App\Models\Country::modulePermissions(true, 'create')]);
    Route::post('/country/store', ['as' => 'muntazim.countries.store', 'uses' => 'Muntazim\CountryController@store',
        'permissions' => \App\Models\Country::modulePermissions(true, 'store')]);

    Route::get('/country/edit/{id}', ['as' => 'muntazim.countries.edit', 'uses' => 'Muntazim\CountryController@edit',
        'permissions' => \App\Models\Country::modulePermissions(true, 'edit')]);
    Route::put('/country/update/{id}', ['as' => 'muntazim.countries.update', 'uses' => 'Muntazim\CountryController@update',
        'permissions' => \App\Models\Country::modulePermissions(true, 'update')]);

    Route::get('/country/delete/{id}', ['as' => 'muntazim.countries.delete', 'uses' => 'Muntazim\CountryController@destroy',
        'permissions' => \App\Models\Country::modulePermissions(true, 'delete')]);

    // Cities
    Route::get('/cities/{pid}', ['as' => 'muntazim.cities', 'uses' => 'Muntazim\CityController@index',
        'permissions' => \App\Models\Country::modulePermissions(true, 'manage')]);

    Route::get('/city/create', ['as' => 'muntazim.cities.create', 'uses' => 'Muntazim\CityController@create',
        'permissions' => \App\Models\Country::modulePermissions(true, 'create')]);
    Route::post('/city/store', ['as' => 'muntazim.cities.store', 'uses' => 'Muntazim\CityController@store',
        'permissions' => \App\Models\Country::modulePermissions(true, 'store')]);

    Route::get('/city/edit/{id}', ['as' => 'muntazim.cities.edit', 'uses' => 'Muntazim\CityController@edit',
        'permissions' => \App\Models\Country::modulePermissions(true, 'edit')]);
    Route::put('/city/update/{id}', ['as' => 'muntazim.cities.update', 'uses' => 'Muntazim\CityController@update',
        'permissions' => \App\Models\Country::modulePermissions(true, 'update')]);

    Route::get('/city/delete/{id}', ['as' => 'muntazim.cities.delete', 'uses' => 'Muntazim\CityController@destroy',
        'permissions' => \App\Models\Country::modulePermissions(true, 'delete')]);

    // Categories

    Route::get('/categories/{pid?}', ['as' => 'muntazim.categories', 'uses' => 'Muntazim\CategoryController@index',
        'permissions' => \App\Models\Category::modulePermissions(true, 'manage')]);
    Route::get('/categories/create/{parent_id?}', ['as' => 'muntazim.categories.create', 'uses' => 'Muntazim\CategoryController@create',
        'permissions' => \App\Models\Category::modulePermissions(true, 'create')]);
    Route::post('/categories/store/{pid?}', ['as' => 'muntazim.categories.store', 'uses' => 'Muntazim\CategoryController@store',
        'permissions' => \App\Models\Category::modulePermissions(true, 'create')]);
    Route::get('/categories/show/{id}', ['as' => 'muntazim.categories.show', 'uses' => 'Muntazim\CategoryController@show',
        'permissions' => \App\Models\Category::modulePermissions(true, 'show')]);
    Route::get('/categories/edit/{id}', ['as' => 'muntazim.categories.edit', 'uses' => 'Muntazim\CategoryController@edit',
        'permissions' => \App\Models\Category::modulePermissions(true, 'edit')]);
    Route::put('/categories/update/{id}', ['as' => 'muntazim.categories.update', 'uses' => 'Muntazim\CategoryController@update',
        'permissions' => \App\Models\Category::modulePermissions(true, 'edit')]);
    Route::get('/categories/destroy/{id}', ['as' => 'muntazim.categories.delete', 'uses' => 'Muntazim\CategoryController@destroy',
        'permissions' => \App\Models\Category::modulePermissions(true, 'manage')]);
    Route::post('/categories/uploader', ['as' => 'muntazim.categories.uploader', 'uses' => 'Muntazim\CategoryController@uploader',
        'permissions' => \App\Models\Category::modulePermissions(true, 'create')]);


    //Cinema
    Route::get('/cinemas/{pid?}', ['as' => 'muntazim.cinemas', 'uses' => 'Muntazim\CinemaController@index',
        'permissions' => \App\Models\Cinema::modulePermissions(true, 'manage')]);
    Route::get('/cinemas/edit/{id}', ['as' => 'muntazim.cinemas.edit', 'uses' => 'Muntazim\CinemaController@edit',
        'permissions' => \App\Models\Cinema::modulePermissions(true, 'edit')]);
    Route::get('/cinemas/media/{id}', ['as' => 'muntazim.cinemas.media', 'uses' => 'Muntazim\CinemaController@media',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    Route::put('/cinemas/update/{id}', ['as' => 'muntazim.cinemas.update', 'uses' => 'Muntazim\CinemaController@update',
        'permissions' => \App\Models\Cinema::modulePermissions(true, 'edit')]);

    //Terminals
    Route::get('/terminals/{pid?}', ['as' => 'muntazim.terminals', 'uses' => 'Muntazim\TerminalController@index',
        'permissions' => \App\Models\Terminal::modulePermissions(true, 'manage')]);
    Route::get('/terminals/edit/{id}', ['as' => 'muntazim.terminals.edit', 'uses' => 'Muntazim\TerminalController@edit',
        'permissions' => \App\Models\Terminal::modulePermissions(true, 'edit')]);
    Route::get('/terminals/media/{id}', ['as' => 'muntazim.terminals.media', 'uses' => 'Muntazim\TerminalController@media',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    Route::put('/terminals/update/{id}', ['as' => 'muntazim.terminals.update', 'uses' => 'Muntazim\TerminalController@update',
        'permissions' => \App\Models\Terminal::modulePermissions(true, 'edit')]);

    //Terminals
    Route::get('/shops/{pid?}', ['as' => 'muntazim.shops', 'uses' => 'Muntazim\ShopController@index',
        'permissions' => \App\Models\Shop::modulePermissions(true, 'manage')]);
    Route::get('/shops/edit/{id}', ['as' => 'muntazim.shops.edit', 'uses' => 'Muntazim\ShopController@edit',
        'permissions' => \App\Models\Shop::modulePermissions(true, 'edit')]);
    Route::get('/shops/media/{id}', ['as' => 'muntazim.shops.media', 'uses' => 'Muntazim\ShopController@media',
        'permissions' => \App\Models\Shop::modulePermissions(true, 'edit')]);
    Route::put('/shops/update/{id}', ['as' => 'muntazim.shops.update', 'uses' => 'Muntazim\ShopController@update',
        'permissions' => \App\Models\Shop::modulePermissions(true, 'edit')]);

    // Posts

    Route::get('/posts/{category_id?}', ['as' => 'muntazim.posts', 'uses' => 'Muntazim\PostController@index',
        'permissions' => \App\Models\Post::modulePermissions(true, 'manage')]);
    Route::get('/posts/create/{type?}', ['as' => 'muntazim.posts.create', 'uses' => 'Muntazim\PostController@create',
        'permissions' => \App\Models\Post::modulePermissions(true, 'create')]);
    Route::post('/posts/store/{type?}', ['as' => 'muntazim.posts.store', 'uses' => 'Muntazim\PostController@store',
        'permissions' => \App\Models\Post::modulePermissions(true, 'create')]);
    Route::get('/posts/show/{id}', ['as' => 'muntazim.posts.show', 'uses' => 'Muntazim\PostController@show',
        'permissions' => \App\Models\Post::modulePermissions(true, 'show')]);
    Route::get('/posts/edit/{id}', ['as' => 'muntazim.posts.edit', 'uses' => 'Muntazim\PostController@edit',
        'permissions' => \App\Models\Post::modulePermissions(true, 'edit')]);
    Route::put('/posts/update/{id}', ['as' => 'muntazim.posts.update', 'uses' => 'Muntazim\PostController@update',
        'permissions' => \App\Models\Post::modulePermissions(true, 'edit')]);
    Route::get('/posts/destroy/{id}', ['as' => 'muntazim.posts.delete', 'uses' => 'Muntazim\PostController@destroy',
        'permissions' => \App\Models\Post::modulePermissions(true, 'manage')]);

    //Movies
    Route::get('/movies/{pid?}', ['as' => 'muntazim.movies', 'uses' => 'Muntazim\MovieController@index',
        'permissions' => \App\Models\Movie::modulePermissions(true, 'manage')]);
    Route::get('/movies/edit/{id}', ['as' => 'muntazim.movies.edit', 'uses' => 'Muntazim\MovieController@edit',
        'permissions' => \App\Models\Movie::modulePermissions(true, 'edit')]);
    Route::get('/movies/media/{id}', ['as' => 'muntazim.movies.media', 'uses' => 'Muntazim\MovieController@media',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    Route::put('/movies/update/{id}', ['as' => 'muntazim.movies.update', 'uses' => 'Muntazim\MovieController@update',
        'permissions' => \App\Models\Movie::modulePermissions(true, 'edit')]);
    Route::get('/movies/seats/{id?}', ['as' => 'muntazim.movies.seats', 'uses' => 'Muntazim\MovieController@seats',
        'permissions' => \App\Models\Movie::modulePermissions(true, 'manage')]);
    //Vehicles
    Route::get('/vehicles/{pid?}', ['as' => 'muntazim.vehicles', 'uses' => 'Muntazim\VehicleController@index',
        'permissions' => \App\Models\Vehicle::modulePermissions(true, 'manage')]);
    Route::get('/vehicles/edit/{id}', ['as' => 'muntazim.vehicles.edit', 'uses' => 'Muntazim\VehicleController@edit',
        'permissions' => \App\Models\Vehicle::modulePermissions(true, 'edit')]);
    Route::get('/vehicles/media/{id}', ['as' => 'muntazim.vehicles.media', 'uses' => 'Muntazim\VehicleController@media',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    Route::put('/vehicles/update/{id}', ['as' => 'muntazim.vehicles.update', 'uses' => 'Muntazim\VehicleController@update',
        'permissions' => \App\Models\Vehicle::modulePermissions(true, 'edit')]);
    Route::get('/vehicles/routes/{id?}', ['as' => 'muntazim.vehicles.routes', 'uses' => 'Muntazim\VehicleController@routes',
        'permissions' => \App\Models\Vehicle::modulePermissions(true, 'manage')]);
    Route::get('/vehicles/seats/{id?}', ['as' => 'muntazim.vehicles.seats', 'uses' => 'Muntazim\VehicleController@seats',
        'permissions' => \App\Models\Vehicle::modulePermissions(true, 'manage')]);

    //Foods
    Route::get('/foods/{pid?}', ['as' => 'muntazim.foods', 'uses' => 'Muntazim\FoodController@index',
        'permissions' => \App\Models\Food::modulePermissions(true, 'manage')]);
    Route::get('/foods/edit/{id}', ['as' => 'muntazim.foods.edit', 'uses' => 'Muntazim\FoodController@edit',
        'permissions' => \App\Models\Food::modulePermissions(true, 'edit')]);
    Route::put('/foods/update/{id}', ['as' => 'muntazim.foods.update', 'uses' => 'Muntazim\FoodController@update',
        'permissions' => \App\Models\Food::modulePermissions(true, 'edit')]);
    Route::get('/foods/media/{id}', ['as' => 'muntazim.foods.media', 'uses' => 'Muntazim\FoodController@media',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    //Media

    Route::get('/media/edit/{id}', ['as' => 'muntazim.media.edit', 'uses' => 'Muntazim\MediaController@edit',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
    Route::put('/media/update/{id}', ['as' => 'muntazim.media.update', 'uses' => 'Muntazim\MediaController@update',
        'permissions' => \App\Models\Media::modulePermissions(true, 'edit')]);
});


Route::get('/movies/{id}', ['as' => 'movies', 'uses' => 'PostController@movies']);
Route::get('/movies/seats/{id}', ['as' => 'movies.seats', 'uses' => 'PostController@movieSeats']);
Route::get('/vehicles/{id}', ['as' => 'vehicles', 'uses' => 'PostController@vehicles']);
Route::get('/vehicles/seats/{id}', ['as' => 'vehicles.seats', 'uses' => 'PostController@vehicleSeats']);
Route::get('/foods/{id}', ['as' => 'foods', 'uses' => 'PostController@foods']);
Route::get('/seats/{id}', ['as' => 'seats', 'uses' => 'PostController@seats']);

// Booking

Route::get('/bookings/{id}', ['as' => 'bookings', 'uses' => 'BookingController@index']);

Route::get('/booking/store', ['as' => 'booking.store', 'uses' => 'BookingController@store']);


//Carts
Route::get('add-to-cart/{id}', 'CartController@addToCart');
Route::patch('/cart/update', 'CartController@update')->name('cart.update');
Route::delete('/cart/delete', 'CartController@remove')->name('cart.remove');
Route::get('/cart/checkout', 'CartController@checkout')->name('cart.checkout');
Route::post('/cart/proceeded', 'CartController@proceeded')->name('cart.proceeded');
Route::get('/cart/index', ['as' => 'cart.index', 'uses' => 'CartController@index']);
Route::get('/cart/index', ['as' => 'cart.index', 'uses' => 'CartController@index']);
Route::get('/booking/vehicle/', ['as' => 'booking.vehicle', 'uses' => 'BookingController@vehicle']);
Route::get('/booking/movie/', ['as' => 'booking.movie', 'uses' => 'BookingController@movie']);