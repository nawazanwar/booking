// Posts

Route::get('/posts', ['as' => 'dashboard.posts', 'uses' => 'Dashboard\PostController@index',
'permissions' => \App\Models\Post::modulePermissions(true, 'manage')]);
Route::get('/post/create', ['as' => 'dashboard.post.create', 'uses' => 'Dashboard\PostController@create',
'permissions' => \App\Models\Post::modulePermissions(true, 'create')]);
Route::post('/post/store', ['as' => 'dashboard.post.store', 'uses' => 'Dashboard\PostController@store']);
Route::get('/post/show/{id}', ['as' => 'dashboard.post.show', 'uses' => 'Dashboard\PostController@show',
'permissions' => \App\Models\Post::modulePermissions(true, 'show')]);
Route::get('/post/edit/{id}', ['as' => 'dashboard.post.edit', 'uses' => 'Dashboard\PostController@edit',
'permissions' => \App\Models\Post::modulePermissions(true, 'edit')]);
Route::put('/post/update/{id}', ['as' => 'dashboard.post.update', 'uses' => 'Dashboard\PostController@update',
'permissions' => \App\Models\Post::modulePermissions(true, 'edit')]);
Route::get('/post/delete/{id}', ['as' => 'dashboard.post.delete', 'uses' => 'Dashboard\PostController@destroy',
'permissions' => \App\Models\Post::modulePermissions(true, 'delete')]);
Route::post('/posts/uploader', ['as' => 'dashboard.posts.uploader', 'uses' => 'Dashboard\PostController@uploader',
'permissions' => \App\Models\Post::modulePermissions(true, 'create')]);